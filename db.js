'use strict';

const pg = require('pg');
const url = require('url');

let {
    DB_STRING = ``,
    VERBOSE,
} = process.env;

const {
    logInfo,
    logError,
    logTrace,
    logTime,
    logTimeEnd,
} = require('./loggers');

if ( !DB_STRING ) {
    const {
        dbConnection = `tcp://postgres@127.0.0.1:5432`,
    } = require('../cfg/web.json');

    if ( dbConnection ) {
        DB_STRING = dbConnection;
    }
    else {
        throw new Error(`Missing database connection info.  Put in ../cfg/web.json or ENV var $DB_STRING`);
    }
}

function parseDbConnString ( connStr ) {
    const {
        auth,
        hostname: host,
        port,
        pathname
    } = url.parse(connStr);

    const [
        user,
        password
    ] = auth.split(/:/);

    /**
     * @TODO remove check for env var
     */
    const database = pathname.split(/\//)[ 1 ];

    const config = {
        user,
        password,
        host,
        port,
        database,
        'ssl': false
    };

    return config;
}

const makePoolConfig = dbConnString => {
    const dbInfo = parseDbConnString(dbConnString);
    let application_name = `exa_web`;
    let min = 1;
    let max = 1;

    if ( global.mode ) {
        application_name += `_${global.mode}`;
        min = 2;
        max = 4;
    }

    return {
        application_name,

        //  max pool size
        max,

        //  min pool size
        min,

        // close idle clients after 1 minute (default is 30 seconds)
        'idleTimeoutMillis': 60000,

        ...dbInfo
    };
};

const config = makePoolConfig(DB_STRING);
const pgPool = new pg.Pool(config);

/**
 * Query DB for data
 * @param   {Object}            args
 * @param   {string}            args.sql
 * @param   {Array?}            args.params
 * @param   {Object?}           args.inputs
 * @returns {Promise}
 */
const query = async args => {
    const {
        sql = ``,
        params = [],
        options = {}
    } = args;

    const throwError = error => {
        logError(error.stack);
        var errorMessage = `
            DB query failed, details:
                           -- pg error   : ${JSON.stringify(error, null, 4)}
                           -- message    : ${error.message}
                           -- query args : ${params}
                           -- query sql  : ${sql}
        `;
        logTrace(errorMessage);
        return errorMessage;
    };

    // if ( VERBOSE ) {
    //     logInfo(sql, params);
    // }

    const client = await pgPool.connect();
    let result;
    try {
        if ( VERBOSE ) {
            logTime(`query timer: ${sql}`);
        }

        result = await client.query(sql, params);

        if ( VERBOSE ) {
            logTimeEnd(`query timer: ${sql}`);
        }
    }
    catch ( error ) {
        throwError(error);
        result = {};
    }
    finally {
        client.release();
    }

    const {
        rows = []
    } = result;

    return rows;
};

module.exports = {
    query,
    config,
};
