'use strict';

const encoding = 'utf8';
const fs = require('fs');

const {
    promisify
} = require('util');

const {
    hstoreStringify,
    makeUidStudy
} = require('./utils');

const {
    logInfo,
    logError,
} = require('./loggers');

const asyncWriteFile = promisify(fs.writeFile);

const makeFile = async ( string, path ) => {
    const fullPath = `${path}/${makeUidStudy()}.csv`;
    const fileContent = string || ``;

    try {
        await asyncWriteFile(fullPath, fileContent, { encoding });
    }
    catch ( e ) {
        logError(`Failed writing ${fullPath}`, e);
        return ``;
    }

    return fullPath;
};

const makeCSV = dataSet => {

    const headerRow = Object.keys(dataSet[ 0 ]);
    const headerString = headerRow.join('|');

    const makeRow = data =>
        headerRow.map(
            key => data[ key ] !== ``
                ? key.slice(-5) === `_info`
                    ? hstoreStringify(data[ key ])
                    : JSON.stringify(data[ key ])
                : ``
        ).join(`|`);

    return [
        headerString,
        ...dataSet.map(makeRow)
    ].join('\n');
};

module.exports = data => {

    const {
        dataSet,
        csvPath
    } = data;

    const csvString = makeCSV(dataSet);

    if ( !csvString ) {
        return dataSet;
    }

    return makeFile(csvString, csvPath);
};
