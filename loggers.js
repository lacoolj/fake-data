'use strict';

/**
 * Logging helpers
 */

const {
    VERBOSE,
    LOG_DIR,
    FAKE_TYPE,
} = process.env;

const fs = require('fs');
const path = require('path');

const PID_TEXT = `[${process.pid}]`;
const LOG_FILE = `${FAKE_TYPE}_${process.pid}_${Date.now()}.log`;
const LOG_FILE_PATH = !LOG_DIR
    ? ``
    : path.join(__dirname, `${LOG_DIR}/${LOG_FILE}`);

const getMessage = VERBOSE
    ? message => `${(new Date()).toLocaleTimeString()} ${PID_TEXT} ${message}`
    : message => `${PID_TEXT} ${message}`;

const fileLogger = ( fileStream = fs.createWriteStream(LOG_FILE_PATH, { 'autoClose': false, 'encoding': `utf8` }) ) => {

    fileStream.on(`error`, () => fileStream.close());

    process.on(`exit`, () => fileStream.close());

    return ( messageText, args ) => {

        let messages = `${messageText}\n`;
        const argsCount = args.length;

        if ( argsCount ) {
            const convertedArgs = args.reduce(function converter ( args, data ) {
                if ( data ) {
                    if ( typeof data !== `object` ) {
                        args.push(getMessage(data));
                    }
                    else if ( Array.isArray(data) ) {
                        args.push(getMessage(data.reduce(converter, [])
                            .join()));
                    }
                    else {
                        args.push(JSON.stringify(data))
                    }
                }
                return args;
            }, []);

            const convertedMessages = convertedArgs.join(`\n`);
            messages += `${convertedMessages}\n`;
        }

        fileStream.write(messages);

        return true;
    };
};

const logFile = !LOG_DIR
    ? async () => true
    : fileLogger();

const logInfo = ( message, ...args ) => {
    const messageText = getMessage(message);
    logFile(messageText, args);
    return console.log(messageText, ...args);
};

const logError = ( message, ...args ) => {
    const messageText = getMessage(message);
    logFile(messageText, args);
    return console.error(messageText, ...args);
};

const logTrace = ( message, ...args ) => {
    const messageText = getMessage(message);
    logFile(messageText, args);
    return console.trace(messageText, ...args);
};

const timers = new Map();

const logTime = ( name ) =>
    timers.set(name, Date.now());

const logTimeEnd = ( name ) => {
    logInfo(`${name}: ${Date.now() - timers.get(name)}ms`);
    timers.delete(name);
};

module.exports = {
    logFile,
    logInfo,
    logError,
    logTrace,
    logTime,
    logTimeEnd,
};
