'use strict';

const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const targets = {
    // 'pdf': require(`${exportDir}/fake/pdf.json`),
    // 'jpg': require(`${exportDir}/fake/jpg.json`),
    // 'png': require(`${exportDir}/fake/png.json`)
};

// const fileTypes = Object.keys(targets);

const fileTypes = [
    `pdf`,
    `jpg`,
    `png`
];

const documentSources = [
    `FX`,
    `SD`,
    `SF`,
    `UP`
];

module.exports = {
    exportDir,
    fileTypes,
    documentSources
};
