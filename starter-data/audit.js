/**
 * Copied from exaweb repo July 9, 2019
 * - then edited for usefulness
 */

const {
    Set,
    List,
} = require('immutable');

const auditObj = {
    hieConsents: {
        save: 'Created HIE consent code {0} with description {1}',
        update: 'Updated HIE consent code from {0} to {1}, and descripton from {2} to {3}',
        delete: 'Deleted HIE consent code {0} with descripton {1}',
        updatePatient: 'Updated Patient: HIE consent updated'
    },

    paymentComments: {
        save: 'Created note "{0}" for claim# {1}',
        update: 'Updated note from "{0}" to "{1}" for claim# {2}'
    },

    criticalFindings: {
        save: 'Created critical finding {0}',
        update: 'Updated critical finding from {0} to {1}',
        delete: 'Deleted critical finding {0}',
        updateStudies: 'Updated Study: critical finding updated'
    },

    general: {
        save: 'Add:{0} created',
        update: 'Update:{0} updated',
        delete: 'Delete:{0} deleted'
    },

    facility: {
        save: 'Add: New Facility ({0}) created',
        update: 'Update: Facility ({0}) updated',
        delete: 'Delete: Facility {0}({1}) deleted',
        updateMrn: 'Update: Company MRN type updated to {0}',
        updateAcc: 'Update: Company Accession Info updated : {0}'
    },
    modality: {
        save: 'Add: New Modality ({0}) created',
        update: 'Update: Modality ({0}) updated',
        delete: 'Delete: Modality {0}({1}) deleted',
        deleteTest: 'Delete: Test Modality deleted',
        updatePriority: 'Update: Modality ({0}) Priority updated'
    },
    modalityRooms: {
        save: 'Add: New ModalityRoom ({0}) created',
        update: 'Update: ModalityRoom ({0}) updated',
        delete: 'Delete: ModalityRoom {0}({1}) deleted',
        deleteTest: 'Delete: Test ModalityRoom deleted'
    },
    studyFlags: {
        save: 'New Study Flag ({0}) created, Institutions ({1}), Facilities ({2}), Modalities ({3})',
        update: 'Study Flag {0}({1}) updated, Institutions ({2}), Facilities ({3}), Modalities ({4})',
        delete: 'Study Flag {0}({1}) deleted',
        deleteTest: 'Test Study Flag deleted'
    },
    userAssignments: {
        update: 'Update: {0} id {1} assigned to user id {2}'
    },
    insuranceProvider: {
        save: 'Add: New Insurance provider ({0}) created',
        update: 'Update: Insurance provider({0}) updated',
        delete: 'Delete: Insurance provider {0}({1}) deleted',
        deleteTest: 'Delete: Test InsuranceProvider deleted'
    },
    icd: {
        save: 'Add: New IcdCode ({0}) created',
        update: 'Update: IcdCode ({0}) updated',
        delete: 'Delete: IcdCode {0}({1}) deleted',
        deleteTest: 'Delete: Test IcdCode deleted'
    },
    Hp: {
        save: 'Add: New Protocol ({0}) created',
        duplicateSave: 'Add: Protocol ({0}) created',
        copy: 'Update: Protocol ({0}) get copied',
        duplicate: 'Update: Protocol ({0}) updated'
    },
    clinicalOverview: {
        save: 'Update: Clinical Overview ({0}) updated'
    },
    clinicalRule: {
        save: 'Add: New clinical rule {0} created',
        update: 'Update: Clinical rule {0} updated',
        delete: 'Delete: Clinical rule {0} deleted'
    },
    customForm: {
        save: 'Add: New customForm ({0}) created',
        update: 'Update: CustomForm ({0}) updated',
        delete: 'Delete: CustomForm ({0}) deleted',
        deleteTest: 'Delete: Test customForm deleted'
    },

    formBuilder: {
        save: 'Add: New Form Builder ({0}) created',
        update: 'Update: Form Builder ({0}) updated',
        delete: 'Delete: Form Builder ({0}) deleted',
        deleteTest: 'Delete: Test formBuilder deleted'
    },

    familyHistory: {
        save: 'Add: New Family History ({0}) created',
        update: 'Update: Family History ({0}) updated',
        delete: 'Delete: Family History ({0}) deleted',
        deleteTest: 'Delete: Test Family History deleted'
    },
    vaccines: {
        save: 'Add: New vaccines ({0}) created',
        update: 'Update: Vaccines ({0}) updated',
        delete: 'Delete: Vaccines ({0}) deleted',
        deleteTest: 'Delete: Test vaccines deleted'
    },

    hl7Receiver: {
        save: 'Add: New HL7Receiver ({0}) created',
        update: 'Update: HL7Receiver ({0}) updated and file saved in {1}',
        delete: 'Delete: HL7Receiver ({0}) deleted',
        deleteTest: 'Delete: Test HL7Receiver deleted',
        reprocess: 'Update: HL7Receiver has been reprocessed for({0}) in the port of ({1}) at ({2})',
        delete: 'Delete: HL7Receiver({0}) log has been deleted'
    },
    hl7Trigger: {
        save: 'Add: New HL7Trigger ({0}) created',
        update: 'Update: HL7Trigger ({0}) updated',
        delete: 'Delete: HL7Trigger ({0}) deleted'
    },
    vehicleRegistration: {
        save: 'Add: New vehicle ({0}) created',
        update: 'Update: Vehicle ({0}) updated',
        delete: 'Delete: Vehicle ({0}) deleted',
        deleteTest: 'Delete: Test vehicle registration deleted',
        updateFence: 'Update: Vehicle ({0}) fence updated'
    },

    vehicleInfo: {
        save: 'Add: New vehicle Info Type ({0}) created for VehicleName ({1})',
        update: 'Update: Vehicle Info Type ({0}) updated for VehicleName ({1})',
        delete: 'Delete: Vehicle Info deleted for VehicleName ({0})',
        deleteTest: 'Delete: Test vehicle information deleted'
    },

    marketingRep: {
        saveTask: "Add: New task({0}) created for ({1})",
        updateTask: "Update: Marketing Rep Task ({0}) updated for ({1})",
        deleteNotes: "Delete: Marketing Rep Task ({0} deleted for ({1})",
        saveFollowup: "Add: New Followup ({0}) created for ({1})",
        updateFollowup: "Update: Marketing Rep ({0}) Followup ({1}) updated",
        deleteFollowup: "Delete: Marketing Rep ({0}) Followup ({1}) deleted",
        update: "Update: marketing Rep details updated",
        contractSave: 'Add: New contract ({0}) saved for ({1})',
        contractDelete: 'Delete: Contract ({0}) deleted for ({1})',
        contractUpdate: 'Update: Contract ({0}) updated for ({1})',
        updateUser: "Update: Marketing Rep ({0}) details updated"
    },

    vehicleMaintenance: {
        save: 'Add: New Vehicle Maintenance created for VehicleName ({0})',
        update: 'Update: Vehicle Maintenance updated for VehicleName ({0})',
        delete: 'Delete: Vehicle Maintenance deleted for VehicleName ({0})',
        deleteTest: 'Delete: Test Vehicle Maintenance deleted'
    },

    vehicleSchedules: {
        save: 'Add: New Vehicle Schedules created for VehicleName ({0}) ',
        update: 'Update: Vehicle Schedules updated for VehicleName ({0})',
        delete: 'Delete: Vehicle Schedules deleted for VehicleName ({0})',
        deleteTest: 'Delete: Test Vehicle Schedules deleted'
    },
    vehicleTracking: {
        save: 'Add: New Vehicle GeoFence created for VehicleName ({0}) ',
        update: 'Update: Vehicle GeoFence updated for VehicleName ({0})',
        delete: 'Delete: Vehicle GeoFence deleted for ({0})'
    },
    hpGroups: {
        save: 'Add: New HPGroup ({0}) created',
        update: 'Update: HP Group ({0}) updated',
        delete: 'Delete: HP Group ({0}) deleted'
    },
    hpRules: {
        save: 'Add: New Matching Rules ({0}) created',
        update: 'Update: Matching Rules ({0}) updated',
        delete: 'Delete: Matching Rules ({0}) deleted'
    },

    bodyParts: {
        save: "Add: New body part ({0}) added",
        update: "Update: Body part ({0}) updated",
        delete: "Delete: Body part ({0}) deleted"
    },

    pendingFollowUps: {
        delete: 'Delete: Pending FollowUps ({0}) deleted',
        update: 'Update: Pending FollowUps ({0}) updated'
    },

    patientInsurance: {
        save: 'Add: New Patient insurance ({0}) for ({1}) created',
        update: 'Update: Patient insurance ({0}) for ({1}) updated',
        delete: 'Delete: Patient insurance ({0}) deleted',
        deleteTest: 'Delete: Test PatientInsurance deleted',
        eligibilityCheck: 'Eligibility check: Patient Insurance for({0})',
        responsibleUpdate: 'Insurance provider({0}) has been updated to ({1}) after its has assigned as Responsible party'
    },
    patientGuarantor: {
        save: 'Add: New Patient Guarantor ({0})created',
        update: 'Update: Patient Guarantor ({0}) updated',
        delete: 'Delete: Patient Guarantor ({0}) deleted'
    },
    familyHealthHistory: {
        save: 'Add: New Family health history created',
        update: 'Update: Family health history updated',
        delete: 'Delete: Family health history deleted'
    },
    teachingStudy: {
        save: 'Add: Teaching Study has been created'
    },
    scheduleBlock: {
        save: 'Add: New ScheduleBlock ({0}) created',
        update: 'Update: ScheduleBlock ({0}) updated',
        delete: 'Delete: ScheduleBlock ({0}) deleted',
        deleteTest: 'Delete: Test ScheduleBlock deleted'
    },
    patientAlerts: {
        update: 'Update: Patient alerts ({0}) and Other alerts ({1}) updated',
        updateAlert: 'Update: Patient alerts updated'
    },
    pendingReferrals: {
        update: 'Update: Pending Referrals Status for referral no ({0}) updated'
    },
    prescription: {
        save: 'Add: New Prescription for ({0}) created for ({1})',
        update: 'Update: Prescription for ({0}) updated for ({1})',
        updateStatusChange: 'Change: Prescription has been updated from {0} to {1} for ({1}) ',
        delete: 'Delete: Prescription for ({0}) deleted for ({1})',
        insertSigned: 'Add: Prescription for ({0}) created and signed',
        updateSigned: 'Update: Prescription for ({0}) updated and signed'
    },
    medications: {
        save: 'Add: Created Medication {0} for {1}',
        update: 'Update: Updated Medication {0} for {1}',
        updateStatusChange: 'Change: Medications ({0}) has been updated from {1} to {2} for ({1})',
        delete: 'Delete: Deleted Medication for {0} for {1}',
        providerInsert: 'Update: RxProvider ({0}) added',
        updateReconcile: 'Update: Medication Reconcile updated'
    },

    vitalSign: {
        save: 'Add: New Vital Sign created',
        update: 'Update: Vital Sign Updated'
    },
    allergies: {
        save: 'Add: Created New allergy {0} for {1}',
        update: 'Update: Updated Allergy {0} for {1}',
        inActive: 'Update: Inactivated Allergy {0} for {1}',
        active: 'Update: Activated Allergy {0} for {1}'
    },
    additionalInfo: {
        update: 'Update: AdditionalInfo updated'
    },
    study: {
        save: 'Add: New Study ({0}) created',
        update: 'Update: Study({0}) CPT  has been changed to ({1})',
        updateStudy: 'Update: New Cpt ({0}) Added to Study ({1})',
        deleteStudy: 'Delete: Cpt ({0}) deleted from Study ({1})',
        delete: 'Delete: {0} {1} {2} has been deleted by {3}',
        unDelete: 'Update: {0} {1} {2} has been undeleted by {3}',
        deleteTest: 'Delete: Test Study deleted',
        updateStatus: 'Change: Study status has been changed to {0}',
        updateStatusProvider: 'Change: Study status has been changed to {0} by {1}',
        updateHL7Status: 'Change: Study status has been changed {0} to {1} from HL7',
        abortedStatus: 'Update: Study has been aborted due to {0}',
        priority: 'Change: Priority has been changed to {0}',
        stat_level: 'Change: Stat level has been changed to {0} ({1})',
        purge: "Purge: {0} {1} {2} has been purged by {3}",
        orderdelete: "Delete: Orders has been deleted",
        orderpurge: "Delete: {0} {1} {2} has been purged ",
        undeleteOrders: "Update: Orders has been undeleted",
        dicomStatus: "Change: DICOM Status has been changed to {0}",
        studyflag: "Change: Study Flag has been changed to {0}",
        sendStatus: "Change: Study Send status has been changed to {0}",
        faxStatus: "Change: Study Fax status has been changed to {0}",
        updateAvailableSlots: "Change: Study status has been changed to {0} by {1}",
        pendStudyMatch: "Update: Pending Studies Matched",
        qcStudyReconciled: "Study(Accession#: {0}, Account#: {1}, Patient: {2}, Study Dt: {3}) has been reconciled with RIS order(Accession#: {4}, Account#: {5}, Patient: {6}, Study Dt: {7})",
        moveStudyManual: "Update: Study has been moved manually for the patient ({0})",
        moveStudyCancel: "Update: Study move has been cancelled manually for the patient ({0})",
        updateLinkStudy: "Update: Ris study ({0} , {1} : {2}) - {3} has been linked with dicom study ({4} , {5} : {6}) - {7}",
        resetLinkStudy: "Update: Ris study ({0} , {1} : {2}) - {3} has been unlinked from dicom study ({4} , {5} : {6}) - {7}",
        studyNotes: "Update: Study notes updated",
        viewerStudyNotes: "Update: Study notes updated",
        updateNote: 'Update: Note #{0} of Study ({1}) was updated from "{2}" to "{3}"',
        addNote: 'Add: Study ({0}) Note ("{1}")',
        recinciliationManualMove: "Update: ({0}) ({1}) ({2}) conflict resolved by the user ({3}). Action: Move Anyway",
        modifyAuthorization: "Change: Order Authorization has been changed to ({0}) days",
        studyCriticalEnable: 'Update: Study has been marked as critical for the patient ({0}) with the study description ({1})',
        studyCriticalDisable: 'Update: Study has been unmarked as critical for the patient ({0}) with the study description ({1})',
        technologistUpdate: 'Change: Technologist has been changed to {0} by {1}',
        reasonForStudy: 'Update: Reason for study {0} updated from "{1}" to "{2}"',
        resetStatus: 'Reset: Study ({0}) status has been changed ({1}) to ({2}) by user ({3})',
        deleteStudyImage: 'Delete study image from ({0}  - {1}) for ({2}), Patient: {3}({4}), Accession #: {5}',
        deleteStudySeries: 'Delete study series from ({0}  - {1}) for ({2}), Series UID: {6}',
        changePatient: 'Update: Patient of Study was updated from "{0}"(DICOM Patient ID) to "{1}"(DICOM Patient ID)'
    },
    queryRetrieve: {
        query_log: 'Query: Study information queried  - ({0},{1},{2})',
        retrieve_studyUID_log: 'Retrieve :Study information retrieved  study_uid -({0})',
        retrieve_log: 'Retrieve :Study information retrieved '
    },
    noShows: {
        update: 'Update: Schedule marked as NoShows for ({2}) facility from ({0}) to({1})'
    },
    appointmentType: {
        save: 'Add: New appointment type created ',
        update: 'Update: appointment type has been updated ',
    },
    patientIcd: {
        save: 'Add: Created Problem {0} for {1}',
        updateStatus: 'Change: Updated Problem {0} from {1} to {2} for {3}',
        update: 'Update: Updated Problem {0} for {1}',
        billingUpdate: 'Update : updated Problem ({0})',
        delete: 'Delete: Deleted Problem {0} for {1}',
        updateOrder_no: 'Update: Updated Order no for Problem {0} of {1}'
    },

    paySchedule:{
        save: 'Add: New Provider Pay Schedule ({0}) created',
        update: 'Update: Provider Pay Schedule ({0}) updated',
        delete: 'Delete: Provider Pay Schedule ({0}) deleted'
    },

    pendingStudy: {
        delete: 'Delete: QC Study deleted for the patient: {0}({1}), Accession #: {2}',
        undelete: 'Update: QC Study undeleted for the patient: {0}({1}), Accession #: {2}',
        purge: 'Delete: QC Study purged for the patient: {0}({1}), Accession #: {2}',
        reprocess: 'Update: QC Study reprocessed for the patient: {0}({1}), Accession #: {2}',
        dicomValUpdate: 'Update: DICOM values updated for the patient: {0}({1}), Accession #: {2}',
        dicomStudy: 'Update: DICOM values updated for the study'
    },

    dmRecRule: {
        save: 'Add: New Receiver Rule ({0}) created',
        update: 'Update: Receiver Rule ({0}) updated',
        updatePriority: 'Update: Receiver Rule ({0}) Priority updated',
        delete: 'Delete: Receiver Rule ({0}) deleted'
    },
    owners: {
        save: 'Add: New Owners ({0},{1}) created',
        update: 'Update: Owner ({0},{1}) updated',
        delete: 'Delete: Owner ({0},{1}) deleted',
        deleteTest: 'Delete: Test Owners deleted'
    },
    userSettings: {
        save: 'Add: New User settings ({0}) created',
        update: 'Update: User settings updated for the user ({0})',
        deleteTest: 'Delete: Test user settings deleted',
        vieweroptions: 'Update: Viewer Option {0} updated',
        userSettingsUpdate: 'Update: User Settings has been updated',
        userInterface: 'Update: UserInterface Updated',
        gridColsUpdated: 'Grid Columns Order Rearranged'
    },

    order: {
        save: 'Add: New order is created',
        splitOrderSave: 'Add: New order has been created by splitting from ({0})',
        splitNewOrderSave: 'Add: Order has been created by splitting from ({0}) to ({1})',
        updateStatus: 'Change: Order status has been changed to {0}',
        canceledStatus: 'Change: Order has been canceled due to {0}, Studies for Order {1} has been canceled due to {0}',
        updateStatusBySystem: 'Change: Order status has been changed to {0} by system',
        stat_level: 'Change: Stat level has been changed to {0}',
        priority: 'Change: Priority has been changed to {0}',
        studyflag: 'Change: Flag has been changed to {0}',
        abortStatus: 'Change: Order status has been changed to {0}',
        assignVehicle: 'Update: Order ({0}) has been assigned for vehicle ({1})',
        assignVehicleChangeStatus: 'Change: Order ({0}) status has been changed to ({1}) by user ({2})',
        orderUpdatedFromHL7: 'Update: Order updated from HL7',
        orderUpdatedFromApp: 'Update: Order updated from Mobile App by ({0})',
        reprocessConflicts: 'Update: Reprocess Conflicts - event executed from QC',
        orderCreateFromPortal: "Add: New order is created from portal",
        orderUpdateFromPortal: "Update: Order is updated from portal",
        undoAssignedJob: "Change: Assigned job ({0}) has been changed to unassigned order",
        orderNotes: "Update: Order notes updated",
        updateNote: 'Update: Note #{0} of Schedule ({1}) was updated from "{2}" to "{3}"',
        addNote: 'Add: Schedule ({0}) Note ("{1}")',
        dispatchVehicle: 'Order({0}) dispatched to vehicle({1})',
        dispatchVehicleUser: 'Order({0}) dispatched to vehicle({1}) by scheduler override',
        dispatchTechnologist: 'Order({0}) dispatched to Technologist({1})',
        dispatchTechnologistUser: 'Order({0}) dispatched to Technologist({1}) by scheduler override',
        dispatchTechnologistVehicle: 'Order({0}) dispatched to Technologist({1}) and Vehicle({2})',
        dispatchTechnologistVehicleUser: 'Order({0}) dispatched to Technologist({1}) and Vehicle({2}) by scheduler override',
        rescheduleTechnologistVehicle: 'Order({0}) Rescheduled to Technologist({1}) and Vehicle({2})',
        updateClaimStatus: 'Claim Status({0}) has been updated to order({1}) while order status changed to check in',
        updateClaimStatusFromHL7: 'Claim Status({0}) has been updated to order({1}) while order status changed to check in from HL7',
        updateRenderingProvider: 'Rendering Provider({0}) has been updated to order({1})',
        updateRenderingProviderFromHL7: 'Rendering Provider({0}) has been updated to order({1}) from HL7',
        updateApprovingProviderAsRenderingProvider: 'Approving Provider({0}) has been assigned as Rendering Provider to order({1})',
        updateApprovingProviderAsRenderingProviderFromHL7: 'Approving Provider({0}) has been assigned as Rendering Provider to order({1}) from HL7',
        uploadDicomDocument:'Dicom upload for accession no ({0})',
        purgeOrder:'Purge: order ({0}) for patient ({1}) has been purged by ({2}) '

    },

    cpt: {
        csvImport: 'Import: CPTs row id {0} through {1} - total: {2}',
        csvImportUpdate: 'Import/Update: CPTs row id {0} through {1} - total: {2}',
        save: 'Add: New CPT ({0}) created',
        update: 'Update: CPT ({0}) updated',
        delete: 'Delete: CPT {0}({1}) deleted',
        templateSave: "Add: Report template ({0}) created for the CPT ({1})",
        templateUpdate: "Update: Report template ({0}) updated for the CPT ({1})",
        templateDelete: "Delete: Report template ({0}) deleted for the CPT ({1})"
    },

    companies: {
        save: 'Add: New Company ({0}) created',
        update: 'Update: Company ({0}) updated',
        delete: 'Delete: Company ({0}) deleted',
        deleteSettings: 'Delete: ({3} - {1}) deleted for the Company {0} ',
        settingsUpdate: 'Update: Settings updated for company ({0})',
        updateDocumentReview: 'Update: Document review details updated for company ({0})',
        generalInfoUpdate: "Update: General information updated for the company ({0})"
    },

    users: {
        save: 'Add: New User ({0}) created',
        update: 'Update: User ({0}) updated',
        delete: 'Delete: User ({0}) deleted',
        userDeviceisactive: 'Update: UserDevice ({0})',
        userUnlink: 'Update: User ({0}) unlinked for the provider ({1})',
        userLink: 'Update: User ({0}) linked for the provider ({1})',
        saveUserImport: 'Import: New User ({0}) created',
        updateUserImport: 'Import: User ({0}) updated',
        copyUserSetting: 'User settings and viewer settings updated for users ({0}) from ({1})'
    },

    userRoles: {
        save: 'Add: New UserRoles ({0}) created',
        update: 'Update: UserRoles ({0}) updated',
        delete: 'Delete: UserRoles ({0}) deleted'
    },
    patientMerge: {
        save: 'Change: Patient {0} ({1}) merged to Patient {2} ({3})'
    },

    scheduleFilters: {
        save: 'Add: New ScheduleFilter ({0}) created',
        update: 'Update: ScheduleFilter ({0}) updated',
        delete: 'Delete: ScheduleFilter ({0}) deleted'
    },

    appointmentTypes: {
        save: 'Add: New Appointment type ({0}) created',
        update: 'Update: Appointment type ({0}) updated',
        delete: 'Delete: Appointment type ({0}) deleted'
    },

    providerGroups: {
        save: 'Add: New ProviderGroup ({0}) created',
        update: 'Update: ProviderGroup ({0}) updated',
        delete: 'Delete: ProviderGroup ({0}) deleted',
        providerDelete: 'Delete: Provider ({0}) deleted from the provider group ({1})'
    },

    provider: {
        saveProvider: 'Add: New Provider ({0}) created',
        saveProviderImport: 'Import: New Provider ({0}) created',
        updateProvider: 'Update: Provider ({0}) updated',
        updateProviderImport: 'Import: Provider ({0}) updated',
        saveProvAltContact: 'Add: New Provider Alt.Name ({0}) created for the provider ({1})',
        updateContact: 'Update: Provider Alt.Name ({0}) updated for the provider ({1})',
        deleteAltName: 'Delete: Provider Alt.Name ({0}) deleted for the provider ({1})',
        deleteProvider: 'Delete: Provider {0} ({1}) deleted',

        savePC: 'Add: New ProviderContact ({0}) created the provider ({1})',
        updatePC: 'Update: ProviderContact ({0}) updated for the provider ({1})',
        deletePC: 'Delete: ProviderContact {0}({1}) deleted',
        saveSchTemplate: 'Add: New scheduleTemplate ({0}) created for the provider ({1})',
        updateSchTemplate: 'Update: ScheduleTemplate ({0}) updated for the provider ({1})',
        deleteSchTemplate: 'Delete: ScheduleTemplate ({0}) deleted for the provider ({1})',
        saveTechTemplate: 'Add: Technologist ScheduleTemplate created for the provider ({0})',
        updateTechTemplate: 'Update: Technologist ScheduleTemplate updated for the provider ({0})',
        scheduleTemplateRemoved: 'Delete: Schedule tempate appointment deleted for ({0}) for the provider ({1})'
    },

    providerContactNotification: {
        save: 'Provider notifications for (ID: {0}, Name: {1}, Contact code: {2}) - saved as ({3})'
    },

    studyFilters: {
        save: 'Add: New StudyFilter ({0}) created',
        update: 'Update: StudyFilter ({0}) updated',
        delete: 'Delete: StudyFilter ({0}) deleted'
    },

    routingRules: {
        save: 'Add: New RoutingRule ({0}) created',
        update: 'Update: RoutingRule ({0}) updated',
        delete: 'Delete: RoutingRule ({0}) deleted'
    },

    applicationEntity: {
        save: 'Add: New ApplicationEntity ({0}) created',
        update: 'Update: ApplicationEntity ({0}) updated',
        updateCompany: 'Update: ApplicationEntity ({0}) companyID ({1}) updated',
        delete: 'Delete: ApplicationEntity ({0}) deleted',
        deleteTest: 'Delete: Test ApplicationEntity deleted'
    },
    srMapping: {
        save: 'Add: New SR Mapping ({0}) created',
        update: 'Update: SR Mapping ({0}) updated',
        delete:  'Delete: SR Mapping ({0}) deleted'
    },
    mappingFields: {
        save: 'Add: New Mapping Field ({0}) created',
        update: 'Update: Mapping Field ({0}) updated',
        delete:  'Delete: Mapping Field ({0}) deleted'
    },
    localCacheConfig: {
        assignLocalCache: 'Update :LocalCache ({0}) by ({1})'
    },
    servers: {
        save: 'Add: New Server ({0}) created',
        update: 'Update: Server ({0}) updated',
        delete: 'Delete: Server ({0}) deleted'
    },

    sites: {
        save: 'Add: New Site ({0}) created',
        update: 'Update: Site ({0}) updated',
        delete: 'Delete: ({0}) deleted in ({1})',
        updateImageServiceConfig: 'Update: Image Service Config updated'
    },
    modifiers: {
        save: 'Add: New modifier ({0}) created',
        update: 'Update: modifier ({0}) updated',
        delete: 'Delete: ({0}) deleted in ({1})'
    },
    imageServer: {
        save: 'Add: New FileStore ({0}) created',
        update: 'Update: FileStore ({0}) updated',
        delete: 'Delete: FileStore ({0})deleted'
    },
    tempPatient: {
        saveTempPatient: 'Add: New TempPatient ({0}) created',
        saveTempStudies: 'Add: New TempStudies ({0}) created',
        updateTempStudies: 'Update: TempStudies ({0}) updated',
        saveTempStudySeries: 'Add: New TempStudySeries ({0}) created',
        saveTempInstanceFiles: 'Add: New TempStudy Series Instance Files ({0}) created',
        saveTempInstances: 'Add: New Temp Study Series Instances ({0}) created'
    },

    apiModels: {
        save: 'Add: New APIModel ({0}) created',
        update: 'Update: APIModel ({0}) updated',
        deleteTest: 'Delete: Test ApiModel deleted'
    },

    apiRequests: {
        save: 'Add: New apiRequest ({0}) created',
        updateStudyAnnotations: 'Update Study Annotations for ({0})'
    },

    orders: {
        studyInfo: {
            save: 'Update: Study Info updated for ({0})',
            delete: 'Delete: Study Info deleted for ({1})'
        },
        refProv: {
            save: 'Add: New Ref.Prov ({0}) added',
            saveReadPhy: 'Add: New Read.Phy ({0}) added',
            saveOrdPhy: 'Add: New Ord.Prov ({0}) added',
            delete: 'Delete: Ref Prov ({0}) deleted',
            saveStudy: 'Add: Study ({0}) created'
        },

        icdCodes: {
            save: 'Add: New ICD Code ({0}) added',
            delete: 'Delete: ICD Code ({0}) deleted '
        },
        transcription: {
            save: 'Add: Transcription saved for study ({0})',
            update: 'Update: Transcription updated for study ({0})',
            overwrite: 'Update: Final Transcription overwrite for study ({0})',
            delete: 'Delete: Transcription deleted for study ({0})',
            saveNotes: "Add: Order Notes Added",
            recordSave: 'Add: New Audio clip uploaded',
            criticalEnable: 'Update: Critical status has been updated',
            resetStatus: 'Reset: Study ({0}) status has been changed ({1}) to ({2}) by user ({3})'
        },
        hl7: {
            update: 'Update: Study updated by hl7 for ({0})'
        },

        manualEdit: {
            update: 'Update: ManualEdit data updated'
        }
    },
    ccRos: {
        updateEditorData: 'Update: ccRos editor content updated',
        save: 'Add: New CCRos ({0}) created',
        update: 'Update: CCRos ({0}) updated',
        delete: 'Delete: CCRos ({0}) deleted'
    },
    medicalHistory: {
        update: 'Update: Medical History updated'
    },
    usergroup: {
        save: 'Add: New Usergroup ({0}) created',
        update: 'Update: Usergroup ({0}) updated',
        delete: 'Delete: Usergroup ({0}) deleted'
    },
    patientinfo: {
        save: 'Add: New Patient ({0}) created',
        saveKiosk: 'Add: New Patient ({0}) created from Patient Kiosk',
        patientName: 'Add: for the Patient ({0})',
        detailsAdded: 'Add: {0} has been added ',
        detailsRemoved: 'Delete: {0} has been Cleared ',
        changes: 'Change: {0} has been changed from "{1}" to "{2}" ',
        fast_payment_changes: 'Change: {0} has been changed ',
        update: 'Update: Patient ({0}) updated',
        delete: 'Delete :Patient ({0}) deleted',
        statusActiveChange: 'Change: Patient ({0}) status has been changed as active',
        statusInActiveChange: 'Change: Patient ({0}) status has been changed as Inactive',
        newAccountNumber: 'Add: Patient account number ({0}) created',
        portallock: 'Change: Patient portal has been locked',
        portalactive: 'Change: Patient portal has been activated',
        portalcreate: 'Add: Patient portal account has been created',
        resetPassword: 'Update: Patient\'s recent password has been updated successfully',
        emailupdate: 'Update: Patient email account has been updated successfully',
        lanuguageUpdate: 'Change: Patient language has been Updated from ({0}) to ({1})',
        raceUpdate: 'Change: Patient Racial has been Updated from ({0}) to ({1})',
        ethinUpdate: 'Change: Patient Ethnicity has been Updated from ({0}) to ({1})',
        commPrefUpdate: 'Change: Patient Communication Preferences has been Updated from ({0}) to ({1})',
        smokingStatusUpdate: 'Change: Patient ({0}) updated - Smoking Status changed From ({1}) to ({2})',
        smokingStatusUpdateHL7: 'Change: Patient ({0}) updated - Smoking Status changed From ({1}) to ({2}) by ({3})',
        smokingStatusAdded: 'Add: Patient ({0}) updated - Smoking Status ({1}) is added',
        smokingStatusCleared: 'Delete: Patient ({0}) updated - Smoking Status ({1}) is cleared',
        emailInsert: 'Update: Mail has been sent to ({0})',
        faxInsert: 'Update: Fax has been sent to ({0})',
        autoMerge: 'Change: Auto Merge has been done for {0}',
        portalAppointment: "Add: Appointment created for patient {0} ({1})",
        approvedReport: "Update: Approved Report has been sent to Patient",
        mailTopatientRep: "Update: Approved Report has been sent to Authorised Patient ({0})",
        portalInfo: "Update: Patient portal information updated",
        patient_queried: 'Query: Patient information queried',
        clinical_rule_queried: 'Query: Clinical supported rules queried ',
        resetPasswordTopatientRep: 'Update: Patient\'s representative recent password has been updated successfully',
        updateNote: 'Update: Note #{0} of Patient ({1}) was updated from "{2}" to "{3}"',
        addNote: 'Add: Patient ({0}) Note ("{1}")',
        studyCancel: 'Study ({0}) has been canceled due to {1}',
        patientStatementPrint: 'Patient({0}) Statement Printed for $({1})'
    },

    patientdocument: {
        delete: 'Delete: Patient document ({0}) deleted',
        save: 'Add: New patient document ({0}) created',
        update: 'Update: Patient document ({0}) updated'
    },
    patientMessages: {
        save: 'Add: New patient Message ({0}) sent to ({1})',
        update: 'Update: Patient Message ({0}) replied to ({1})'
    },
    login: {
        audit: 'Update: User {0} logged in at {1}'
    },
    logout: {
        audit: 'Update: User {0} logged out at {1}'
    },
    tasks: {
        save: 'Add: New Tasks ({0}) created',
        update: 'Update: Tasks ({0}) updated',
        delete: 'Delete: Tasks ({0}) deleted',
        deleteTest: 'Delete: Test tasks deleted'
    },
    news: {
        save: 'Add: New News ({0}) created',
        update: 'Update: News ({0}) updated',
        delete: 'Delete: News ({0}) deleted',
        deleteTest: 'Delete: Test News deleted'
    },
    mySettings: {
        update: 'Update: MySettings ({0} {1}) updated',
        updateEmergencyAccess: 'Update: EmergencyAccess is {0} by {1}'
    },

    issues: {
        save: 'Add: New issue ({0}) created',
        update: 'Update: Issue ({0}) resolved',
        delete: 'Delete: Issue ({0}) deleted',
        deleteTest: 'Delete: Test Issue deleted'
    },

    usersonline: {
        terminate: 'Update: User ({0}) from ({1}) has been terminated',
        save: 'Add: UserOnline created',
        update: 'Update: UserOnline updated'
    },
    studyStatus: {
        save: 'Add: New Study Status ({0}) created',
        update: 'Update: Study Status ({0}) updated',
        delete: 'Delete: Study Status ({0}) and all status notifications deleted',
        deleteTest: 'Delete: Test Study Status deleted'
    },
    statusNotifications: {
        save: 'Save: Status notifications for ({0} facility {1}) saved as ({2})'
    },
    statusFlow: {
        // save: 'Change: Study StatusFlow from ({0}) to ({1}) updated for ({2})',
        saveStatusLocation: 'Update: Study StatusFlow Location updated',
        update: 'Update: Study StatusFlow ({0}) updated for ({1})',
        delete: 'Delete: Study StatusFlow ({0}) deleted for ({1})',
        deleteTest: 'Delete: Test StatusFlow deleted',
        reset: 'Update: Study StatusFlow for ({0}) reset'

    },
    reportTemplate: {
        save: 'Add: New Report Template ({0}) created',
        update: 'Update: Report Template ({0}) updated',
        delete: 'Delete: Report Template ({0}) deleted'
    },
    emailTemplate: {
        save: 'Add: New Email Template ({0}) created',
        update: 'Update: Email Template ({0}) updated',
        delete: 'Delete: Email Template ({0}) deleted'
    },
    notificationTemplates: {
        save: 'Add: New Notification Template (name: {0}) created',
        update: 'Update: Notification Template (name: {0}) updated',
        delete: 'Delete: Notification Template (name: {0}) deleted'
    },
    coverSheets: {
        save: 'Add: New cover sheet (name: {0}) created',
        update: 'Update: cover sheet (name: {0}) updated',
        delete: 'Delete: cover sheet (name: {0}) deleted'
    },
    modules: {
        save: 'Add: New module ({0}) created',
        update: 'Update: Module ({0}) updated',
        delete: 'Delete: Module ({0}) deleted'

    },
    modulePermissions: {
        save: 'Add: New modulePermissions ({0}) created',
        update: 'Update: ModulePermissions ({0}) updated',
        delete: 'Delete: ModulePermissions ({0}) deleted'

    },
    subDomains: {
        save: 'Add: New subDomains ({0}) created',
        update: 'Update: SubDomains ({0}) updated',
        delete: 'Delete: SubDomains ({0}) deleted'

    },
    securityAudit: {
        save: 'Add: New SecurityAudit created for ({0}) in ({1}) )',
        update: 'Update: SecurityAudit updated for ({0}) in ({1}) )'
    },
    assignForms: {
        save: 'Change: ({0}) form is assigned to ({1})'
    },
    referrals: {
        save: 'Add: New Referrals order ({0}) created',
        update: 'Update: Referrals ({0}) updated',
        delete: 'Delete: Referrals ({0}) deleted'

    },
    insAuthorizationHL7: {
        save: 'Save :  {0}  insurance is authorized for accession ( {1} ) and for CPT ( {2} )',
        update: 'Update :  {0}  insurance is authorized for accession ( {1} ) and for CPT ( {2} )'
    },

    insAuthorizationInfo: {
        update: 'Update: {0} insurance is Authorized for the CPT ({1})'
    },
    providerSchedule: {
        save: 'Add: New Appointment Type({0}) created',
        update: 'Update: Appointment Type Updated for Provider ({0})',
        reschedule: 'Update: Appointment Type Updated '
    },
    labOrders: {
        save: 'Add: New Lab Order ({0}) created',
        orderUpdate: 'Update: Lab Order ({0}) updated',
        update: 'Update: Lab Order ({0}) updated for the Lab order ({1})',
        delete: 'Delete: Lab Order ({0}) deleted',
        criticalUpdate: 'Update: Critical option set as {0} for the lab test ({1})',
        criticalEnable: 'Update: Critical option enabled for the lab test ({0})',
        criticalDisable: 'Update: Critical option disabled for the lab test ({0})',
        labResultReviewed: 'Update: Lab result ({0}) has been reviewed by the provider ({1}) for the patient ({2})',
        encNotesReviewed: 'Update: Encounter result ({0}) has been reviewed by the provider ({1}) for the patient ({2})',
        encNotesReviewedPhysician: 'Update: Encounter result has been reviewed by the provider ({0}) for the patient ({1})',
        appReportReviewed: 'Update: Approved result ({0}) has been reviewed by the provider ({1}) for the patient ({2})'
    },

    reportFormat: {
        save: 'Add: New Report Format ({0}) created',
        update: 'Update: Report Format ({0}) updated',
        delete: 'Delete: Report Format ({0}) deleted'
    },

    providerMessages: {
        reply: 'Update: ({0}) Replied to the Patient ({1}) and Refill not taken yet',
        delete: 'Delete: ({0}) Deleted the message of ({1})',
        refillReq: 'Change: Refill Request has been taken by the provider ({0}) for the patient ({1})'
    },
    scheduleBook: {
        reschedule: 'Change: Appointment Type has been Rescheduled from ({0}) to ({1}) for ({2})',
        facilityReschedule: 'Change: Appointment Type has been Rescheduled & Facility changed from ({0}) to ({1}) on ({2}) for modality ({3})',
        update: 'Change: Appointment Type Duration has been changed',
        moveToBucket: 'Change: {0} has been set to Rescheduled status',
        moveOrderToBucket: 'Change: All Studies in Order {0} have been set to rescheduled status'
    },
    queue: {
        save: 'Add: New queue created for Accession No({0}) and studyDesc({1})',
        update: 'Update: Queue has been ({0}) by ({1}) on ({2})',
        patientPortalSave: 'Update: Portal E-mail link has been sent to the patient',
        reprocessQueue: "Reprocessed Sucessfully ",
        reprocesFailed: "Reprocessed  sucessfully for AETitle : ({0}) from ({1}) to ({2}) "
    },

    SRReportQueue: {
        save: 'Add: New queue created for study Accession No({0}) and Report ({1})',
        update: 'Update: Queue TEXT SR Report Queue has been updated ({0})'
    },

    reportQueue: {
        reprocesFailed: "Reprocessed  sucessfully for delivery method ({0}) from  ({1}) to ({2}) ",
        cancel: "Report Queue({0}) status has been canceled",
        change: "Report Queue({0}) status has been changed",
        requeue: "Report Queue({0}) status has been requeued"
    },

    HL7Queue: {
        save: 'Add: {0}: Patient ({1}) with Account No({2}) has been added in HL7 Queue',
        reprocess: 'Update: HL7 sender reprocess and requeue for message ID - ({0}) and interface -({1}) on ({2})',
        retry: 'Update: HL7 sender Retry for message ID-({0}) and interface - ({1}) on ({2})',
        allRetry: 'Update: All HL7 sender has been retry',
        deleted: 'Delete: HL7 sender deleted for message ID-({0})',
        triggerLabOrders: 'Add: Lab orders ({0}) has been added in HL7 Queue',
        triggerLabResults: 'Add: Lab results ({0}) has been added in HL7 Queue',
        triggerImmunization: 'Add: Immunization ({0}) has been added in HL7 Queue'
    },

    tempStudies: {
        update: 'Update: tempStudies AE ({0}) companyId ({1}) updated '
    },
    aeScripts: {
        save: 'Add: New AEScript ({0}) created',
        update: 'Update: AEScripts ({0}) updated',
        delete: 'Delete: AEScripts ({0}) deleted',
        updatePriority: 'Update: AEScripts ({0}) Priority updated'
    },

    dicomServer: {
        save: 'Add: New dicomServer ({0}) created',
        update: 'Update: DicomServer ({0}) updated',
        delete: 'Delete: DicomServer ({0}) deleted'
    },

    DisplayManager: {
        save: 'Add: New Display Manager ({0}) created',
        update: 'Update: Display Manager ({0}) updated',
        delete: 'Delete: Display Manager ({0}) deleted'
    },
    DisplayManagerHpPages: {
        save: 'Add: New Display Manager HpPage ({0}) created',
        update: 'Update: Display Manager HpPage ({0}) updated',
        update_do: 'Update: Display Manager HpPage ({0}) folders display order updated',
        delete: 'Delete: Display Manager HpPage ({0}) deleted'
    },
    MergeStudy: {
        createStudy: 'Add: Merge/Split New Study ({0}) created',
        createSeries: 'Add: Merge/Split New Series created for study ({0})',
        createInstance: 'Add: Merge/Split New Instance created for study ({0})',
        updateStudy: 'Update: Merge/Split update study ({0}) DICOM complete',
        deleteStudy: 'Delete: Merge/Split - Study ({0}) has been deleted as no images associated in it',
        moveSeries: 'Update: Merge/Split Series moved from study ({0}) to ({1})',
        moveInstance: 'Update: Merge/Split Instance moved from study ({0}) to ({1})'
    },
    DisplayManagerHpNewCells: {
        save: 'Add: New MergeStudy ({0}) created',
        update: 'Update: MergeStudy ({0}) updated',
        delete: 'Delete: MergeStudy ({0}) deleted'
    },
    Allergies: {
        save: 'Add: New Allergy ({0}) created',
        update: 'Update: Updated Allergy ({0}) updated',
        delete: 'Delete: Allergy ({0}) deleted'
    },
    OrderForms: {
        save: 'Add: New Order form ({0}) created',
        update: 'Update: Order form SignedBy ({0})',
        delete: 'Delete: Order form ({0}) deleted',
        updateFromPortal: 'Update: Order form content edited by patient ({0}) from patient portal',
        assignToPortal: 'Update: Order form assigned to patient portal',
        unassignFromPortal: 'Update: Order form un-assigned from patient portal'
    },
    EducationMaterials: {
        add: 'Add: Education Material ({0}) is added ',
        delete: 'Delete: Education Material ({0}) is deleted ',
        updateProvided: 'Update: Education Material ({0}) is provided ',
        update: 'Update: Education Material ({0}) is set to false'
    },
    FollowUps: {
        save: 'Add: New FollowUp ({0}) created',
        update: 'Update: FollowUp ({0}) updated',
        delete: 'Delete: FollowUp ({0}) deleted'
    },
    editMyProfile: {
        save: 'Update: Edit MyProfile ({0}) updated'
    },

    immunization: {
        save: 'Add: Immunization {0}' + '(' + '{1}' + ') added',
        update: 'Update: Immunization {0}' + '(' + '{1}' + ') updated',
        delete: 'Delete: Immunization {0} deleted'
    },

    immunizationChart: {
        save: 'Add: New immunization chart ({0}) created',
        update: 'Update: Immunization chart ({0}) updated',
        delete: 'Update: Immunization chart ({0}) deleted'
    },
    outsideReferrals: {
        save: 'Add: New CCD/CCR ({0}) created',
        delete: 'Delete: CCD/CCR ({0}) deleted',
        update: 'Update: CCD/CCR ({0}) updated ',
        add: 'Add: New Recipient ({0}) Added',
        update: 'Update: Update Recipient ({0})'
    },

    insuranceAuthorization: {
        save: 'Add: Exam Authorization for {0} created in {1}',
        update: 'Update: Exam Authorization for {0} updated in {1}',
        delete: 'Delete: Exam Authorization for {0} deleted in {1}'
    },

    chargeandpayment: {
        insertStudyCpt: 'Add: Study Cpt has been added for {0}',
        updateStudyCpt: 'Update: Study Cpt has been updated for {0}',
        insertPaymentComment: 'Add: Payment Comment has been added',
        updatePaymentComment: 'Update: Payment Comment has been updated',
        savePayment: 'Update: ${0} paid by {1}',
        saveReconcilation: 'Update: ${0} paid for {1}',
        updateAppliedPayment: 'Change: Payment Current status changed to {0}',
        updateAppliedReconcilation: 'Update: Reconciliation for ( PA({0}),AD({1}),BL({2}),MODE({3}) ) updated',
        deletePayment: 'Delete: Payment has been deleted',
        deleteCharge: 'Delete: Study Cpt has been deleted',
        insertPayment: 'Add: Payment {0} created in MODE({1}) by {2}',
        updatePayment: 'Update: Payment {0} is updated by {1}',
        updateordericd: 'Update: Order ICD has been updated',
        updatepaymentinorder: 'Update: Payment details updated in order',
        cptreconciliationgroupCode: 'Update : Payment reason updated for ({0})',
        orderPaymentMode: 'Order Payment applied to the payment batch ({0}) in ({1}) mode',
        payerUpdatedAfterResponsibleAssigned: 'Payer({0}) has been updated after Responsible party ({1}) has assigned'
    },

    adjustmentCode: {
        save: 'Add: New adjustment code ({0}) created',
        update: 'Update: Adjustment code ({0}) updated',
        delete: 'Delete: Adjustment code ({0}) deleted'
    },
    providerLevelCode: {
        save: 'Add: New Provider level code ({0}) created',
        update: 'Update: Provider level code ({0}) updated',
        delete: 'Delete: Provider level code ({0}) deleted'
    },

    cptFee: {
        newFeeIns: "Add: CPT Fee added for the cpt ({0}) by the insurance provider ({1})",
        feeUpdateIns: "Update: CPT Fee updated for the insurance Provider ({0})",
        feeDeleteIns: "Delete: Fee info deleted for the cpt ({0})",

        newFeePro: "Add: CPT Fee added for the cpt ({0}) by the provider ({1})",
        feeUpdatePro: "Update: CPT Fee updated for the cpt ({0}) by the Provider/Ord.Facility ({1})",
        feeDeletePro: "Delete: Fee info deleted for the cpt ({0})",

        newFeePG: "Add: CPT Fee added for the cpt ({0}) by the ordering facility ({1})",
        feeUpdatePG: "Update: CPT Fee updated for the ordering facility ({0})",
        feeDeletePG: "Delete: Fee info deleted for the cpt ({0})",
        importSpecfimen: "Update: Update Specimen for cpt ({0}) and test #({1})"
    },

    cptFeeAllowed: {
        newAllowedFeeIns: "Add: Allowed amount added for the cpt ({0}) by the insurance provider ({1})",
        allowedFeeUpdateIns: "Update: Allowed amount updated for the cpt ({0}) by the insurance provider ({1})",
        allowedFeeDeleteIns: "Delete: Allowed amount info deleted for the cpt ({0})",

        newAllowedFeePro: "Add: Allowed amount added for the cpt ({0}) by the provider ({1})",
        allowedFeeUpdatePro: "Update: Allowed amount updated for the cpt ({0}) by the Provider/Ord.Facility ({1})",
        allowedFeeDeletePro: "Delete: Allowed amount deleted for the cpt ({0})",

        newallowedFeePG: "Add: Allowed amount added for the cpt ({0}) by the ordering facility ({1})",
        allowedFeeUpdatePG: "Update: Allowed amount updated for the ordering facility ({0})",
        allowedFeeDeletePG: "Delete: Allowed amount deleted for the cpt ({0})"
    },

    insProviderPayRate: {
        newPayRate: "Add: New pay rate added for the CPT ({0}) for the insurance provider ({1})",
        payRateUpdate: "Update: Pay rate updated for the CPT ({0}) for the insurance provider ({1})",
        deletePayRate: "Delete: Pay rate deleted for the CPT ({0}) for the insurance provider ({1})"
    },

    insProviderFeeUpdate: {
        feeInfoUpdate: "Update: Fee info updated for the cpt ({0}) for the ins.provider ({1})"
    },

    providerFeeUpdate: {
        feeInfoUpdate: "Update: Fee info updated for the cpt ({0}) for the provider ({1})"
    },

    providerGroupFeeUpdate: {
        feeInfoUpdate: "Update: Fee info updated for the cpt ({0}) for the ord.facility ({1})"
    },

    orderImages: {
        otherPhyNewOrder: "Add: New image order added for other physician ({0})",
        otherPhyUpdate: "Update: Image order updated for other physician ({0})",
        otherPhyDeleted: "Delete: Image order deleted for other physician ({0}) for the patient ({1})",

        outsideLocNewOrder: "Add: New outside location image order ({0}) added",
        outsideLocOrderUpdate: "Update: Outside location image order ({0}) updated",
        outsideLocOrderDelete: "Delete: Outside location image order ({0}) Deleted for the patient ({1})",

        patOrderInsert: "Add: New film order added for the patient ({0})",
        patOrderUpdate: "Update: Film order updated for the patient ({0})",

        refPhyOrderInsert: "Add: New film order added for the ref.provider ({0})",
        refPhyOrderUpddate: "Update: Film order updated for the ref.provider ({0})"
    },

    filmorder: {
        cancelupdate: "Update: Film order has been cancelled",
        updatestatus: "Change: Film order status has been changed to {0}"
    },

    rCopiaInfo: {
        rCopiaUpdated: "Update: User ({0}) trying to update RCopia info",
        rCopiaURLupdated: "Update: User ({0}) updated the RCopia urls"
    },

    hl7Sender: {
        newInterfaceCreated: "Add: New HL7 Interface ({0}) created",
        interfaceUpdated: "Update: Interface ({0}) updated",
        interfaceDeleted: "Delete: Interface ({0}) deleted",
        newTriggerCreated: "Add: New trigger ({0}) created for the interface ({1})",
        triggerUpdated: "Update: Trigger ({0}) updated for the interface ({1}) and file saved in {2}",
        triggerDeleted: "Delete: Trigger ({0}) deleted for the interface ({1})"
    },

    importImgCdBurn: {
        downloadImage: "Update: Images downloaded for the patient {0}",
        burnImage: "Update: Images written on the disc for the patient {0}",
        importImage: "Add: Images imported by the user {0}"
    },

    clinicalSummaryTransmit: {
        save: "Update: Clinical Summary has been transmitted from {0} ({1}) to {2} "
    },
    portalUsers: {
        update: "Update : portal credentials for  updated ",
        portalAutoUpdate: "Update :  Portal access already exist. Updated credentials only. Last created dt {0} ",
        portalRegister: "Add : New Portal user request created for ({0}) "

    },
    cmsRegistryInfo: {
        save: 'Add: New Cms registry info ({0}) created',
        update: 'Update: Cms registry info ({0}) updated',
        delete: 'Delete: Cms registry info ({0}) deleted'
    },
    billingProvider: {
        save: "Add: New Billing Provider ({0}) created",
        update: "Update: Billing Provider ({0}) updated",
        delete: 'Delete: Billing Provider ({0}) deleted',
        idCodeSave: "Add: Insurance Provider ({0}) added to the Billing provider ({1})",
        idCodeUpdate: "Update: Insurance Provider ID code details updated for the insurance provider ({0}) by the Billing provider ({1})",
        idCodeDelete: "Delete: Insurance Provider ID Code ({0}) deleted by the Billing provider ({1})"
    },
    claim: {
        save: "Add: New Claim  ({0}) has been created for ({1})",
        submitted: "Update: {0} Claim has been submitted for {1} ",
        billingClass: "Update: Billing class ({0}) updated",
        billingCode: "Update: Billing code ({0}) updated",
        billingStatus: "Update: Billing status ({0}) updated",
        billingClassClear: "Update: Billing class cleared",
        billingCodeClear: "Update: Billing code cleared",
        newCliamPost: "Add: New Claim ({0}) has been posted by changing responsible person to ({1})",
        cliamUpdatePost: "Update: Claim ({0}) has been updated in responsible person to ({1})",
        claimResOF: "Update: Claim ({2}) has been updated in responsible person ({0}) to ({1})",
        claimRevertedReadyToValidate: "Update: Claim ({0}) has been reverted to ready to validate",
        claimValidated: "Add: Claim ({0}) has been validated for responsible ({1})",
        orderMerged: 'Order is created while splitting'
    },
    toBeReviewed: {
        assignedToUser: "Add:({0}) was assigned to the user ({1})",
        reviewed: "Update:({0}) was marked as reviewed"
    },
    cob: {
        feesUpdated: 'Update: Study cpt fees updated for the patient ({0}) from COB'
    },

    portalGuarantor: {
        guarantorSave: 'Add: New guarantor ({0}) added to the patient ({1})',
        guarantorUpdate: 'Update: Guarantor ({0}) updated to the patient ({1})'
    },
    authorisedRep: {
        authorisedRepSave: 'Add: New authorised Rep ({0}) added to the patient ({1})',
        authorisedRepPatChartSave: 'Add: New authorised Rep ({0})',
        authorisedRepUpdate: 'Update: Authorised Rep ({0}) updated to the patient ({1})',
        authorisedRepPatChartUpdate: 'Update: Authorised Rep ({0}) updated'
    },
    reportActions: {
        repPatientReportViewed: "Update: Rep.Patient ({0}) viewed the clinical summary report",
        repPatientReportDownloaded: "Update: Clinical summary report downloaded by the Rep.Patient ({0})",
        patientReportViewed: "Update: Patient ({0}) viewed the clinical summary report",
        patientReportDownloaded: "Update: Clinical summary report downloaded by the Patient ({0})"
    },
    reports: {
        report_queried: 'Query: Data Queried for Printing/Store in PDF/Excel for {0}',
        reportExported: 'Query: Data Queried for Export/Store in CSV',
        report_queryPDF: "Query: Patient Statement for {0} Printed",
        report_queryExcel: "Query: Data Queried for Generating In Excel for {0}",
        Switched: 'Claim status switched from ({0}) to ({1}) for ({2})'
    },
    physicianPortal: {
        orderSigned: 'Update: Order has been signed by the provider ({0}) for the patient ({1})',
        refPhyresetPassword: "Update: Ref.provider recent password has been updated successfully",
        updateFilter: "Update: Worklist filter({0}) has been updated",
        createFilter: "Add: New worklist filter({0}) has been created",
        deleteFilter: "Deleted: Worklist filter({0}) deleted",
        updateMyAccount: "Update: Provider Profile has been updated"

    },
    posMap: {
        posMapSave: "Add: New pos ({0}) Mapped and added",
        posMapDelete: "Delete: POS Map ({0}) deleted",
        posMapUpdate: "Update: POS Map ({0}) updated"
    },
    eob: {
        eobFileCreated: 'An EOB File with the name ({0}) created for processing.',
        eobFileCreatedFromSftp: 'An EOB File with the name ({0}) created for processing from SFTP.',
        orderCreated: 'An order payment has been received for the patient ({0}) with the account no ({1}) from EOB',
        paymentCreatedForOrder: 'Create: Payment for order ({0}) from EOB',
        cptReconcile: 'Insert: Reconciliation for claim ({0}) CPT ({1}) (PA({2}),AD({3}),BL({4}),MODE({5})) from EOB',
        updateCptReconcile: 'Update: Reconciliation for claim ({0}) CPT ({1}) (PA({2}),AD({3}),BL({4}),MODE({5})) updated from EOB',
        cptPaymentReason:'Update : Payment reason updated for ({0})',
        fileDelete: 'File ({0}) has been deleted from ERA Inbox'
    },

    studyLastState: {
        save: 'Last Study State ({0}) created',
        update: 'Last Study State ({0})  updated'
    },
    feeSchedule: {
        save: "Add: New Fee Schedule ({0}) added",
        delete: "Delete: Fee Schedule ({0}) deleted",
        update: 'Fee Schedule ({0})  updated',
        saveFeeScheduleCpt: "Add: New Fee Schedule CPT ({0}) added",
        updateFeeScheduleCpt: "Update: Fee Schedule CPT ({0}) has been updated successfully"
    },
    feeScheduleCpt: {
        save: "Add: New Fee Schedule Cpt ({0}) added",
        delete: "Delete: Fee Schedule Cpt ({0}) deleted",
        update: 'Fee Schedule Cpt ({0})  updated'
    },
    suggestions: {
        save: 'Add: New Suggestion ({0}) created',
        update: 'Update: Suggestion ({0}) updated',
        delete: 'Delete: Suggestion ({0}) deleted'
    },
    cardiologyTemplate: {
        save: 'Add: New Cardiology Template ({0}) created',
        update: 'Update: Cardiology Template ({0}) updated',
        delete: 'Delete Cardiology Template for ({0})'
    },
    cardioTemplate: {
        save: 'Study({0}) study status has been changed to draft',
        approve: 'Study({0}) study status has been changed to approved',
        update:'Study({0}) has been updated'
    },
    orderingFacilityPortal: {
        queried: 'Full data Access: User ({0}) enabled',
        linkedOrdFacility: 'Switched : Facility Portal switched to linked Ordering Facility ({0})',
        OrdFacility: 'Switched : Facility Portal switched to logged Ordering Facility ({0})'
    },
    followUpDate: {
        save: 'FollowUp Date ({0}) Inserted for claim no ({1})',
        update: 'FollowUp Date Updated from ({0}) to ({1}) for claim no ({2})',
        delete: 'FollowUp Date Deleted for claim no ({0})'
    },
    cardiologyKeywords: {
        save: 'Add: New Cardiology Keywords ({0}) created',
        update: 'Update: Cardiology Keywords ({0}) updated',
        suggestionSave: 'Add: New Cardiology Keywords suggestions ({0}) created',
        suggestionUpdate: 'Update: Cardiology Keywords suggestions ({0}) updated',
        delete: 'Delete Cardiology Keywords for ({0})'
    },
    form: {
        fax: 'Fax: {0} form ({1}) for order ({2}) faxed to ({3}) at fax number ({4})'
    },
    document: {
        fax: 'Fax: Document ({0}) faxed to ({1}) at fax number ({2})'
    },
    monthlyGoals: {
        save: 'Created goal for month of {0} of modality {1}',
        update: 'Updated goal for month of {0} of modality {1}',
        delete: 'Deleted goal for month of {0} of modality {1} (goal {2}, working days {3})'
    },

    employmentStatus: {
        save: 'Created employment status of Company {0}',
        update: 'Updated employment status of Company {0}',
        delete: 'Deleted employment status of Company {0}'
    },
    relationshipStatus: {
        save: 'Created Relation of Company {0}',
        update: 'Updated Relation of Company {0}',
        delete: 'Deleted Relation of Company {0}'
    },
    insuranceProviderPayerType: {
        insuranceProviderPayerTypeSave: "Add: New Insurance Provider Payer Type ({0})  and added",
        insuranceProviderPayerTypeDelete: "Delete: Insurance Provider Payer Type ({0}) deleted",
        insuranceProviderPayerTypeUpdate: "Update: Insurance Provider Payer Type ({0}) updated"
    }
};

const moduleNames = {
    'patientKiosk': 'Patient Kiosk',
    'setup': 'Setup',
    'patient': 'Patient',
    'scheduling': 'Schedule',
    'order': 'Order',
    'customer': 'Customer',
    'api': 'api',
    'home': 'Home',
    'billing': 'Billing',
    'vehicle': 'Vehicle',
    'rcopia': 'RCopia',
    'dicomViewer': 'Viewer',
    'peerReview': 'Peer Review',
    'portalRegUsers': 'Portal Registered Users'
};

function getValue ( obj, key ) {
    const value = obj[ key ];
    if ( value ) {
        if ( typeof value === `object` && value ) {
            return getValues(value);
        }
        return [ value ];
    }
    return [];
}

function getValues ( obj ) {
    const keys = Object.keys(obj);
    const values = keys.reduce(( values, key ) => {
        return [
            ...values,
            ...getValue(obj, key)
        ];
    }, []);
    return values;
}

const auditArray = getValues(auditObj);
const moduleArray = getValues(moduleNames);

module.exports = {
    'auditMessages': List(Set(auditArray)),
    'auditModules': List(Set(moduleArray)),
};
