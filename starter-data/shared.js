'use strict';

// const {
//     date: fakeDate
// } = require('faker');

const {
    numberUtils: {
        randomEl,
    },
} = require('../utils');

const dayjs = require('dayjs');

const now = dayjs();

const years = [
    now.format(`YYYY`),
    now.subtract(1, `years`).format(`YYYY`),
    now.subtract(2, `years`).format(`YYYY`),
    now.subtract(3, `years`).format(`YYYY`),
    now.subtract(4, `years`).format(`YYYY`),
    now.subtract(5, `years`).format(`YYYY`),
];

const futureYears = [
    now.format(`YYYY`),
    now.add(1, `years`).format(`YYYY`),
    now.add(2, `years`).format(`YYYY`),
];

const months = [
    `01`,
    `02`,
    `03`,
    `04`,
    `05`,
    `06`,
    `07`,
    `08`,
    `09`,
    `10`,
    `11`,
    `12`,
];

const futureMonths = [
    now.format(`MM`),
    now.add(1, `months`).format(`MM`),
    now.add(2, `months`).format(`MM`),
    now.add(3, `months`).format(`MM`),
    now.add(4, `months`).format(`MM`),
    now.add(5, `months`).format(`MM`),
    now.add(6, `months`).format(`MM`),
    now.add(7, `months`).format(`MM`),
    now.add(8, `months`).format(`MM`),
    now.add(9, `months`).format(`MM`),
    now.add(10, `months`).format(`MM`),
    now.add(11, `months`).format(`MM`),
];

const days = [
    `01`,
    `02`,
    `03`,
    `04`,
    `05`,
    `06`,
    `07`,
    `08`,
    `09`,
    `10`,
    `11`,
    `12`,
    `13`,
    `14`,
    `15`,
    `16`,
    `17`,
    `18`,
    `19`,
    `20`,
    `21`,
    `22`,
    `23`,
    `24`,
    `25`,
    `26`,
    `27`,
    `28`,
];

const futureDays = [
    now.format(`DD`),
    now.add(1, `days`).format(`DD`),
    now.add(2, `days`).format(`DD`),
    now.add(3, `days`).format(`DD`),
    now.add(4, `days`).format(`DD`),
    now.add(5, `days`).format(`DD`),
    now.add(6, `days`).format(`DD`),
    now.add(7, `days`).format(`DD`),
    now.add(8, `days`).format(`DD`),
    now.add(9, `days`).format(`DD`),
    now.add(10, `days`).format(`DD`),
    now.add(11, `days`).format(`DD`),
    now.add(12, `days`).format(`DD`),
    now.add(13, `days`).format(`DD`),
    now.add(14, `days`).format(`DD`),
    now.add(15, `days`).format(`DD`),
    now.add(16, `days`).format(`DD`),
    now.add(17, `days`).format(`DD`),
    now.add(18, `days`).format(`DD`),
    now.add(19, `days`).format(`DD`),
    now.add(20, `days`).format(`DD`),
    now.add(21, `days`).format(`DD`),
    now.add(22, `days`).format(`DD`),
    now.add(23, `days`).format(`DD`),
    now.add(24, `days`).format(`DD`),
    now.add(25, `days`).format(`DD`),
    now.add(26, `days`).format(`DD`),
    now.add(27, `days`).format(`DD`),
];

const hours = [
    `00`,
    `01`,
    `02`,
    `03`,
    `04`,
    `05`,
    `06`,
    `07`,
    `08`,
    `09`,
    `10`,
    `11`,
    `12`,
    `13`,
    `14`,
    `15`,
    `16`,
    `17`,
    `18`,
    `19`,
    `20`,
    `21`,
    `22`,
    `23`
];

const futureHours = [
    now.format(`HH`),
    now.add(1, `hours`).format(`HH`),
    now.add(2, `hours`).format(`HH`),
    now.add(3, `hours`).format(`HH`),
    now.add(4, `hours`).format(`HH`),
    now.add(5, `hours`).format(`HH`),
    now.add(6, `hours`).format(`HH`),
    now.add(7, `hours`).format(`HH`),
    now.add(8, `hours`).format(`HH`),
    now.add(9, `hours`).format(`HH`),
    now.add(10, `hours`).format(`HH`),
    now.add(11, `hours`).format(`HH`),
    now.add(12, `hours`).format(`HH`),
    now.add(13, `hours`).format(`HH`),
    now.add(14, `hours`).format(`HH`),
    now.add(15, `hours`).format(`HH`),
    now.add(16, `hours`).format(`HH`),
    now.add(17, `hours`).format(`HH`),
    now.add(18, `hours`).format(`HH`),
    now.add(19, `hours`).format(`HH`),
    now.add(20, `hours`).format(`HH`),
    now.add(21, `hours`).format(`HH`),
    now.add(22, `hours`).format(`HH`),
    now.add(23, `hours`).format(`HH`),
];

const minutes = [
    `00`,
    `01`,
    `02`,
    `03`,
    `04`,
    `05`,
    `06`,
    `07`,
    `08`,
    `09`,
    `10`,
    `11`,
    `12`,
    `13`,
    `14`,
    `15`,
    `16`,
    `17`,
    `18`,
    `19`,
    `20`,
    `21`,
    `22`,
    `23`,
    `24`,
    `25`,
    `26`,
    `27`,
    `28`,
    `29`,
    `30`,
    `31`,
    `32`,
    `33`,
    `34`,
    `35`,
    `36`,
    `37`,
    `38`,
    `39`,
    `40`,
    `41`,
    `42`,
    `43`,
    `44`,
    `45`,
    `46`,
    `47`,
    `48`,
    `49`,
    `50`,
    `51`,
    `52`,
    `53`,
    `54`,
    `55`,
    `56`,
    `57`,
    `58`,
    `59`,
];

const futureMinutes = [
    now.format(`mm`),
    now.add(1, `minutes`).format(`mm`),
    now.add(2, `minutes`).format(`mm`),
    now.add(3, `minutes`).format(`mm`),
    now.add(4, `minutes`).format(`mm`),
    now.add(5, `minutes`).format(`mm`),
    now.add(6, `minutes`).format(`mm`),
    now.add(7, `minutes`).format(`mm`),
    now.add(8, `minutes`).format(`mm`),
    now.add(9, `minutes`).format(`mm`),
    now.add(10, `minutes`).format(`mm`),
    now.add(11, `minutes`).format(`mm`),
    now.add(12, `minutes`).format(`mm`),
    now.add(13, `minutes`).format(`mm`),
    now.add(14, `minutes`).format(`mm`),
    now.add(15, `minutes`).format(`mm`),
    now.add(16, `minutes`).format(`mm`),
    now.add(17, `minutes`).format(`mm`),
    now.add(18, `minutes`).format(`mm`),
    now.add(19, `minutes`).format(`mm`),
    now.add(20, `minutes`).format(`mm`),
    now.add(21, `minutes`).format(`mm`),
    now.add(22, `minutes`).format(`mm`),
    now.add(23, `minutes`).format(`mm`),
    now.add(24, `minutes`).format(`mm`),
    now.add(25, `minutes`).format(`mm`),
    now.add(26, `minutes`).format(`mm`),
    now.add(27, `minutes`).format(`mm`),
    now.add(28, `minutes`).format(`mm`),
    now.add(29, `minutes`).format(`mm`),
    now.add(30, `minutes`).format(`mm`),
    now.add(31, `minutes`).format(`mm`),
    now.add(32, `minutes`).format(`mm`),
    now.add(33, `minutes`).format(`mm`),
    now.add(34, `minutes`).format(`mm`),
    now.add(35, `minutes`).format(`mm`),
    now.add(36, `minutes`).format(`mm`),
    now.add(37, `minutes`).format(`mm`),
    now.add(38, `minutes`).format(`mm`),
    now.add(39, `minutes`).format(`mm`),
    now.add(40, `minutes`).format(`mm`),
    now.add(41, `minutes`).format(`mm`),
    now.add(42, `minutes`).format(`mm`),
    now.add(43, `minutes`).format(`mm`),
    now.add(44, `minutes`).format(`mm`),
    now.add(45, `minutes`).format(`mm`),
    now.add(46, `minutes`).format(`mm`),
    now.add(47, `minutes`).format(`mm`),
    now.add(48, `minutes`).format(`mm`),
    now.add(49, `minutes`).format(`mm`),
    now.add(50, `minutes`).format(`mm`),
    now.add(51, `minutes`).format(`mm`),
    now.add(52, `minutes`).format(`mm`),
    now.add(53, `minutes`).format(`mm`),
    now.add(54, `minutes`).format(`mm`),
    now.add(55, `minutes`).format(`mm`),
    now.add(56, `minutes`).format(`mm`),
    now.add(57, `minutes`).format(`mm`),
    now.add(58, `minutes`).format(`mm`),
    now.add(59, `minutes`).format(`mm`),
];

const notesForLater = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "notes": {
                "type": "string"
            },
            "created_dt": {
                "type": "string",
                "format": "date-time"
            },
            "created_by": {
                "type": "string",
                "faker": "name.findName"
            }
        },
        "required": [
            "created_dt",
            "created_by",
            "notes"
        ]
    },
    "minItems": 0,
    "maxItems": 7,
    "uniqueItems": true
};

const email = {
    "type": "string",
    "faker": "internet.email"
};

const phone = {
    "type": "string",
    "faker": "phone.phoneNumberFormat"
};

const address1 = {
    "type": `string`,
    "faker": `address.streetAddress`
};

const address2 = {
    "type": "string",
    "faker": "address.secondaryAddress"
};

const city = {
    "type": "string",
    "faker": "address.city"
};

const state = {
    "type": "string",
    "faker": "address.state"
};

const zip = {
    "type": "string",
    "faker": "address.zipCode"
};

const website = {
    "type": `string`,
    "faker": `internet.url`
};

const stateAbbr = {
    "type": "string",
    "faker": "address.stateAbbr"
};

const gender = {
    "type": `string`,
    "enum": [
        `F`,
        `F`,
        `F`,
        `F`,
        `F`,
        `F`,
        `F`,
        `M`,
        `M`,
        `M`,
        `M`,
        `M`,
        `M`,
        `M`,
        `O`,
        `U`
    ]
};

const birth_date = {
    "type": `string`,
    "chance": {
        "birthday": {
            "string": true
        }
    }
};

const medicareInsuranceTypeCode = {
    "type": `string`,
    "enum": [
        `12`,
        `13`,
        `14`,
        `15`,
        `16`,
        `41`,
        `42`,
        `43`,
        `47`,
    ],
};

/*const empStatus = {
    "type": "string",
    "enum": [
        "Employeed",
        "Unemployeed",
        "Full-time school",
        "Part-time work"
    ]
};*/

const timezones = [
    `05:00`,
    `06:00`,
    `07:00`,
    `08:00`,
];

const birthYears = [
    dayjs(`1901`).format(`YYYY`)
];
while ( birthYears[ birthYears.length - 1 ] < now.subtract(1, 'year').year() ) {
    birthYears.push(String(~~birthYears[ birthYears.length - 1 ] + 1));
}

const makeDate = () =>
    `${randomEl(years)}-${randomEl(months)}-${randomEl(days)}`;

const makeBirthDate = () =>
    `${randomEl(birthYears)}-${randomEl(months)}-${randomEl(days)}`;

const makeTime = () =>
    `${randomEl(hours)}:${randomEl(minutes)}:00`;

const makeTimestamp = () =>
    `${makeDate()}T${makeTime()}`;

const makeTimestampTz = () =>
    `${makeTimestamp()}-${randomEl(timezones)}`;

// Have to use `days` instead of `futureDays` for now - doesnt account for month right now
const makeFutureDate = () =>
    `${randomEl(futureYears)}-${randomEl(futureMonths)}-${randomEl(days)}`;

const makeFutureTime = () =>
    `${randomEl(futureHours)}:${randomEl(futureMinutes)}:00`;

const makeFutureTimestamp = () =>
    `${makeFutureDate()}T${makeFutureTime()}`;

const makeFutureTimestampTz = () =>
    `${makeFutureTimestamp()}-${randomEl(timezones)}`;

/*const makeFutureDate = () =>
    fakeDate.future()
        .toISOString()
        .slice(0, 10);*/

module.exports = {
    years,
    futureYears,
    months,
    futureMonths,
    days,
    futureDays,
    hours,
    futureHours,
    minutes,
    futureMinutes,
    notesForLater,
    email,
    phone,
    address1,
    address2,
    city,
    state,
    stateAbbr,
    zip,
    website,
    gender,
    birth_date,
    // empStatus,
    medicareInsuranceTypeCode,
    makeDate,
    makeBirthDate,
    makeTime,
    makeTimestamp,
    makeTimestampTz,
    makeFutureDate,
    makeFutureTime,
    makeFutureTimestamp,
    makeFutureTimestampTz,
    timezones,
};
