'use strict';

const {
    List
} = require('immutable');

const moment = require('moment');
const DATE_TIME_FORMAT = `YYYYMMDDHHmmss`;
const memoize = require('fast-memoize');

const {
    networkInterfaces
} = require('os');

const nics = networkInterfaces();
const nicNames = Object.keys(nics);
const loIndex = nicNames.indexOf(`lo`);

nicNames.splice(loIndex, 1);

const macAddr = {
    EMPTY_MAC_ADDR: `00:00:00:00:00:00`,
    regMac: /(^[^A-Za-z1-9]|[^A-Za-z0-9])/g,
    addr: ``,
    setAddr: function () {
        const {
            EMPTY_MAC_ADDR,
            regMac
        } = this;

        const total = nicNames.length;
        let a = 0;
        for ( ; a < total; ++a ) {
            const name = nicNames[ a ];
            const nicArray = nics[ name ];
            const total = nicArray.length;
            let i = 0;
            for ( ; i < total; ++i ) {
                const {
                    mac
                } = nicArray[ i ];

                if ( mac && mac !== EMPTY_MAC_ADDR ) {
                    const newMAC = mac
                        .split(/:|-/)
                        .map(id => parseInt(id, 16).toString().replace(regMac, ``))
                        .join('');

                    if ( newMAC ) {
                        this.addr = newMAC;
                        return this;
                    }
                }
            }

        }

        this.addr = `1`;
        return this;
    },

    getAddr: function () {
        return this.addr || this.setAddr().addr;
    }
};

process.nextTick(
    () => macAddr.setAddr()
);

const getNumbers = ( numbers, e ) => {
    while ( e < 10 ) {
        numbers[ e ] = e++;
    }
    return numbers;
};

const numbers = getNumbers(new Uint8Array(10), 0);

/**
 * @param   {Immutable.List}    list
 * @return {any}
 */
const randomElFromList = ( list = List() ) => {
    const randomNumber = ~~(Math.random() * list.size);
    return list.get(randomNumber);
};

const randomEl = ( array = numbers, count = 1 ) => {
    let string = ``;
    const arraySize = array.length;
    if ( arraySize > 0 ) {
        while ( count-- > 0 ) {
            const randomNumber = ~~(Math.random() * arraySize);
            string += String(array[ randomNumber ]);
        }
    }
    return string;
};

const aNumber = count =>
    ~~randomEl(numbers, count);

const nonZeroNumberInit = numbers =>
    count => ~~randomEl(numbers, count);

/**
 *
 * @param {number}      scale       - the higher, the more likely
 * @param {number}      lowEnd      - the higher, the less likely
 * @return {boolean}
 */
const beatTheOdds = ( scale = 96, lowEnd = 12 ) =>
    Math.random() * scale > lowEnd;

const nonZeroNumber = nonZeroNumberInit(numbers.slice(1));

const numberUtils = {
    numbers,
    randomEl,
    aNumber,
    nonZeroNumber,
    beatTheOdds
};

const lettersArray = [ `A`, `B`, `C`, `D`, `E`, `F`, `G`, `H`, `I`, `J`, `K`, `L`, `M`, `N`, `O`, `P`, `Q`, `R`, `S`, `T`, `U`, `V`, `W`, `X`, `Y`, `Z` ];

// For use with .charAt() and an index 0-9
const shortLetterString = `ABCDEFGHIJ`;

const aLetter = ( count = 1 ) => {
    let string = ``;
    while ( string.length < count ) {
        string += `${shortLetterString.charAt(aNumber())}`;
    }
    return string;
}

const letterUtils = {
    lettersArray,
    shortLetterString,
    aLetter
};

const makeUidInit = ( macNo = macAddr.getAddr() ) => flag => {

    let uidFlag;
    switch ( flag ) {
        case `SERIES`:
            uidFlag = `1.`;
            break;
        case `INSTANCE`:
            uidFlag = `2.`;
            break;
        default:
            uidFlag = ``;
            break;
    }

    const uidStart = `1.3.6.1.4.1.11157.${uidFlag}${macNo}`;

    let previousTime;
    let currentTime = 0;

    const regMultiDot = /\.{2,}/g;

    return ( date = moment() ) => {

        const datestring = date.format(DATE_TIME_FORMAT);

        if ( datestring == previousTime ) {
            currentTime += 1;
        }

        previousTime = datestring;

        const hrTime = process.hrtime();

        const uid = `${uidStart}.${datestring}`;

        const longUID = `${uid}.${hrTime[ 0 ]}.${hrTime[ 1 ]}.${currentTime}`;

        if ( longUID.length > 64 ) {
            const shorterUID = `${uid}.${hrTime[ 1 ]}.${currentTime}`
                .slice(0, 64)
                .replace(regMultiDot, `.`);
            return shorterUID.charAt(64) !== `.`
                ? shorterUID.slice(0, 64)
                : `${shorterUID.slice(0, 63)}0`;
        }

        return longUID.replace(regMultiDot, `.`);
    };
};

const makeUid = makeUidInit(macAddr.setAddr().addr);
const makeUidSeries = makeUid(`SERIES`);
const makeUidInstance = makeUid(`INSTANCE`);
const makeUidStudy = makeUid();

/**
 * Stolen from EXA
 */
function sanitize_input ( input ) {
    // http://www.postgresql.org/docs/9.0/static/sql-syntax-lexical.html [4.1.2.1-4.1.2.2]
    return input
    // single quotes (') must be replaced with double single quotes ('')
        .replace(/'/g, `''`)
        // backslashes (\) must be replaced with double backslashes (\\)
        .replace(/\\/g, '\\\\')
        // double quotes (") must be replaced with escaped quotes (\\")
        .replace(/"/g, '\\"');
}

/**
 * Stolen from EXA
 */
function to_string ( input, sanitize ) {
    switch ( typeof input ) {
        case 'boolean':
        case 'number':
        case 'object':
            return String(input);
        case 'string':
            return sanitize ? sanitize_input(input) : input;
        default:
            return '';
    }
}

/**
 * Stolen from EXA (sort of)
 */
const hstoreStringify = function (data) {
    if ( typeof data === `object` ) {
        const keys = Object.keys(data);
        const hstore = keys.map(key =>
            data[ key ] === null
                ? `""${to_string(key, true)}""=>NULL`
                : `""${to_string(key, true)}""=>""${to_string(data[ key ], true)}""`
        );
        return `"${hstore.join()}"`;
    }
    return ``;
};

const getHexCode = () =>
    (~~(Math.random() * 255))
        .toString(16)
        .padStart(2, `0`);

const getColor = () => [
    getHexCode(),
    getHexCode(),
    getHexCode()
].join(``).toUpperCase();

// Cancelled can be both so leaving off for now
const regScheduledOrderStatuses = /SCH|RSCH|ABRT|CON|CHI|CHO|NOS/i;

const checkStatus = memoize(status_code =>
    !regScheduledOrderStatuses.test(status_code)
);

const statusBeforeSchedule = status => {
    const {
        order_related,
        status_code,
    } = status;

    if ( order_related && checkStatus(status_code) ) {
        return true;
    }

    return false;
};

const TIMESTAMP_FORMAT = `YYYY-MM-DDTHH:mm:ss`;

module.exports = {
    letterUtils,
    numberUtils,
    randomElFromList,
    makeUidSeries,
    makeUidInstance,
    makeUidStudy,
    hstoreStringify,
    getHexCode,
    getColor,
    statusBeforeSchedule,
    TIMESTAMP_FORMAT,
};
