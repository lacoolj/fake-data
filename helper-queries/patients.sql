DROP TABLE IF EXISTS public.patients_t;
CREATE TABLE public.patients_t (LIKE public.patients INCLUDING ALL);

INSERT INTO
    public.patients_t
SELECT
    *
FROM
    public.patients;

ALTER TABLE public.patients_t
    ADD COLUMN "commPref" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "is_animal" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "maritalStatus" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "ethnic" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "height" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "weight" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "licenseNo" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "licenseState" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "expiryDate" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "language" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "racial" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1Email" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2Email" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1HomePhone" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2HomePhone" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1MobilePhone" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2MobilePhone" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1AddressLine1" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2AddressLine1" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1AddressLine2" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2AddressLine2" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1City" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2City" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1State" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2State" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1WorkPhone" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2WorkPhone" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c1Zip" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "c2Zip" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "employerAddress" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "enableVerterinary" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "employerName" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "resonForDeath" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "ssn" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "empStatus" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "providerfacilities" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "employerPhone" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "patientFlag" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "mothers_maiden" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "deathReason" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "providername" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "providerID" TEXT;
ALTER TABLE public.patients_t
    ADD COLUMN "smokingStatus" TEXT;


SELECT
    setval(
        'patients_id_seq',
        (
            SELECT
                max(id)
            FROM
                public.patients
        ) + 1
    );

-- Only run after running COPY FROM using export CSVs.

UPDATE
    public.patients_t
SET
    patient_info = COALESCE(patient_info, hstore('')) ||
       hstore('commPref', COALESCE(patient_info -> 'commPref', "commPref", '')) ||
       hstore('is_animal', COALESCE(patient_info -> 'is_animal', "is_animal", '')) ||
       hstore('maritalStatus', COALESCE(patient_info -> 'maritalStatus', "maritalStatus", '')) ||
       hstore('ethnic', COALESCE(patient_info -> 'ethnic', "ethnic", '')) ||
       hstore('height', COALESCE(patient_info -> 'height', "height", '')) ||
       hstore('weight', COALESCE(patient_info -> 'weight', "weight", '')) ||
       hstore('licenseNo', COALESCE(patient_info -> 'licenseNo', "licenseNo", '')) ||
       hstore('licenseState', COALESCE(patient_info -> 'licenseState', "licenseState", '')) ||
       hstore('expiryDate', COALESCE(patient_info -> 'expiryDate', "expiryDate", '')) ||
       hstore('language', COALESCE(patient_info -> 'language', "language", '')) ||
       hstore('racial', COALESCE(patient_info -> 'racial', "racial", '')) ||
       hstore('c1Email', COALESCE(patient_info -> 'c1Email', "c1Email", '')) ||
       hstore('c2Email', COALESCE(patient_info -> 'c2Email', "c2Email", '')) ||
       hstore('c1HomePhone', COALESCE(patient_info -> 'c1HomePhone', "c1HomePhone", '')) ||
       hstore('c2HomePhone', COALESCE(patient_info -> 'c2HomePhone', "c2HomePhone", '')) ||
       hstore('c1MobilePhone', COALESCE(patient_info -> 'c1MobilePhone', "c1MobilePhone", '')) ||
       hstore('c2MobilePhone', COALESCE(patient_info -> 'c2MobilePhone', "c2MobilePhone", '')) ||
       hstore('c1AddressLine1', COALESCE(patient_info -> 'c1AddressLine1', "c1AddressLine1", '')) ||
       hstore('c2AddressLine1', COALESCE(patient_info -> 'c2AddressLine1', "c2AddressLine1", '')) ||
       hstore('c1AddressLine2', COALESCE(patient_info -> 'c1AddressLine2', "c1AddressLine2", '')) ||
       hstore('c2AddressLine2', COALESCE(patient_info -> 'c2AddressLine2', "c2AddressLine2", '')) ||
       hstore('c1City', COALESCE(patient_info -> 'c1City', "c1City", '')) ||
       hstore('c2City', COALESCE(patient_info -> 'c2City', "c2City", '')) ||
       hstore('c1State', COALESCE(patient_info -> 'c1State', "c1State", '')) ||
       hstore('c2State', COALESCE(patient_info -> 'c2State', "c2State", '')) ||
       hstore('c1WorkPhone', COALESCE(patient_info -> 'c1WorkPhone', "c1WorkPhone", '')) ||
       hstore('c2WorkPhone', COALESCE(patient_info -> 'c2WorkPhone', "c2WorkPhone", '')) ||
       hstore('c1Zip', COALESCE(patient_info -> 'c1Zip', "c1Zip", '')) ||
       hstore('c2Zip', COALESCE(patient_info -> 'c2Zip', "c2Zip", '')) ||
       hstore('employerAddress', COALESCE(patient_info -> 'employerAddress', "employerAddress", '')) ||
       hstore('enableVerterinary', COALESCE(patient_info -> 'enableVerterinary', "enableVerterinary", '')) ||
       hstore('employerName', COALESCE(patient_info -> 'employerName', "employerName", '')) ||
       hstore('resonForDeath', COALESCE(patient_info -> 'resonForDeath', "resonForDeath", '')) ||
       hstore('ssn', COALESCE(patient_info -> 'ssn', "ssn", '')) ||
       hstore('empStatus', COALESCE(patient_info -> 'empStatus', "empStatus", '')) ||
       hstore('providerfacilities', COALESCE(patient_info -> 'providerfacilities', "providerfacilities", '')) ||
       hstore('employerPhone', COALESCE(patient_info -> 'employerPhone', "employerPhone", '')) ||
       hstore('patientFlag', COALESCE(patient_info -> 'patientFlag', "patientFlag", '')) ||
       hstore('mothers_maiden', COALESCE(patient_info -> 'mothers_maiden', "mothers_maiden", '')) ||
       hstore('deathReason', COALESCE(patient_info -> 'deathReason', "deathReason", '')) ||
       hstore('providername', COALESCE(patient_info -> 'providername', "providername", '')) ||
       hstore('providerID', COALESCE(patient_info -> 'providerID', "providerID", '')) ||
       hstore('smokingStatus', COALESCE(patient_info -> 'smokingStatus', "smokingStatus", ''));

