-- ============================================
-- New order, one study + schedule, new patient
-- ============================================

INSERT INTO patients (dicom_patient_id, first_name, middle_name, last_name, suffix_name, birth_date, gender, patient_info, company_id, facility_id, account_no, alt_account_no, last_edit_by, account_no_history, patient_type, is_active, has_deleted, full_name, owner_id, patient_details, ordering_facilities, portal_info, alerts)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11,
        $12,
        $13,
        $14,
        $15,
        $16,
        $17,
        $18,
        $19,
        $20,
        $21,
        $22,
        $23 )
RETURNING "id";

INSERT INTO patient_insuarances (coverage_level, insurance_provider_id, subscriber_relationship, subscriber_info, patient_id, has_deleted)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        FALSE )
RETURNING id;

INSERT INTO orders (patient_id, facility_id, modality_id, modality_room_id, order_info, order_notes, order_type, icd_codes, scheduled_dt, referring_provider_ids, referring_providers, is_quick_appt, ordered_by, ordered_dt, order_status, order_status_desc, insurance_provider_ids, insurance_providers, company_id, duration, scheduled_date, vehicle_id, technologist_id, has_deleted, mu_validation_data, icd_code_ids_billing)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11,
        $12,
        $13,
        $14,
        $15,
        $16,
        $17,
        $18,
        $19,
        $20,
        $21,
        $22,
        $23,
        FALSE,
        $24,
        $25 )
RETURNING "id";

INSERT INTO patient_icds (icd_id, current_status, onset_date, inactive_from, request_status, rcopia_id, external_id, more_info, patient_id, has_deleted, order_id)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11 )
RETURNING "id";

INSERT INTO studies (study_dt, patient_id, modality_id, facility_id, order_id, body_part, reason_for_study, study_info, orientation, study_created_dt, has_deleted, referring_physician_id, reading_physician_id, accession_no, department, dicom_study_id, study_details, study_status, patient_age, no_of_instances, procedure_id, study_description, cpt_codes, company_id, modalities, dicom_status, notes, duration, institution, provider_group_id, status_last_changed_dt, study_uid, tat_level, priority, stat_level, status_last_changed_by, is_qc_study)
    SELECT
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        CURRENT_TIMESTAMP,
        FALSE,
        $10,
        $11,
        $12,
        $13,
        $14,
        $15,
        $16,
        $17,
        $18,
        $19,
        $20,
        $21,
        $22,
        $23,
        $24,
        $25,
        $26,
        $27,
        $28,
        CURRENT_TIMESTAMP,
        $29,
        0,
        $30,
        $31,
        $32,
        FALSE
RETURNING "id", study_details;

INSERT INTO study_cpt (study_id, cpt_code_id, cpt_code, study_cpt_info, has_deleted, order_id, billing_fee, authorization_info)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        FALSE,
        $5,
        $6,
        $7 )
RETURNING id;


INSERT INTO payment_comments (code, study_cpt_id, comment, order_id, more_info, comment_by, comment_dt)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        now() )
RETURNING id;

UPDATE orders
SET
    referring_provider_ids = $1,
    reading_providers = $2,
    referring_providers = $3
WHERE
    id = $4
RETURNING id, order_info, referring_provider_ids [1] AS referring_provider_id, facility_id, patient_id, company_id;

UPDATE orders
SET
    order_info = order_info || hstore(ARRAY ['balance', 'bill_fee', 'allowed_fee', 'billing_method', 'payer_type', 'payer_billing_method', 'payer_id', 'payer_name', 'billing_payer_name', 'billing_payer_id', 'billing_payer_type', 'payer', 'adjustment'], ARRAY [$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13])
WHERE
    id = $14
RETURNING order_status;

UPDATE orders
SET
    order_info = order_info || hstore('pos', $1) || hstore('orderContact', $2) || hstore('dispatch_contact_name', $3) || hstore('dispatch_contact_number', $4) || hstore('dispatch_address', $5) || hstore('pos_type_code', $6) || hstore('billing_provider', $8) || hstore('ordering_facility', $9) || hstore('ordering_facility_id', $10)
WHERE
    id = $7
RETURNING id;

-- ============================================
-- New order, one study, existing patient
-- ============================================

UPDATE patients
SET
    dicom_patient_id = $1,
    first_name = $2,
    middle_name = $3,
    last_name = $4,
    suffix_name = $5,
    birth_date = $6,
    gender = $7,
    patient_info = $8,
    account_no_history = $9,
    facility_id = $10,
    account_no = $11,
    alt_account_no = $12,
    owner_id = $13,
    patient_type = $14,
    full_name = $15,
    patient_details = $16,
    notes = $17,
    ordering_facilities = $19
WHERE
    id = $18
RETURNING "id";
INSERT INTO orders (patient_id, facility_id, modality_id, modality_room_id, order_info, order_notes, order_type, icd_codes, scheduled_dt, referring_provider_ids, referring_providers, is_quick_appt, ordered_by, ordered_dt, order_status, order_status_desc, insurance_provider_ids, insurance_providers, company_id, duration, scheduled_date, vehicle_id, technologist_id, has_deleted, mu_validation_data, icd_code_ids_billing)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11,
        $12,
        $13,
        $14,
        $15,
        $16,
        $17,
        $18,
        $19,
        $20,
        $21,
        $22,
        $23,
        FALSE,
        $24,
        $25 )
RETURNING "id";
INSERT INTO patient_icds (icd_id, current_status, onset_date, inactive_from, request_status, rcopia_id, external_id, more_info, patient_id, has_deleted, order_id)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11 )
RETURNING "id";
INSERT INTO studies (study_dt, patient_id, modality_id, facility_id, order_id, body_part, reason_for_study, study_info, orientation, study_created_dt, has_deleted, referring_physician_id, reading_physician_id, accession_no, department, dicom_study_id, study_details, study_status, patient_age, no_of_instances, procedure_id, study_description, cpt_codes, company_id, modalities, dicom_status, notes, duration, institution, provider_group_id, status_last_changed_dt, study_uid, tat_level, priority, stat_level, status_last_changed_by, is_qc_study)
    SELECT
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        CURRENT_TIMESTAMP,
        FALSE,
        $10,
        $11,
        $12,
        $13,
        $14,
        $15,
        $16,
        $17,
        $18,
        $19,
        $20,
        $21,
        $22,
        $23,
        $24,
        $25,
        $26,
        $27,
        $28,
        CURRENT_TIMESTAMP,
        $29,
        0,
        $30,
        $31,
        $32,
        FALSE
RETURNING "id", study_details;
INSERT INTO payment_comments (code, study_cpt_id, comment, order_id, more_info, comment_by, comment_dt)
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        now() )
RETURNING id
