SELECT
    ( SELECT COUNT(1) FROM file_stores )         AS file_stores_count,
    ( SELECT COUNT(1) FROM facilities )          AS facilities_count,
    ( SELECT COUNT(1) FROM modality_rooms )      AS modality_rooms_count,
    ( SELECT COUNT(1) FROM providers )           AS providers_count,
    ( SELECT COUNT(1) FROM provider_contacts )   AS provider_contacts_count,
    ( SELECT COUNT(1) FROM users )               AS users_count,
    ( SELECT COUNT(1) FROM insurance_providers ) AS insurance_providers_count,
    ( SELECT COUNT(1) FROM hl7_interfaces )      AS hl7_interfaces_count,
    ( SELECT COUNT(1) FROM hl7_receivers )       AS hl7_receivers_count,
    ( SELECT COUNT(1) FROM hl7_messages )        AS hl7_messages_count,
    ( SELECT COUNT(1) FROM patients )               AS patients_count,
    ( SELECT COUNT(1) FROM patient_insurances )     AS patient_insurances_count,
    ( SELECT COUNT(1) FROM orders )                 AS orders_count,
    ( SELECT COUNT(1) FROM studies )                AS studies_count,
    ( SELECT COUNT(1) FROM study_cpt )              AS study_cpt_count,
    ( SELECT COUNT(1) FROM patient_documents )      AS patient_documents_count,
    ( SELECT COUNT(1) FROM study_forms )            AS study_forms_count,
    ( SELECT COUNT(1) FROM billing.payments )               AS payments_count,
--(SELECT COUNT(1) FROM call_logs) AS call_logs_count,
    ( SELECT COUNT(1) FROM study_transcriptions )   AS study_transcriptions_count,
    ( SELECT COUNT(1) FROM audit_log )              AS audit_log_count,
    ( SELECT COUNT(1) FROM study_series )           AS study_series_count,
    ( SELECT COUNT(1) FROM study_series_instances ) AS study_series_instances_count,
    ( SELECT COUNT(1) FROM hl7_receiver_log )       AS hl7_receiver_log_count,
    ( SELECT COUNT(1) FROM hl7_queue )              AS hl7_queue_count;

-- all databases and their sizes
SELECT
    datname,
    pg_size_pretty(pg_database_size(datname))
FROM
    pg_database
ORDER BY
    pg_database_size(datname) DESC;

-- all tables and their size, ordered by schema then size
SELECT
    schemaname || '.' || tablename                                AS "tableName",
    pg_size_pretty(pg_table_size(schemaname || '.' || tablename)) AS "tableSize"
FROM
    pg_tables
ORDER BY
    schemaname ASC,
    pg_table_size(schemaname || '.' || tablename) DESC;

ALTER TABLE patient_insurances
    ALTER COLUMN subscriber_dob DROP NOT NULL;
ALTER TABLE patient_insurances
    ALTER COLUMN subscriber_firstname DROP NOT NULL;
ALTER TABLE patient_insurances
    ALTER COLUMN subscriber_lastname DROP NOT NULL;
