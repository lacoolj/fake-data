'use strict';

const {
    List,
    Set
} = require('immutable');

const {
    symbolMap,
    dbMap
} = require('./helpers');

const {
    query
} = require('./db');

const {
    logInfo,
    logTrace,
    logTime,
    logTimeEnd,
} = require('./loggers');

class Transformer {
    /**
     * Gets an object of settings and helpers to pull dependent or main data
     * from respective tables.
     * @param   {string} dataType - Identifier of type of data needed - can be typeName or tableName
     * @return  {Object}
     */
    static getDataHelper ( dataType ) {
        const lookupSymbol = symbolMap.get(dataType);
        const table = dbMap.get(lookupSymbol);
        return table;
    }

    constructor ( type, subType ) {
        const table = Transformer.getDataHelper(type);
        /*const lookupSymbol = symbolMap.get(type);
        const table = dbMap.get(lookupSymbol);*/

        const {
            schemaName = `public`,
            dependencies,
            typeName,
            tableName,
            getSchemaInit
        } = table;

        this.schemaName = schemaName;
        this.dependencies = dependencies;
        this.typeName = typeName;
        this.subType = subType;
        this.tableName = tableName;
        this.getSchemaInit = getSchemaInit;

    }

    async processData () {
        logInfo(`Calling DB for dependent data`);

        logTime(`DB total time`);
        const dependentData = await this.getTables(this.dependencies);
        logTimeEnd(`DB total time`);

        logTime(`Processing data from DB to shallow normalize`);
        const processedData = this.reduceDataSets(dependentData);
        logTimeEnd(`Processing data from DB to shallow normalize`);

        this.getSchema = this.getSchemaInit(processedData);

        const blankSchema = this.getSchema({
            index: 0,
            subType: this.subType
        });

        this.fieldList = Object.keys(blankSchema).slice(0, -1);

        return this;
    }

    async getTables () {
        const deps = [];
        for ( let tableName of this.dependencies ) {
            deps.push(await this.getTable(tableName));
        }
        return deps;

        // const dependentPromises = this.dependencies.map(async tableName => await this.getTable(tableName));
        // return dependentPromises;
        //
        // return await Promise
        //     .all(dependentPromises)
        //     .catch(( error ) => {
        //         logTrace(`Could not load dependent data from`, error);
        //         return [];
        //     });
    }

    getTable ( tableName ) {
        const table = Transformer.getDataHelper(tableName);

        const {
            schemaName = `public`,
            columns = [
                `id`
            ],
            customQuery = () => ``,
        } = table;

        logInfo(`Getting data from ${tableName}`);
        logTime(`${tableName} table data`);

        const sql = customQuery(this.typeName) || `
            SELECT
                ${columns.join(`,\n`)}
            FROM
                ${schemaName}.${tableName}
            ORDER BY
                ${schemaName}.${tableName}.id DESC
            ;
        `;

        return query({ sql })
            .then(rows => {
                logTimeEnd(`${tableName} table data`);
                return {
                    tableName,
                    rows,
                };
            });
    }

    reduceRow ( tableData, row ) {
        const {
            idList,
            dataMap
        } = tableData;

        if ( dataMap ) {
            tableData.dataMap.set(row.id, row);
        }

        tableData.idList = idList.push(row.id);

        return tableData;
    }

    reduceDataSets ( dataSets ) {
        return dataSets.reduce(( tableMap, tableObj ) => {
            const {
                tableName,
                rows,
            } = tableObj;

            const listMap = rows.reduce(this.reduceRow, {
                tableName,
                "idList": List(),
                "dataMap": Object.keys(rows[ 0 ] || {}).length !== 1
                    ? new Map()
                    : null,
            });

            tableMap[ tableName ] = listMap
            return tableMap;
        }, {});
    }
}

module.exports = Transformer;
