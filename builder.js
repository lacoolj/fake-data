'use strict';

const jsf = require('json-schema-faker');
const createFiles = require('./createFiles');

async function startBuilding ( info ) {

    const {
        logInfo,
    } = require('./loggers');

    let {
        increment,
        lastIndex,
        index,
        csvPath,
        schemaConfig
    } = info;

    /**
     * Holds promises awaiting a filename produced once
     * CSV files are written to disk
     * @type {Array}
     */
    const fileNames = [];

    const {
        schemaName,
        fieldList,
        getSchema,
        tableName,
        subType
    } = schemaConfig;

    const fieldListString = fieldList.join();

    let items = [];
    const gap = lastIndex - index;
    let i = gap;

    logInfo(`Generating ${gap} fake ${tableName}...`);

    while ( i-- ) {
        const schema = getSchema({
            index: index++,
            subType
        });

        const {
            _id,
            id,
            ...data
        } = await jsf.resolve(schema);

        items.push(data);

        if ( i % increment === 0 || index >= lastIndex ) {
            logInfo(`${(100.0 - (100.0 * (i / gap))).toFixed(1)}% -- ${index} complete of ${lastIndex} ${tableName}`);

            const dataSet = items.splice(0);

            const fileName = await createFiles({
                dataSet,
                csvPath
            });

            fileNames.push(fileName);

        }
    }
    if ( items.length > 0 ) {
        logInfo(`Handling ${items.length} stragglers`);

        const dataSet = items.splice(0);

        const fileName = await createFiles({
            dataSet,
            csvPath
        });

        fileNames.push(fileName);
    }

    const fileNameArray = await Promise.all(fileNames);

    return {
        schemaName,
        tableName,
        fieldListString,
        fileNameArray
    };
};

module.exports = startBuilding;
