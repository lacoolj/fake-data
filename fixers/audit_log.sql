
SET ROLE TO postgres;

-- audit hashes n stuff
DO
$$
    BEGIN
        RAISE NOTICE 'AUDIT_LOG updates [START]';
    END
$$;

UPDATE
    audit_log
SET
    hash_value = md5(log_description),
    detailed_info = jsonb_build_object(
        'oldValues',
        CASE
            WHEN audit_log.study_id > 0
            THEN (SELECT JSONB_AGG(s.*) FROM studies s WHERE s.id = audit_log.study_id LIMIT 1)
            WHEN audit_log.order_id > 0
            THEN (SELECT JSONB_AGG(o.*) FROM orders o WHERE o.id = audit_log.order_id LIMIT 1)
            WHEN audit_log.patient_id > 0
            THEN (SELECT JSONB_AGG(p.*) FROM patients p WHERE p.id = audit_log.patient_id LIMIT 1)
            ELSE '{}' :: JSONB
        END,
        'newValues',
        CASE
            WHEN audit_log.study_id > 0
            THEN (SELECT JSONB_AGG(s.*) FROM studies s WHERE s.id = audit_log.study_id)
            WHEN audit_log.order_id > 0
            THEN (SELECT JSONB_AGG(o.*) FROM orders o WHERE o.id = audit_log.order_id)
            WHEN audit_log.patient_id > 0
            THEN (SELECT JSONB_AGG(p.*) FROM patients p WHERE p.id = audit_log.patient_id)
            ELSE '{}' :: JSONB
        END
    )
WHERE
    hash_value IS NULL
    AND log_description IS NOT NULL
    AND (
        study_id > 0
        OR order_id > 0
        OR patient_id > 0
    );

DO
$$
    BEGIN
        RAISE NOTICE 'AUDIT_LOG updates ==== END';
    END
$$;
RESET ROLE;
