
SET ROLE TO postgres;

-- match user names to provider names where available

DO
$$
    BEGIN
        RAISE NOTICE 'USERS updates [START]';
    END
$$;
UPDATE
    users
SET
    first_name = p.first_name,
    middle_initial = p.middle_initial,
    last_name = p.last_name,
    suffix = p.suffix
FROM
    providers p
WHERE
    user_type != 'NU'
    AND users.first_name IS NULL
    AND users.middle_initial IS NULL
    AND users.last_name IS NULL
    AND users.suffix IS NULL
    AND p.id = provider_id;

DO
$$
    BEGIN
        RAISE NOTICE 'USERS updates ...[END]';
    END
$$;
RESET ROLE;
