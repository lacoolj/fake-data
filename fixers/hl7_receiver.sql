
SET ROLE TO POSTGRES;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_RECEIVERS updates [START]';
    END
$$;
UPDATE
    hl7_receivers
SET
    translation_script = ( SELECT translation_script FROM hl7_receivers WHERE id = 1 )
WHERE
    translation_script IS NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_RECEIVERS updates ...[END]';
    END
$$;
RESET ROLE;
