
SET ROLE TO postgres;

-- ================
-- Corrections to appt types missing new facilities
-- and study status setup for new facilities
-- (copies from facility with ID 1)
-- ================

DO
$$
    BEGIN
        RAISE NOTICE 'FACILITIES updates [START]';
    END
$$;

UPDATE
    appointment_types
SET
    facility_ids = ARRAY(
        SELECT
            id
        FROM
            facilities
        WHERE
            is_active
            AND NOT has_deleted
    );


WITH ss AS (
    SELECT
        *
    FROM
        study_status
    WHERE
        facility_id = 1
        AND NOT has_deleted
), f AS (
    SELECT
        id
    FROM
        facilities
    WHERE
        id != 1
        AND NOT has_deleted
)
INSERT INTO
    study_status (status_code, status_desc, color_code, facility_id, order_related, can_edit, can_delete, has_deleted, deleted_dt, rad_related, display_order, waiting_time, status_validation, show_right_click, show_validation_window, mobile_rad_related)
SELECT
    status_code,
    status_desc,
    color_code,
    f.id,
    order_related,
    can_edit,
    can_delete,
    has_deleted,
    deleted_dt,
    rad_related,
    display_order,
    waiting_time,
    status_validation,
    show_right_click,
    show_validation_window,
    mobile_rad_related
FROM
    ss,
    f
WHERE
    NOT EXISTS (
        SELECT 1
        FROM study_status
        WHERE
            status_code = ss.status_code
            AND facility_id = f.id
            AND NOT has_deleted
        LIMIT 1
    );

WITH ss AS (
    SELECT
        *
    FROM
        study_status_workflow
    WHERE
        facility_id = 1
), f AS (
    SELECT
        id
    FROM
        facilities
    WHERE
        id != 1
        AND NOT has_deleted
)
INSERT INTO
    study_status_workflow (from_status, can_edit, can_delete, has_deleted, deleted_dt, short_summary, is_active, facility_id, actions, rad_related, display_order, to_status, is_mandatory)
SELECT
    from_status,
    can_edit,
    can_delete,
    has_deleted,
    deleted_dt,
    short_summary,
    is_active,
    f.id,
    actions,
    rad_related,
    display_order,
    to_status,
    is_mandatory
FROM
    ss,
    f
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            study_status_workflow
        WHERE
            from_status = ss.from_status
            AND to_status = ss.to_status
            AND facility_id = f.id
            AND NOT has_deleted
        LIMIT
            1
    );


UPDATE
    facilities
SET study_workflow_locations = (
    SELECT
        study_workflow_locations
    FROM
        facilities
    WHERE
        id = 1
)
WHERE
    id != 1
    AND NOT has_deleted;

UPDATE
    study_status
SET
    has_deleted = TRUE,
    deleted_dt = now()
FROM
    (
        SELECT
            id,
            row_number() OVER (W) AS pos
        FROM
            study_status
        WINDOW W AS (
            PARTITION BY
                status_code,
                facility_id
            ORDER BY
                deleted_dt ASC NULLS FIRST,
                id ASC
        )
    ) a
WHERE
    a.id = study_status.id
    AND a.pos > 1;

DO
$$
    BEGIN
        RAISE NOTICE 'FACILITIES updates ...[END]';
    END
$$;

RESET ROLE;
