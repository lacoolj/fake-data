
SET ROLE TO POSTGRES;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_RECEIVER_LOG updates [START]';
    END;
$$;

UPDATE
    hl7_receiver_log
SET
    order_id = s.order_id,
    patient_id = s.patient_id,
    status_info = json_build_object()
FROM
    studies s
WHERE
    s.accession_no = hl7_receiver_log.accession_no
    AND hl7_receiver_log.order_id IS NULL
    AND hl7_receiver_log.patient_id IS NULL
    AND hl7_receiver_log.status_info IS NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_RECEIVER_LOG updates ...[END]';
    END;
$$;

RESET ROLE;
