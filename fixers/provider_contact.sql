
SET ROLE TO postgres;

-- ===============================
-- Handle primary contacts not being set
-- ===============================

DO
$$
    BEGIN
        RAISE NOTICE 'PROVIDER_CONTACTS updates [START]';
    END
$$;

UPDATE
    provider_contacts
SET
    is_primary = (
        a.pos = 1
    ),
    is_active = (
        a.is_active OR a.pos = 1
    ),
    has_deleted = (
        a.pos != 1 AND a.has_deleted
    ),
    deleted_dt = (
        CASE WHEN
            a.pos != 1 AND a.has_deleted
            THEN a.deleted_dt
            ELSE NULL
        END
    )
FROM
    (
        SELECT
            id,
            provider_id,
            row_number() OVER (W) AS pos,
            is_primary,
            is_active,
            has_deleted,
            deleted_dt
        FROM
            provider_contacts pc
        WINDOW W AS (
            PARTITION BY
                pc.provider_id
            ORDER BY
                pc.deleted_dt NULLS FIRST,
                pc.provider_id ASC,
                pc.is_primary DESC NULLS LAST,
                pc.is_active DESC NULLS LAST
        )
    ) a
WHERE
    a.id = provider_contacts.id
    AND ((
        a.is_primary AND (
            a.pos > 1
            OR NOT a.is_active
            OR a.has_deleted
        )
    )
    OR (
        a.pos = 1
        AND (
            NOT a.is_primary
            OR NOT a.is_active
            OR a.has_deleted
        )
    ));

DO
$$
    BEGIN
        RAISE NOTICE 'PROVIDER_CONTACTS updates ...[END]';
    END
$$;

RESET ROLE;
