
SET ROLE TO postgres;

-- Not exactly consistent with other DBs but logical way to set "series_number"

DO
$$
    BEGIN
        RAISE NOTICE 'STUDY_SERIES updates [START]';
    END
$$;

WITH row_nums AS (
    SELECT
        ss.id,
        ss.study_id,
        row_number() OVER (W) AS row
    FROM
        study_series ss
    WINDOW W AS (
        PARTITION BY
            study_id
        ORDER BY
            ss.created_dt ASC
    )
)
UPDATE
    study_series
SET
    series_number = row_nums.row,
    series_attributes = hstore('seriesNumber', row_nums.row :: TEXT),
    series_info = json_build_object()
FROM
    row_nums
WHERE
    study_series.id = row_nums.id
    AND study_series.series_number IS NULL
    AND study_series.series_attributes IS NULL;


DO
$$
    BEGIN
        RAISE NOTICE 'STUDY_SERIES updates ...[END]';
    END
$$;

RESET ROLE;
