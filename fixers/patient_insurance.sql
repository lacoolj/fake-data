
SET ROLE TO postgres;

-- Fix demo/contact info for subscriber

DO
$$
    BEGIN
        RAISE NOTICE 'PATIENT_INSURANCES updates [START]';
    END
$$;
WITH relate AS (
    SELECT
        id
    FROM
        relationship_status
    WHERE
        description ILIKE 'self'
    LIMIT 1
)
UPDATE
    patient_insurances
SET
    subscriber_firstname = p.first_name,
    subscriber_lastname = p.last_name,
    subscriber_middlename = p.middle_name,
    subscriber_name_suffix = p.suffix_name,
    subscriber_dob = p.birth_date,
    subscriber_gender = p.gender
FROM
    patients p
WHERE
    p.id = patient_insurances.patient_id
    AND patient_insurances.subscriber_relationship_id = ( SELECT id FROM relate )
    AND NOT p.has_deleted
    AND p.is_active
    AND subscriber_firstname IS NULL
    AND subscriber_lastname IS NULL
    AND subscriber_dob IS NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'PATIENT_INSURANCES updates ...[END]';
    END
$$;
RESET ROLE;
