
SET ROLE TO postgres;

-- Fix duplicated study_transcriptions

DO
$$
    BEGIN
        RAISE NOTICE 'STUDY_TRANSCRIPTIONS updates [START]';
    END
$$;

WITH allst AS (
    SELECT
        id,
        study_id,
        transcription_type,
        row_number() OVER (W) AS pos
    FROM
        study_transcriptions
    WINDOW W AS (
        PARTITION BY
            study_id,
            transcription_type
        ORDER BY
            study_id ASC NULLS LAST,
            transcription_type ASC,
            id ASC
    )
), dups AS (
    SELECT
        allst.id
    FROM
        allst
    WHERE
        allst.pos > 1
        AND allst.study_id IS NOT NULL
        AND allst.transcription_type = 'F'
)
DELETE FROM
    study_transcriptions
WHERE
    study_transcriptions.id IN (
        SELECT
            dups.id
        FROM
            dups
    );


-- Fix fake transcriptions by taking transcription_data from an existing one and updating empty col in table

WITH data AS (
    SELECT DISTINCT
        LENGTH(transcription_data) AS size,
        transcription_data,
        transcription_text
    FROM
        study_transcriptions
    WHERE
        transcription_data IS NOT NULL
        AND transcription_text IS NOT NULL
    ORDER BY
        LENGTH(transcription_data) DESC
    LIMIT
        100
), arr AS (
    SELECT
        ARRAY_AGG(transcription_data) AS data_array,
        ARRAY_AGG(transcription_text) AS text_array,
        COUNT(1) AS total
    FROM
        data
)
UPDATE
    study_transcriptions
SET
    transcription_data = arr.data_array[ study_transcriptions.id % arr.total ],
    transcription_text = arr.text_array[ study_transcriptions.id % arr.total ]
FROM
    arr
WHERE
    study_transcriptions.study_id IS NOT NULL
    AND study_transcriptions.transcription_text IS NULL
    AND study_transcriptions.transcription_data IS NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'STUDY_TRANSCRIPTIONS updates ...[END]';
    END
$$;
RESET ROLE;
