
-- Set missing patient_id where applicable

DO
$$
    BEGIN
        RAISE NOTICE 'STUDY_FORMS updates [START]';
    END
$$;
UPDATE
    study_forms
SET
    patient_id = o.patient_id
FROM
    orders o
WHERE
    study_forms.order_id IS NOT NULL
    AND study_forms.patient_id IS NULL
    AND o.id = study_forms.order_id
    AND o.patient_id IS NOT NULL
    AND NOT o.has_deleted;

SET ROLE TO postgres;

-- Fix fake forms by taking document_html from template and updating missing col in study_forms table

WITH html AS (
    SELECT
        id,
        document_html
    FROM
        study_form_templates
    WHERE
        is_active
        AND NOT has_deleted
)

UPDATE
    study_forms
SET
    document_html = html.document_html
FROM
    html
WHERE
    study_forms.study_form_id = html.id
    AND study_forms.document_html IS NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'STUDY_FORMS updates ...[END]';
    END
$$;
RESET ROLE;
