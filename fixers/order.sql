
SET ROLE TO postgres;

-- Insert notes for orders

DO
$$
    BEGIN
        RAISE NOTICE 'ORDERS updates [START]';
    END
$$;
UPDATE
    orders
SET
    order_notes = json_build_array(
        json_build_object(
            'notes', 'Consectetur purus ut faucibus pulvinar elementum integer enim neque. Tellus mauris a diam maecenas sed. In metus vulputate eu scelerisque felis imperdiet proin fermentum. Sit amet mattis vulputate enim nulla aliquet. Dignissim sodales ut eu sem integer.',
            'createdby', COALESCE(order_info -> 'createdby', ''),
            'createddt', ordered_dt
        )
    )
WHERE
    order_notes IS NULL
    AND NOT has_deleted;

DO
$$
    BEGIN
        RAISE NOTICE 'ORDERS updates ...[END]';
    END
$$;
RESET ROLE;
