
SET ROLE TO postgres;

DO
$$
    BEGIN
        RAISE NOTICE 'PATIENT_DOCUMENTS updates [START]';
    END
$$;

-- Set missing patient_id where applicable

UPDATE
    patient_documents
SET
    patient_id = s.patient_id,
    order_id = s.order_id
FROM
    studies s
WHERE
    patient_documents.study_id IS NOT NULL
    AND patient_documents.order_id IS NULL
    AND patient_documents.patient_id IS NULL
    AND patient_documents.provider_id IS NULL
    AND patient_documents.provider_group_id IS NULL
    AND NOT patient_documents.has_deleted
    AND NOT s.has_deleted
    AND s.id = patient_documents.study_id
    AND s.patient_id IS NOT NULL
    AND s.order_id IS NOT NULL;

UPDATE
    patient_documents
SET
    patient_id = o.patient_id
FROM
    orders o
WHERE
    patient_documents.order_id IS NOT NULL
    AND patient_documents.study_id IS NULL
    AND patient_documents.patient_id IS NULL
    AND patient_documents.provider_id IS NULL
    AND patient_documents.provider_group_id IS NULL
    AND NOT patient_documents.has_deleted
    AND NOT o.has_deleted
    AND o.id = patient_documents.order_id
    AND o.patient_id IS NOT NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'PATIENT_DOCUMENTS updates ...[END]';
    END
$$;
RESET ROLE;
