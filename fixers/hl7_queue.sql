
SET ROLE TO POSTGRES;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_QUEUE updates [START]';
    END
$$;

UPDATE
    hl7_queue
SET
    order_id = s.order_id,
    patient_id = s.patient_id,
    status_info = json_build_object(),
    input_data = COALESCE(( SELECT input_data FROM hl7_queue WHERE input_data IS NOT NULL LIMIT 1 ), json_build_object())
FROM
    studies s
WHERE
    s.id = hl7_queue.study_id
    AND hl7_queue.order_id IS NULL
    AND hl7_queue.patient_id IS NULL
    AND hl7_queue.status_info IS NULL
    AND hl7_queue.input_data IS NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_QUEUE updates ...[END]';
    END
$$;

RESET ROLE;
