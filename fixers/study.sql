
DO $$

DECLARE

    facility_count              INT;
    scheduled_statuses          VARCHAR[];
    scheduled_status_count      INT;
    non_scheduled_statuses      VARCHAR[];
    non_scheduled_status_count  INT;

BEGIN

    SET ROLE TO postgres;

    -- Insert notes for studies

    BEGIN
        RAISE NOTICE 'STUDIES updates [START]';
    END;

    SELECT
        COUNT(1)
    INTO
        facility_count
    FROM
        facilities
    WHERE
        deleted_dt IS NULL
        AND is_active;

    SELECT
        ARRAY_AGG(ss.status_code),
        COUNT(1)
    INTO
        scheduled_statuses,
        scheduled_status_count
    FROM
        study_status ss
    WHERE
        facility_count = (
            SELECT DISTINCT
                COUNT(ss2.facility_id)
            FROM
                study_status ss2
            JOIN facilities f
                ON ss2.facility_id = f.id
            WHERE
                f.deleted_dt IS NULL
                AND f.is_active
                AND ss2.status_code = ss.status_code
        )
        AND (
            NOT ss.order_related
            OR ss.status_code NOT IN ( 'ORD', 'CAN' )
            -- OR ss.status_code IN ( 'SCH', 'RSCH', 'CON', 'CHI', 'ABRT', 'CHO', 'NOS' )
        );

    SELECT
        ARRAY_AGG(ss3.status_code),
        COUNT(1)
    INTO
        non_scheduled_statuses,
        non_scheduled_status_count
    FROM
        study_status ss3
    WHERE
        facility_count = (
            SELECT DISTINCT
                COUNT(ss4.facility_id)
            FROM
                study_status ss4
            JOIN facilities f
                ON ss4.facility_id = f.id
            WHERE
                f.deleted_dt IS NULL
                AND f.is_active
                AND ss4.status_code = ss3.status_code
        )
        AND (
            ss3.order_related
            AND ss3.status_code IN ( 'ORD', 'CAN' )
        );

    WITH s AS (
        SELECT
            s.id,
            s.order_id,
            s.duration,
            row_number() OVER (W) AS row
        FROM
            studies s
        WINDOW W AS (
            PARTITION BY
                order_id
            ORDER BY
                s.id ASC
        )
    )
    UPDATE
        studies
    SET
        notes = json_build_array(
            json_build_object(
                'notes', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
                'createdby', COALESCE(get_full_name(u.last_name, u.first_name), ''),
                'createddt', o.ordered_dt
            )
        ),
        study_status = (
            CASE
                WHEN o.order_status IN ('ORD', 'CAN') OR o.scheduled_dt IS NULL
                THEN non_scheduled_statuses[ studies.id % non_scheduled_status_count ]
                ELSE scheduled_statuses[ studies.id % scheduled_status_count ]
            END
        ),
        study_info = study_info || hstore('createdby', get_full_name(u.last_name, u.first_name)),
        stat_level = COALESCE(o.order_info->'stat', '0') :: INT,
        patient_id = o.patient_id,
        referring_physician_id = o.referring_provider_ids[ 1 ],
        reading_physician_id = o.reading_providers[ 1 ],
        last_edited_dt = o.ordered_dt,
        study_created_dt = o.ordered_dt,
        status_last_changed_dt = o.ordered_dt,
        study_dt = (
            CASE
                WHEN o.scheduled_dt IS NOT NULL
                THEN ( o.scheduled_dt + ( ( SELECT SUM(duration) FROM s WHERE s.row < s_data.row ) :: TEXT || ' minutes' ) :: INTERVAL )
                ELSE NULL
            END
        ),
        schedule_dt = (
            CASE
                WHEN o.scheduled_dt IS NOT NULL
                THEN ( o.scheduled_dt + ( ( SELECT SUM(duration) FROM s WHERE s.row < s_data.row ) :: TEXT || ' minutes' ) :: INTERVAL )
                ELSE NULL
            END
        )
    FROM
        orders o
    JOIN s s_data
        ON s_data.order_id = o.id
    JOIN users u
        ON u.id = o.ordered_by
    WHERE
        studies.notes IS NULL
        AND studies.patient_id IS NULL
        AND studies.deleted_dt IS NULL
        AND studies.study_status IS NULL
        AND studies.stat_level IS NULL
        AND o.deleted_dt IS NULL
        AND o.id = studies.order_id
        AND s_data.id = studies.id;

    BEGIN
        RAISE NOTICE 'STUDIES updates ...[END]';
    END;

    RESET ROLE;

END
$$;
