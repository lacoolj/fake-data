
SET ROLE TO postgres;

-- Insert notes for patients

DO
$$
    BEGIN
        RAISE NOTICE 'PATIENTS updates [START]';
    END
$$;

UPDATE
    patients
SET
    notes = json_build_array(
        json_build_object(
            'notes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'createdby', COALESCE((SELECT full_name FROM users WHERE id = patients.owner_id), ''),
            'createddt', created_dt
        )
    )
WHERE
    notes IS NULL
    AND deleted_dt IS NULL
    AND is_active;

DO
$$
    BEGIN
        RAISE NOTICE 'PATIENTS updates ...[END]';
    END
$$;

RESET ROLE;
