
SET ROLE TO POSTGRES;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_MESSAGES updates [START]';
    END
$$;

UPDATE
    hl7_messages
SET
    translation_script = ( SELECT translation_script FROM hl7_messages WHERE id = 1 )
WHERE
    translation_script IS NULL;

DO
$$
    BEGIN
        RAISE NOTICE 'HL7_MESSAGES updates ...[END]';
    END
$$;
RESET ROLE;
