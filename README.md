# fake-data

### setup

- Set up `pg_hba.conf` to be very liberal (or edit db.js and whatever else I put in here)
- Make sure `..cfg/web.json` exists and has your DB info
> If you have Unix domain socket available, you can use this config:
> `local all postgres trust`

### Install project modules
```sh
npm install
# or
yarn
```

### Set ENV vars

```sh
# If running on same server as DB, use 127.0.0.1 as PG_HOST here
export DB_STRING=postgresql://PG_USER:PG_PASS@PG_HOST:PG_PORT/EXA_DB_NAME
```

_Obviously replacing what needs to be replaced_

### Execute
```sh
# Run manually with node using arguments available (see below for options)
node generator.js [...options]

# Test out the generators
npm run test

# ... or individual tests (replace DATA_TYPE with...the data type - see list of options below or in package.json)
npm run test-DATA_TYPE

# Or generate pre-specified amounts of each individual type
npm run [ file_store | facility | modality_room | provider | provider_contact | user | insurance_provider | call_category | patient | patient_insurance | order | study | study_cpt | document | study_form | call_log | study_transcription | audit_log ]

# Or just run this to execute all the available types in order 
npm run stuff
```

#### options (not really used)

`-c, --record-count` 
> *10000*

Number of records to create (when running single instance)

`-d` 
> `$FAKE_EXPORT_DIR` value or `./csv`

Directory to output files to (temporarily while sending to postgres)

`-c, --record-count` 
> *10000*

Number of records to create

## order of operations

Given types currently supported, create in the following order *(logically proportional amounts in parenthesis)*:

### 1. Setup
- file_store (1 per facility)
- facility (< 100)
- modality_room (> facility count - probably assume 5-10 per facility)
- provider (10000-20000)
- provider_contact (2-4x provider count for good measure)
- user (100 - 20000)
- insurance_provider (16000)
- call_category

### 2. Content

- patient (>300000 < 2000000 for now)
- patient_insurance (1-3x patient count-ish)
- order (> 800000 < 10000000 for now)
- study (1-3x order count)
- study_cpt (1-5x study count)
- document (> 5000000 < 20000000)
- study_form
- call_log
- study_transcription
- audit_log (8,000,000)
