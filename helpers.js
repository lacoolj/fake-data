'use strict';

/**
 * Other helpers
 */

const quittingTime = ( pendingMap, offsetMap ) => {
    return (
        pendingMap.size === 0 && offsetMap.size === 0
    );
};

const updateOffset = ( offset, index, offsetMap ) => {
    const offsetInfo = offsetMap.get(offset);

    if ( !offsetInfo ) {
        return true;
    }

    const {
        lastIndex,
        increment
    } = offsetInfo;

    const moreComing = (
        lastIndex > index &&
        index + increment <= lastIndex
    );

    if ( !moreComing ) {
        offsetMap.delete(offset);
        return true;
    }
    offsetMap.set(offset, {
        ...offsetInfo,
        index
    });

    return false;
};

/**
 * Data helpers
 */

/* AUTO-GENERATED SYMBOLS */

const callLogSymbol = Symbol();

const callCategorySymbol = Symbol();

const studyTranscriptionSymbol = Symbol();

const auditLogSymbol = Symbol();

const studySeriesSymbol = Symbol();

const studySeriesInstanceSymbol = Symbol();

const applicationEntitySymbol = Symbol();

const hl7ReceiverLogSymbol = Symbol();

const hl7ReceiverSymbol = Symbol();

const hl7InterfaceSymbol = Symbol();

const hl7MessageSymbol = Symbol();

const hl7QueueSymbol = Symbol();

/* MANUAL SYMBOLS */

const documentTypeSymbol = Symbol();
const documentSymbol = Symbol();
const fileStoreSymbol = Symbol();
const companySymbol = Symbol();
const facilitySymbol = Symbol();
const modalitySymbol = Symbol();
const modalityRoomSymbol = Symbol();
const providerSymbol = Symbol();
const providerContactSymbol = Symbol();
const providerGroupSymbol = Symbol();
const insuranceProviderSymbol = Symbol();
const orderSymbol = Symbol();
const patientSymbol = Symbol();
const patientInsuranceSymbol = Symbol();
const relationshipStatusSymbol = Symbol();
const employmentStatusSymbol = Symbol();
const studySymbol = Symbol();
const studyCptSymbol = Symbol();
const studyFormSymbol = Symbol();
const studyFormTemplateSymbol = Symbol();
const studyStatusSymbol = Symbol();
const paymentSymbol = Symbol();
const paymentReasonSymbol = Symbol();
const userRoleSymbol = Symbol();
const userGroupSymbol = Symbol();
const userSymbol = Symbol();
const cptCodeSymbol = Symbol();
const feeScheduleCptSymbol = Symbol();
const appointmentTypeSymbol = Symbol();
const appointmentTypeProcedureSymbol = Symbol();
const siteSymbol = Symbol();

const symbolMap = new Map([
    /* AUTO-GENERATED SYMBOL ALIASING */

    ["call_log", callLogSymbol],


    ["call_category", callCategorySymbol],
    ["call_categories", callCategorySymbol],

    ["study_transcription", studyTranscriptionSymbol],
    ["study_transcriptions", studyTranscriptionSymbol],

    ["audit_log", auditLogSymbol],

    ["study_series", studySeriesSymbol],

    ["study_series_instance", studySeriesInstanceSymbol],
    ["study_series_instances", studySeriesInstanceSymbol],

    ["application_entity", applicationEntitySymbol],
    ["application_entities", applicationEntitySymbol],

    ["hl7_receiver_log", hl7ReceiverLogSymbol],

    ["hl7_receiver", hl7ReceiverSymbol],
    ["hl7_receivers", hl7ReceiverSymbol],

    ["hl7_interface", hl7InterfaceSymbol],
    ["hl7_interfaces", hl7InterfaceSymbol],

    ["hl7_message", hl7MessageSymbol],
    ["hl7_messages", hl7MessageSymbol],

    ["hl7_queue", hl7QueueSymbol],

    /* MANUAL SYMBOL ALIASING */

    ["document_type", documentTypeSymbol],
    ["document_types", documentTypeSymbol],

    ["document", documentSymbol],
    ["patient_documents", documentSymbol],

    ["file_store", fileStoreSymbol],
    ["file_stores", fileStoreSymbol],

    ["company", companySymbol],
    ["companies", companySymbol],

    ["facility", facilitySymbol],
    ["facilities", facilitySymbol],

    ["insurance_provider", insuranceProviderSymbol],
    ["insurance_providers", insuranceProviderSymbol],

    ["modality", modalitySymbol],
    ["modalities", modalitySymbol],

    ["modality_room", modalityRoomSymbol],
    ["modality_rooms", modalityRoomSymbol],

    ["order", orderSymbol],
    ["orders", orderSymbol],

    ["patient", patientSymbol],
    ["patients", patientSymbol],

    ["patient_insurance", patientInsuranceSymbol],
    ["patient_insurances", patientInsuranceSymbol],

    ["relationship_status", relationshipStatusSymbol],
    ["relationship_statuses", relationshipStatusSymbol],

    ["employment_status", employmentStatusSymbol],
    ["employment_statuses", employmentStatusSymbol],

    ["provider", providerSymbol],
    ["providers", providerSymbol],

    ["provider_contact", providerContactSymbol],
    ["provider_contacts", providerContactSymbol],

    ["provider_group", providerGroupSymbol],
    ["provider_groups", providerGroupSymbol],

    ["study", studySymbol],
    ["studies", studySymbol],

    ["study_cpt", studyCptSymbol],

    ["study_form", studyFormSymbol],
    ["study_forms", studyFormSymbol],

    ["study_form_template", studyFormTemplateSymbol],
    ["study_form_templates", studyFormTemplateSymbol],

    ["study_status", studyStatusSymbol],

    ["payment", paymentSymbol],
    ["payments", paymentSymbol],

    ["payment_reason", paymentReasonSymbol],
    ["payment_reasons", paymentReasonSymbol],

    ["user_role", userRoleSymbol],
    ["user_roles", userRoleSymbol],

    ["user_group", userGroupSymbol],
    ["user_groups", userGroupSymbol],

    ["user", userSymbol],
    ["users", userSymbol],

    ["cpt_code", cptCodeSymbol],
    ["cpt_codes", cptCodeSymbol],

    ["fee_schedule_cpt", feeScheduleCptSymbol],
    ["fee_schedule_cpts", feeScheduleCptSymbol],

    ["appointment_type_procedure", appointmentTypeProcedureSymbol],
    ["appointment_type_procedures", appointmentTypeProcedureSymbol],

    ["appointment_type", appointmentTypeSymbol],
    ["appointment_types", appointmentTypeSymbol],

    ["site", siteSymbol],
    ["sites", siteSymbol],
]);

const dbMap = new Map([
    /* AUTO-GENERATED SYMBOL MAPPING */

    [callLogSymbol, {
        "typeName": `call_log`,
        "tableName": `call_log`,
        "dependencies": [
            `call_categories`,
            `users`,
            `studies`,
        ],
        "columns": [
            `id`,
            `company_id`,
            `call_category_id`,
            `created_by`,
            `study_id`,
        ],
        "getSchemaInit": require(`./schema/call_log`),
    }],

    [callCategorySymbol, {
        "typeName": `call_category`,
        "tableName": `call_categories`,
        "dependencies": [
            `companies`,
        ],
        "columns": [
            `id`,
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `call_log`:
                    return `
                        SELECT
                            id,
                            company_id
                        FROM
                            call_categories
                        WHERE
                            deactivated_dt IS NULL
                            AND deleted_dt IS NULL;
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/call_category`),
    }],

    [studyTranscriptionSymbol, {
        "typeName": `study_transcription`,
        "tableName": `study_transcriptions`,
        "dependencies": [
            `studies`,
            `users`,
            `provider_contacts`,
        ],
        "columns": [
            `id`,
            `company_id`,
            `study_id`,
            `created_by`,
            `provider_contact_id`,
        ],
        "getSchemaInit": require(`./schema/study_transcription`),
    }],

    [auditLogSymbol, {
        "typeName": `audit_log`,
        "tableName": `audit_log`,
        "dependencies": [
            `companies`,
            `users`,
            `patients`,
            `studies`,
            `orders`,
        ],
        "columns": [
            `id`,
            `company_id`,
            `created_by`,
            `patient_id`,
            `study_id`,
            `order_id`,
        ],
        "getSchemaInit": require(`./schema/audit_log`),
    }],

    [studySeriesSymbol, {
        "typeName": `study_series`,
        "tableName": `study_series`,
        "dependencies": [
            `studies`,
            `modalities`,
            `users`,
        ],
        "columns": [
            `id`,
            `study_id`,
        ],
        "getSchemaInit": require(`./schema/study_series`),
    }],

    [studySeriesInstanceSymbol, {
        "typeName": `study_series_instance`,
        "tableName": `study_series_instances`,
        "dependencies": [
            `study_series`,
            `file_stores`,
            `application_entities`,
        ],
        "columns": [
            `id`,
            `study_id`,
            `study_series_id`,
            `file_store_id`,
            `application_entity_id`,
        ],
        "getSchemaInit": require(`./schema/study_series_instance`),
    }],

    [applicationEntitySymbol, {
        "typeName": `application_entity`,
        "tableName": `application_entities`,
        "dependencies": [
            `companies`,
            `file_stores`,
            `facilities`,
        ],
        "columns": [
            `id`,
            `file_store_id`,
            `facility_id`,
        ],
        // "getSchemaInit": require(`./schema/application_entity`),
    }],

    [hl7ReceiverLogSymbol, {
        "typeName": `hl7_receiver_log`,
        "tableName": `hl7_receiver_log`,
        "dependencies": [
            `companies`,
            `hl7_receivers`,
            `studies`,
        ],
        "columns": [
            `id`,
            `company_id`,
            `hl7_receiver_id`,
            `study_id`,
        ],
        "getSchemaInit": require(`./schema/hl7_receiver_log`),
    }],

    [hl7ReceiverSymbol, {
        "typeName": `hl7_receiver`,
        "tableName": `hl7_receivers`,
        "dependencies": [
            `companies`,
        ],
        "columns": [
            `id`,
        ],
        "getSchemaInit": require(`./schema/hl7_receiver`),
    }],

    [hl7InterfaceSymbol, {
        "typeName": `hl7_interface`,
        "tableName": `hl7_interfaces`,
        "dependencies": [
            `companies`,
        ],
        "columns": [
            `id`,
            `company_id`,
        ],
        "getSchemaInit": require(`./schema/hl7_interface`),
    }],

    [hl7MessageSymbol, {
        "typeName": `hl7_message`,
        "tableName": `hl7_messages`,
        "dependencies": [
            `companies`,
            `hl7_interfaces`,
        ],
        "columns": [
            `id`,
        ],
        "getSchemaInit": require(`./schema/hl7_message`),
    }],

    [hl7QueueSymbol, {
        "typeName": `hl7_queue`,
        "tableName": `hl7_queue`,
        "dependencies": [
            `companies`,
            `studies`,
            `hl7_interfaces`,
            `hl7_messages`,
        ],
        "columns": [
            `id`,
            `company_id`,
            `study_id`,
            `hl7_interface_id`,
            `hl7_message_id`,
        ],
        "getSchemaInit": require(`./schema/hl7_queue`),
    }],

    /* MANUAL SYMBOL MAPPING */

    [documentTypeSymbol, {
        "typeName": `document_type`,
        "tableName": `document_types`,
        "dependencies": [],
        "columns": [
            `id`,
            `deleted_dt`,
            `name`,
            // `needs_review`
        ],
        "getSchemaInit": require(`./schema/document_type`),
    }],

    [documentSymbol, {
        "typeName": `document`,
        "tableName": `patient_documents`,
        "dependencies": [
            // `document_types`,
            `companies`,
            `file_stores`,
            `patients`,
            `providers`,
            `provider_groups`,
            `users`,
            `orders`,
            `studies`,
        ],
        "columns": [
            `id`,
        ],
        "getSchemaInit": require(`./schema/document`),
    }],

    [employmentStatusSymbol, {
        "typeName": `employment_status`,
        "tableName": `employment_status`,
        "dependencies": [
            `companies`,
        ],
        "columns": [
            `id`,
            `company_id`,
            `inactivated_dt`,
            `description`,
        ],
        "getSchemaInit": require(`./schema/employment_status`),
    }],

    [fileStoreSymbol, {
        "typeName": `file_store`,
        "tableName": `file_stores`,
        "dependencies": [],
        "columns": [
            `id`,
        ],
        "getSchemaInit": require(`./schema/file_store`),
    }],

    [companySymbol, {
        "typeName": `company`,
        "tableName": `companies`,
        "dependencies": [
            `file_stores`,
        ],
        "columns": [
            `id`,
            `file_store_id`,
            // `mrn_info`,
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `document`:
                    return `
                        SELECT DISTINCT
                            c.id,
                            ARRAY_AGG(legacy_document_types ->> 'description') AS document_types
                        FROM
                            companies c,
                            json_array_elements(
                                c.scan_document_types -> 'scan_document_type'
                            ) legacy_document_types
                        WHERE
                            c.is_active
                            AND c.deleted_dt IS NULL
                        GROUP BY
                            c.id;
                    `;

                default:
                    return ``;
            }
        },
    }],

    [facilitySymbol, {
        "typeName": `facility`,
        "tableName": `facilities`,
        "dependencies": [
            `file_stores`,
        ],
        "columns": [
            `id`,
            `is_active`,
            `file_store_id`,
            `mrn_info`,
            `study_workflow_locations`,
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `application_entity`:
                case `payment`:
                    return `
                        SELECT
                            id
                        FROM
                            facilities
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                    `;

                case `order`:
                    return `
                        SELECT
                            id
                        FROM
                            facilities
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/facility`),
    }],

    [insuranceProviderSymbol, {
        "typeName": `insurance_provider`,
        "tableName": `insurance_providers`,
        "dependencies": [
            `facilities`
        ],
        "columns": [
            `id`,
            `is_active`,
            `facility_id`,
            `insurance_name`,
            `insurance_info`
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `payment`:
                    return `
                        SELECT
                            id
                        FROM
                            insurance_providers
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/insurance_provider`),
    }],

    [modalitySymbol, {
        "typeName": `modality`,
        "tableName": `modalities`,
        "dependencies": [],
        "columns": [
            `id`,
            `modality_code`
        ],
        "customQuery": ( parentType ) => {
            switch ( parentType ) {
                case `order`:
                    return `
                        SELECT
                            id
                        FROM
                            modalities
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/modality`),
    }],

    [modalityRoomSymbol, {
        "typeName": `modality_room`,
        "tableName": `modality_rooms`,
        "dependencies": [
            `facilities`,
            `modalities`,
        ],
        "columns": [
            `id`,
            `is_active`,
            `facility_id`,
            `modalities`,
            `modality_room_name`,
            `modality_room_code`,
        ],
        "getSchemaInit": require(`./schema/modality_room`),
    }],

    [orderSymbol, {
        "typeName": `order`,
        "tableName": `orders`,
        "dependencies": [
            `facilities`,
            `modalities`,
            `modality_rooms`,
            `patients`,
            `patient_insurances`,
            `provider_contacts`,
            `sites`,
            `study_status`,
            `users`,
        ],
        "columns": [
            `id`,
            `COALESCE(order_info->'stat', '0')::INT AS stat_level`,
            `order_status`,
            `ordered_dt::TEXT`,
            `ordered_by`,
            `patient_id`,
            `facility_id`,
            `modality_id`,
            `modality_room_id`,
            `referring_provider_ids`,
            `referring_providers`,
            `reading_providers`,
            `to_char(scheduled_dt, 'YYYY-MM-DD') AS scheduled_dt`
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `study`:
                    return `
                        SELECT
                            o.id,
                            --o.order_status,
                            --COALESCE(o.order_info->'stat', '0')::INT AS stat_level,
                            o.facility_id,
                            o.modality_id,
                            o.modality_room_id
                        FROM
                            orders o
                        WHERE
                            NOT EXISTS (
                                SELECT 
                                    1
                                FROM 
                                    studies s
                                WHERE 
                                    s.order_id = o.id
                                    AND s.deleted_dt IS NULL
                                    AND o.deleted_dt IS NULL
                            )
                            AND o.deleted_dt IS NULL
                    `;

                case `document`:
                case `study_form`:
                    return `
                        SELECT
                            id
                        FROM
                            orders
                        WHERE
                            deleted_dt IS NULL
                    `;

                case `audit_log`:
                    return `
                        SELECT
                            o.id
                        FROM
                            orders o
                        LEFT JOIN patients p
                            ON p.id = o.patient_id
                        WHERE
                            o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            AND p.is_active
                            AND p.id IS NOT NULL
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    audit_log
                                WHERE
                                    o.id = audit_log.order_id
                                LIMIT
                                    1
                            )
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/order`),
    }],

    [providerSymbol, {
        "typeName": `provider`,
        "tableName": `providers`,
        "dependencies": [
            `facilities`,
            `modalities`
        ],
        "columns": [
            `id`,
            `is_active`,
            `provider_type`,
            `full_name`,
            `facilities`
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `user`:
                    return `
                        SELECT
                            p.id,
                            p.first_name,
                            p.middle_initial,
                            p.last_name,
                            p.suffix,
                            p.provider_type
                        FROM
                            providers p
                        WHERE
                            NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    users u
                                WHERE
                                    u.deleted_dt IS NULL
                                    AND u.provider_id = p.id
                                    AND u.is_active
                            )
                            AND p.is_active
                            AND p.deleted_dt IS NULL
                    `;

                case `provider_contact`:
                    return `
                        SELECT
                            p.id,
                            p.is_active,
                            p.provider_type,
                            p.full_name,
                            p.facilities
                        FROM
                            providers p
                        LEFT JOIN provider_contacts pc ON 
                            pc.provider_id = p.id
                        WHERE
                            pc.id IS NULL
                            AND p.deleted_dt IS NULL
                            AND p.is_active
                            AND NOT EXISTS ( 
                                SELECT 
                                    1
                                FROM
                                    provider_contacts pc1
                                WHERE
                                    pc1.provider_id = p.id
                                    AND pc1.is_primary
                                    AND pc1.deleted_dt IS NULL
                                    AND pc1.is_active
                                LIMIT
                                    1
                            );
                    `;

                case `document`:
                    return `
                        SELECT
                            p.id
                        FROM
                            providers p
                        WHERE
                            p.is_active
                            AND p.deleted_dt IS NULL
                            AND p.provider_type IN ('AT', 'RF');
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/provider`),
    }],

    [providerContactSymbol, {
        "typeName": `provider_contact`,
        "tableName": `provider_contacts`,
        "dependencies": [
            `providers`,
        ],
        "columns": [
            `id`,
            `is_active`,
            `provider_id`,
            `is_primary`
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `payment`:
                    return `
                        SELECT
                            id
                        FROM
                            provider_contacts
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                    `;

                case `study`:
                case `order`:
                    return `
                        SELECT
                            pc.id,
                            p.provider_type
                        FROM
                            provider_contacts pc
                        LEFT JOIN providers p ON 
                            p.id = pc.provider_id
                        WHERE
                            p.id IS NOT NULL
                            
                            AND pc.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND pc.is_active
                            AND p.is_active;
                    `;

                case `study_transcription`:
                    return `
                        SELECT
                            pc.id,
                            p.provider_type
                        FROM
                            provider_contacts pc
                        JOIN providers p
                            ON p.id = pc.provider_id
                        WHERE
                            p.is_active
                            AND pc.is_active
                            
                            AND p.deleted_dt IS NULL
                            AND pc.deleted_dt IS NULL                        
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/provider_contact`),
    }],

    [providerGroupSymbol, {
        "typeName": `provider_group`,
        "tableName": `provider_groups`,
        "dependencies": [
            `companies`,
        ],
        "columns": [
            `id`,
            `group_type`,
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `document`:
                    return `
                        SELECT
                            id
                        FROM
                            provider_groups
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                            AND group_type IN ('PG', 'OF');
                    `;

                case `payment`:
                    return `
                        SELECT
                            id
                        FROM
                            provider_groups
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                            AND group_type = 'OF';
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": null,
    }],

    [patientSymbol, {
        "typeName": `patient`,
        "tableName": `patients`,
        "dependencies": [
            `facilities`,
            `providers`,
            `users`,
            `employment_status`,
        ],
        "columns": [
            `id`,
            `is_active`,
            `birth_date :: TEXT`,
            `gender`,
            `first_name`,
            `last_name`,
            `middle_name`,
            `suffix_name`,
            `employment_status_id`,
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `patient_insurance`:
                    return `
                        SELECT
                            p.id,
                            p.employment_status_id
                        FROM
                            patients p
                        WHERE
                            p.is_active
                            AND p.deleted_dt IS NULL
                            AND p.first_name IS NOT NULL
                            AND p.last_name IS NOT NULL
                            AND p.birth_date IS NOT NULL
                    `;

                case `order`:
                case `document`:
                case `study_form`:
                case `payment`:
                case `audit_log`:
                    return `
                        SELECT
                            p.id
                        FROM
                            patients p
                        WHERE
                            p.is_active
                            AND p.deleted_dt IS NULL
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/patient`),
    }],

    [patientInsuranceSymbol, {
        "typeName": `patient_insurance`,
        "tableName": `patient_insurances`,
        "dependencies": [
            `patients`,
            `insurance_providers`,
            `relationship_status`,
            `employment_status`,
        ],
        "columns": [
            `id`,
            `patient_id`,
            `coverage_level`,
        ],
        "getSchemaInit": require(`./schema/patient_insurance`),
    }],

    [relationshipStatusSymbol, {
        "typeName": `relationship_status`,
        "tableName": `relationship_status`,
        "dependencies": [
            `companies`,
        ],
        "columns": [
            `id`,
            `company_id`,
            `inactivated_dt`,
            `description`,
        ],
        "getSchemaInit": require(`./schema/relationship_status`),
    }],

    [studySymbol, {
        "typeName": `study`,
        "tableName": `studies`,
        "dependencies": [
            `orders`,
            // `companies`, // Needed for accession number formatting
            `facilities`,
            `modalities`,
            `modality_rooms`,
            `study_status`,
            `users`,
            `appointment_types`,
            `appointment_type_procedures`,
        ],
        "columns": [
            `id`,
            `study_status`,
            `patient_id`,
            `facility_id`,
            `order_id`,
            `modality_id`,
            `procedure_id`,
            `appointment_type_id`,
            `duration`,
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `hl7_receiver_log`:
                    return `
                        SELECT
                            s.id,
                            s.accession_no
                        FROM
                            studies s
                        LEFT JOIN orders o
                            ON o.id = s.order_id
                        LEFT JOIN patients p
                            ON p.id = s.patient_id
                        WHERE
                            s.deleted_dt IS NULL
                            AND o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND p.is_active
                            AND o.id IS NOT NULL
                            
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    hl7_receiver_log hl
                                WHERE
                                    s.accession_no = hl.accession_no
                                LIMIT
                                    1
                            )
                    `;

                case `hl7_queue`:
                    return `
                        SELECT
                            s.id
                        FROM
                            studies s
                        LEFT JOIN orders o
                            ON o.id = s.order_id
                        LEFT JOIN patients p
                            ON p.id = s.patient_id
                        WHERE
                            s.deleted_dt IS NULL
                            AND o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND p.is_active
                            AND o.id IS NOT NULL
                            
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    hl7_queue hl
                                WHERE
                                    s.id = hl.study_id
                                LIMIT
                                    1
                            )
                    `;

                case `study_cpt`:
                    return `
                        SELECT
                            s.id,
                            s.study_status,
                            s.patient_id,
                            s.facility_id,
                            s.order_id,
                            s.modality_id,
                            -- s.procedure_id,
                            s.appointment_type_id,
                            s.duration,
                            o.modality_room_id,
                            atp.procedure_id,
                            atp.modifier1_id,
                            atp.modifier2_id,
                            atp.modifier3_id,
                            atp.modifier4_id,
                            atp.duration AS duration_proc,
                            atp.modality_room_ids,
                            atp.modality_room_durations,
                            at.duration AS appt_duration,
                            at.code AS appt_display_code,
                            at.name AS appt_display_description
                        FROM
                            studies s
                        LEFT JOIN orders o ON 
                            o.id = s.order_id
                        LEFT JOIN appointment_types at ON
                            at.id = s.appointment_type_id
                        LEFT JOIN appointment_type_procedures atp ON 
                            atp.appointment_type_id = s.appointment_type_id
                        WHERE
                            NOT EXISTS (
                                SELECT 1
                                FROM study_cpt
                                WHERE study_id = s.id
                            )
                            AND s.id IS NOT NULL
                            AND o.id IS NOT NULL
                            AND at.id IS NOT NULL
                            AND atp.id IS NOT NULL
                            AND at.is_active
                            AND atp.is_active
                            AND o.deleted_dt IS NULL
                            AND s.deleted_dt IS NULL
                        ORDER BY
                            s.id DESC;
                    `;

                case `study_transcription`:
                    return `
                        SELECT
                            s.id
                        FROM
                            studies s
                        LEFT JOIN orders o
                            ON o.id = s.order_id
                        LEFT JOIN patients p
                            ON p.id = s.patient_id
                        WHERE
                            s.deleted_dt IS NULL
                            AND o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND p.is_active
                            AND o.id IS NOT NULL
                            
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    study_transcriptions st
                                WHERE
                                    s.id = st.study_id
                                    AND st.transcription_type = 'F'
                                LIMIT
                                    1
                            )
                            AND s.study_status IN ('APP', 'APCD', 'TRAN', 'DRFT')
                    `;

                case `study_series`:
                    return `
                        SELECT
                            s.id,
                            s.modality_id
                        FROM
                            studies s
                        LEFT JOIN orders o
                            ON o.id = s.order_id
                        LEFT JOIN patients p
                            ON p.id = s.patient_id
                        WHERE
                            s.deleted_dt IS NULL
                            AND o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND p.is_active
                            AND o.id IS NOT NULL
                            
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    study_series ss
                                WHERE
                                    s.id = ss.study_id
                                LIMIT
                                    1
                            )
                    `;

                case `call_log`:
                    return `
                        SELECT
                            s.id
                        FROM
                            studies s
                        LEFT JOIN orders o
                            ON o.id = s.order_id
                        LEFT JOIN patients p
                            ON p.id = s.patient_id
                        WHERE
                            s.deleted_dt IS NULL
                            AND o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND p.is_active
                            AND o.id IS NOT NULL
                            
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    call_log
                                WHERE
                                    s.id = call_log.study_id
                                ORDER BY
                                    call_log.study_id DESC, 
                                    call_log.id DESC
                                LIMIT
                                    1
                            )
                    `;

                case `audit_log`:
                    return `
                        SELECT
                            s.id
                        FROM
                            studies s
                        LEFT JOIN orders o
                            ON o.id = s.order_id
                        LEFT JOIN patients p
                            ON p.id = s.patient_id
                        WHERE
                            s.deleted_dt IS NULL
                            AND o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND p.is_active
                            AND o.id IS NOT NULL
                            
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    audit_log
                                WHERE
                                    s.id = audit_log.study_id
                                LIMIT
                                    1
                            )
                    `;

                case `document`:
                    return `
                        SELECT
                            s.id
                        FROM
                            studies s
                        LEFT JOIN orders o
                            ON o.id = s.order_id
                        LEFT JOIN patients p
                            ON p.id = s.patient_id
                        WHERE
                            s.deleted_dt IS NULL
                            AND o.deleted_dt IS NULL
                            AND p.deleted_dt IS NULL
                            
                            AND p.is_active
                            AND o.id IS NOT NULL
                            
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    patient_documents
                                WHERE
                                    s.id = patient_documents.study_id
                                ORDER BY
                                    patient_documents.study_id, 
                                    patient_documents.id DESC
                                LIMIT
                                    1
                            )
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/study`),
    }],

    [studyCptSymbol, {
        "typeName": `study_cpt`,
        "tableName": `study_cpt`,
        "dependencies": [
            `studies`,
            `cpt_codes`,
            `fee_schedule_cpts`,
        ],
        "columns": [
            `id`,
        ],
        "getSchemaInit": require(`./schema/study_cpt`),
    }],

    [studyFormSymbol, {
        "typeName": `study_form`,
        "tableName": `study_forms`,
        "dependencies": [
            `study_form_templates`,
            `orders`,
            `patients`,
        ],
        "columns": [
            `id`,
        ],
        "getSchemaInit": require(`./schema/study_form`),
    }],

    [studyFormTemplateSymbol, {
        "typeName": `study_form_template`,
        "tableName": `study_form_templates`,
        "dependencies": [],
        "columns": [
            `id`,
            `is_active`,
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `study_form`:
                    return `
                        SELECT
                            id,
                            
                            -- Currently too much to deal with in CSV import - just update after
                            -- document_html,
                            
                            COALESCE(assign_options -> 'patientPortal', 'false') :: BOOLEAN AS patient_portal
                        FROM
                            study_form_templates
                        WHERE
                            is_active
                            AND deleted_dt IS NULL
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": null,
    }],

    [paymentSymbol, {
        "schemaName": `billing`,
        "typeName": `payment`,
        "tableName": `payments`,
        "dependencies": [
            `companies`,
            `facilities`,
            `payment_reasons`,
            `users`,
            `patients`,
            `insurance_providers`,
            `provider_contacts`,
            `provider_groups`,
        ],
        "columns": [
            `id`,
            `patient_id`,
            `provider_contact_id`,
            `provider_group_id`,
            `insurance_provider_id`,
            `payment_reason_id`,
        ],
        "getSchemaInit": require(`./schema/payment`),
    }],

    [paymentReasonSymbol, {
        "schemaName": `billing`,
        "typeName": `payment_reason`,
        "tableName": `payment_reasons`,
        "dependencies": [
            `companies`,
        ],
        "columns": [
            `id`,
        ],
        "getSchemaInit": null,
    }],

    [userRoleSymbol, {
        "typeName": `user_role`,
        "tableName": `user_roles`,
        "dependencies": [],
        "columns": [
            `id`,
            `is_active`
        ],
        "getSchemaInit": require(`./schema/user_role`),
    }],

    [userGroupSymbol, {
        "typeName": `user_group`,
        "tableName": `user_groups`,
        "dependencies": [
            `user_roles`
        ],
        "columns": [
            `id`,
            `is_active`
        ],
        "getSchemaInit": require(`./schema/user_group`),
    }],

    [userSymbol, {
        "typeName": `user`,
        "tableName": `users`,
        "dependencies": [
            `user_groups`,
            `providers`,
            `facilities`,
        ],
        "columns": [
            `id`,
            `is_active`,
            `provider_id`,
            `get_full_name(last_name, first_name, middle_initial) AS full_name`,
            `username`
        ],
        "customQuery": parentType => {
            switch ( parentType ) {
                case `study_transcription`:
                    return `
                        SELECT
                            id,
                            user_type,
                            provider_id
                        FROM
                            users
                        WHERE
                            is_active
                            AND deleted_dt IS NULL;
                    `;

                case `call_log`:
                case `document`:
                case `payment`:
                case `order`:
                case `study`:
                case `study_series`:
                    return `
                        SELECT
                            id
                        FROM
                            users
                        WHERE
                            is_active
                            AND deleted_dt IS NULL;
                    `;

                case `audit_log`:
                    return `
                        SELECT
                            id,
                            username
                        FROM
                            users
                        WHERE
                            is_active
                            AND deleted_dt IS NULL;
                    `;

                default:
                    return ``;
            }
        },
        "getSchemaInit": require(`./schema/user`),
    }],

    [cptCodeSymbol, {
        "typeName": `cpt_code`,
        "tableName": `cpt_codes`,
        "dependencies": [],
        "columns": [
            `id`,
            `is_active`,
            `display_code`,
            `display_description`,
            `rvu`,
            `duration`
        ],
        "getSchemaInit": null,
    }],

    [feeScheduleCptSymbol, {
        "typeName": `fee_schedule_cpt`,
        "tableName": `fee_schedule_cpts`,
        "dependencies": [],
        "columns": [
            `id`,
            `cpt_code_id`,
            `global_fee`,
        ],
        "getSchemaInit": null,
    }],

    [appointmentTypeSymbol, {
        "typeName": `appointment_type`,
        "tableName": `appointment_types`,
        "dependencies": [],
        "columns": [
            `id`,
            `is_active`,
            `code`,
            `name`,
            `facility_ids`,
            `modality_ids`,
            `modality_room_ids`,
            `modality_room_durations`,
            `duration`
        ],
        "getSchemaInit": null,
    }],

    [appointmentTypeProcedureSymbol, {
        "typeName": `appointment_type_procedure`,
        "tableName": `appointment_type_procedures`,
        "dependencies": [
            `cpt_codes`,
            `appointment_types`
        ],
        "columns": [
            `id`,
            `is_active`,
            `appointment_type_id`,
            `procedure_id`,
            `modality_ids`,
            `duration`,
            `allow_scheduling`,
        ],
        "getSchemaInit": null,
    }],

    [studyStatusSymbol, {
        "typeName": `study_status`,
        "tableName": `study_status`,
        "dependencies": [
            `facilities`,
        ],
        "columns": [
            `id`,
            `facility_id`,
            `status_code`,
            `status_desc`,
            `order_related`,
        ],
        "getSchemaInit": null, //require(`./schema/study_status`),
    }],

    [siteSymbol, {
        "typeName": `site`,
        "tableName": `sites`,
        "dependencies": [],
        "columns": [
            `id`,
            `COALESCE(stat_level_config->'stat_level', '[]'::JSON) AS stat_levels`
        ],
        "getSchemaInit": null,
    }]
]);

module.exports = {
    quittingTime,
    updateOffset,
    symbolMap,
    dbMap,
}
