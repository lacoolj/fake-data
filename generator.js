#!/usr/bin/env node

'use strict';

const fs = require('fs');
const path = require('path');

const {
    promisify
} = require('util');

const asyncUnlink = promisify(fs.unlink);

const {
    List,
    Map,
    Set
} = require('immutable');

const {
    FAKE_EXPORT_DIR = `${__dirname}/csv`,
    LOG_DIR,
} = process.env;

const yargs = require('yargs');
const argv = yargs
    .alias(`c`, `record-count`)
    .default(`c`, 10000)

    .alias(`l`, `file-size-limit`)
    .default(`l`, 1000000)

    .alias(`d`, `directory`)
    .default(`d`, FAKE_EXPORT_DIR)

    .alias(`t`, `type`)
    .default(`t`, ``)

    .alias(`s`, `sub-type`)
    .default(`s`, undefined)

    /**
     * Chained - run each dependency through init to create fake data
     * before getting to the main `type`.
     *
     * Only for bootstrapping and big benchmarks
     */
    .alias(`g`, `chained`)
    .default(`g`, undefined)

    .alias(`v`, `verbose`)
    .default(`v`, undefined)

    .argv;

const {
    c: totalCount,
    l: limit,
    d: dir,
    t: type,
    s: subType,
    g: chained,
    v: verbose, // Either from argv or from process.env as `VERBOSE=1`
} = argv;

const {
    logInfo,
    logError,
} = require('./loggers');

if ( !type ) {
    logError(`Need to pass this: -t [TYPE]`);
    process.exit(1);
}

logInfo(`generator [${type}] started`);

process.on(`exit`, code =>
    logInfo(`generator [${type}] exiting with code ${code}`)
);

process.on(`uncaughtException`, error => (
    logError(`generator [${type}] uncaught exception`, error),
    process.exit(1)
    // logTrace(`generator [${type}] uncaught exception trace`, error)
));

const { execSync } = require('child_process');
const mkdirp = require('mkdirp');
const csvPath = `${dir}/${type}`;

mkdirp.sync(csvPath);

if ( LOG_DIR ) {
    mkdirp.sync(path.join(__dirname, LOG_DIR));
}

const Transformer = require('./data-transform');
const builder = require('./builder');

const {
    query,
    config: dbConfig,
} = require('./db');

const DROP_INDEXES = tableName => `
DO $$

    DECLARE

        cmd TEXT;

    BEGIN
    
        SET ROLE TO postgres;
        
        WITH cmds AS (
            SELECT
                STRING_AGG(
                    'DROP TRIGGER IF EXISTS ' || trigger_name || ' ON ' || event_object_table,
                    '; '
                ) AS cmd
            FROM
                information_schema.triggers ccu
            WHERE
                event_object_table = '${tableName}' 
                AND event_object_table != 'providers' 
                AND event_object_table != 'provider_contacts' 

            UNION

            SELECT
                'DROP INDEX IF EXISTS ' || string_agg(indexrelid::REGCLASS::TEXT, ', ') AS cmd
            FROM
                pg_index i
            LEFT JOIN pg_depend d
                ON d.objid = i.indexrelid AND d.deptype = 'i'
            WHERE
                i.indrelid = '${tableName}'::REGCLASS
                AND d.objid IS NULL
        )

        SELECT
            STRING_AGG(cmds.cmd, '; ') || ';'
        INTO cmd
        FROM cmds;

        IF ( cmd IS NOT NULL ) THEN
            EXECUTE cmd;
        END IF;

    END
    
$$;
`;

const readFileToQuery = ( schemaName, tableName, fieldListString ) =>
    async fileName => {
        try {
            if ( verbose ) {

            }
            await query({
                sql: DROP_INDEXES(tableName)
            });
        }
        catch (e) {
            logError(`Couldn't drop indexes on table ${tableName} because:`, e);
        }

        const file = fileName.split(/\/|\\/g).pop();
        logInfo(`Starting COPY on ${schemaName}.${tableName} from ${file}`);

        if ( /127\.0\.0\.1|localhost/.test(dbConfig.host) ) {
            await query({
                'sql': `
                    COPY ${schemaName}.${tableName}(${fieldListString}) 
                    FROM '${fileName}'
                    DELIMITER '|' CSV HEADER;
                `
            });
        }
        else {
            const url = `postgresql://${dbConfig.user}:${dbConfig.password}@${dbConfig.host}:${dbConfig.port}/${dbConfig.database}`;
            // -U postgres -d ${dbConfig.database} --host ${dbConfig.host} --port ${dbConfig.port}
            execSync(`psql ${url} -c "\\copy ${schemaName}.${tableName}(${fieldListString}) from '${fileName}' delimiter '|' csv header;"`);
        }

        try {
            return asyncUnlink(fileName);
        }
        catch (e) {
            logError(`ERROR removing file ${fileName}`, e);
        }
    };

const handleBuildResults = result => {
    const {
        schemaName,
        tableName,
        fieldListString,
        fileNameArray
    } = result;

    const fileReader = readFileToQuery(schemaName, tableName, fieldListString);

    return Promise.all(
        fileNameArray.map(fileReader)
    );
};

function* iterator ( schemaConfig ) {
    const total = totalCount;

    let offset = 0;
    let gapSize = Math.ceil(
        total > 5000 ?
            total / 8 :
            total
    );

    // 50 50 50 50 50 50 50 50 49 ...
    const maxOffset = total - gapSize;
    while ( total > offset ) {
        const index = offset;

        let lastIndex = index + gapSize;
        if ( lastIndex >= total ) {
            lastIndex = total;
        }

        const increment = total > 5000
            ? Math.ceil(gapSize / 10)
            : total;

        const offsetObj = {
            lastIndex,
            increment,
            index,
            offset,
            csvPath,
            schemaConfig
        };

        yield offsetObj;

        if ( offset > maxOffset ) {
            gapSize = lastIndex - offset;
        }
        offset += gapSize;
    }
};

const init = async ( typeName ) => {
    const buildResults = [];
    const dataHandler = new Transformer(typeName);
    const schemaConfig = await dataHandler.processData();

    const offsetInfo = iterator(schemaConfig);
    let nextOffset = offsetInfo.next();
    while ( !nextOffset.done ) {
        const result = await builder(nextOffset.value);
        buildResults.push(handleBuildResults(result));
        nextOffset = offsetInfo.next();
    }

    return await Promise.all(buildResults);
};

init(type)
    // .then(buildResults => {
    //     return buildResults
    // })
    .then(() => process.exit());
