#!/bin/sh

#psql -d "$EXA_DB" -c "COPY ( SELECT id FROM document_types WHERE is_active ORDER BY id ASC ) TO '/tmp/document_types.csv';"
psql -d "$EXA_DB" -c "COPY ( SELECT REPLACE(COALESCE(sd.id,(COUNT(1) OVER (RANGE UNBOUNDED PRECEDING)) :: TEXT), 'scan_document_types_', '') AS id FROM ( SELECT json_array_elements(scan_document_types -> 'scan_document_type')->>'id' AS id FROM companies) sd ORDER BY id ASC ) TO '/tmp/document_type.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' /tmp/document_type.csv)]" > $FAKE_EXPORT_DIR/document_type.json
