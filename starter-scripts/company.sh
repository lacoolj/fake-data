#!/bin/sh

psql -d "$EXA_DB" -c "COPY ( SELECT id FROM companies WHERE NOT has_deleted AND is_active ORDER BY id ASC ) TO '/tmp/companies.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' /tmp/company.csv)]" > $FAKE_EXPORT_DIR/company.json
