#!/bin/sh

psql -d "$EXA_DB" -c "COPY ( SELECT id FROM modalities WHERE NOT has_deleted ORDER BY id ASC ) TO '/tmp/modality.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' /tmp/modality.csv)]" > $FAKE_EXPORT_DIR/modality.json
