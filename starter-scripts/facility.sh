#!/bin/sh

psql -d "$EXA_DB" -c "COPY ( SELECT id FROM facilities WHERE NOT has_deleted ORDER BY id ASC ) TO '${FAKE_EXPORT_DIR}/facility.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' ${FAKE_EXPORT_DIR}/facility.csv)]" > $FAKE_EXPORT_DIR/facility.json
