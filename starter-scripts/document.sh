#!/bin/sh

#psql -d "$EXA_DB" -c "COPY ( SELECT id FROM assets WHERE NOT deleted ORDER BY id ASC ) TO '/tmp/asset.csv';"
psql -d "$EXA_DB" -c "COPY ( SELECT id FROM patient_documents WHERE NOT has_deleted ORDER BY id ASC ) TO '/tmp/document.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' /tmp/document.csv)]" > $FAKE_EXPORT_DIR/document.json
