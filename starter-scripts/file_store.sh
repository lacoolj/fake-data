#!/bin/sh

psql -d "$EXA_DB" -c "COPY ( SELECT id FROM file_stores WHERE NOT has_deleted ORDER BY id ASC ) TO '/tmp/file_store.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' /tmp/file_store.csv)]" > $FAKE_EXPORT_DIR/file_store.json
