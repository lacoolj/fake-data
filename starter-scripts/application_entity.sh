#!/bin/sh

psql -d "$EXA_DB" -c "COPY ( SELECT id FROM application_entities WHERE NOT has_deleted AND is_active ORDER BY id ASC ) TO '/tmp/application_entity.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' /tmp/application_entity.csv)]" > $FAKE_EXPORT_DIR/application_entity.json
