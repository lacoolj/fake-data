#!/bin/sh
psql -d "$EXA_DB" -c "COPY ( SELECT id FROM patients WHERE NOT has_deleted AND is_active ORDER BY id ASC ) TO '/tmp/patients.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' /tmp/patient.csv)]" > $FAKE_EXPORT_DIR/patient.json
