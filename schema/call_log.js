'use strict';

const {
    random,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
} = require('../utils');

const {
    beatTheOdds,
    randomEl,
} = numberUtils;

const {
    makeTimestampTz,
} = require('../starter-data/shared');

module.exports = dependentData => {

    const {
        call_categories,
        users,
        studies,
    } = dependentData;

    const {
        idList: callCategoryIds,
    } = call_categories;

    const {
        idList: userIds,
    } = users;

    const {
        idList: studyIds,
    } = studies;

    return ({ index }) => {
        const call_category_id = randomElFromList(callCategoryIds);
        const created_by = randomElFromList(userIds);
        const study_id = randomElFromList(studyIds);

        return {
            call_category_id,
            created_by,
            study_id,
            "note": random.words(),
            "created_dt": makeTimestampTz(),
            "_id": `${index}`
        };
    };
};
