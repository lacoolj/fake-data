'use strict';

const {
    random
} = require('faker');

const {
    numberUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber,
    beatTheOdds
} = numberUtils;

module.exports = dependentData => {
    const {
        user_roles
    } = dependentData;

    const {
        idList: userRoleIds
    } = user_roles;

    const userRoleCount = userRoleIds.size;

    return ( { index } ) => {
        const favorableOdds = beatTheOdds(98, 8);

        const group_name = random.words()
            .slice(0, 64);

        const group_description = `${group_name} ${random.words()}`
            .slice(0, 256);

        const group_code = group_name
            .replace(/[a-z]{3,}(?=\s)/g, ``)
            .replace(/\s*/g, ``)
            .slice(0, 4)
            .toUpperCase();

        const start = index % userRoleCount;
        const userRoleListStr = favorableOdds
            ? randomElFromList(userRoleIds)
            : userRoleIds
                .slice(start, start + 2)
                .join();

        return {
            group_name,
            group_description,
            "is_active": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            "ad_group_id": ``,
            "group_type": ``,
            "can_delete": favorableOdds,
            "company_id": 1,
            "user_roles": `{${userRoleListStr}}`,
            group_code,
            "document_types": ``,
            "group_info": ``,
            "_id": `${index}`
        };
    };
};
