'use strict';

const {
    name
} = require('faker');

const {
    years,
    months,
    days,
    hours,
    minutes,
    email,
    phone,
    address1,
    address2,
    city,
    stateAbbr,
    zip,
    gender,
    makeBirthDate,
    makeFutureDate
} = require('../starter-data/shared');

const {
    numberUtils,
    letterUtils,
    randomElFromList,
    makeUidStudy
} = require('../utils');

const {
    randomEl,
    aNumber,
    beatTheOdds
} = numberUtils;

const {
    aLetter
} = letterUtils;

module.exports = dependentData => {
    const {
        facilities,
        providers,
        users,
        employment_status,
    } = dependentData;

    const {
        idList: facilityIds,
        dataMap: facilityData
    } = facilities;

    const {
        idList: providerIds,
        dataMap: providerData
    } = providers;

    const {
        idList: userIds
    } = users;

    const {
        idList: employmentStatusIds
    } = employment_status;

    return ({ index }) => {

        const favorableOdds = beatTheOdds(76, 12);

        const prefix_name = name.prefix();
        const first_name = name.firstName();
        const middle_name = name.firstName();
        const last_name = name.lastName();
        const suffix_name = name.suffix();
        const ssn = `${aNumber(3)}-${aNumber(2)}-${aNumber(4)}`;

        const facilityId = randomElFromList(facilityIds);
        const facility = facilityData.get(facilityId);

        const {
            mrn_info
        } = facility;

        const {
            prefix = ``
        } = mrn_info || {};

        const account_no = `${prefix}${aNumber(6)}${aLetter(2)}`;

        const year = randomEl(years);
        const month = randomEl(months);
        const day = randomEl(days);
        const hour = randomEl(hours);
        const minute = randomEl(minutes);
        const created_dt = `${year}-${month}-${day}T${hour}:${minute}:00-05:00`;

        const patient_uid = makeUidStudy();

        const providerID = randomElFromList(providerIds);
        const providername = providerData.get(providerID).full_name;

        return {
            'dicom_patient_id': account_no,
            account_no,
            'alt_account_no': ``,
            'account_no_history': ``,
            patient_uid,
            prefix_name,
            first_name,
            middle_name,
            last_name,
            suffix_name,
            gender,
            'birth_date': makeBirthDate(),
            'patient_type': `h`,
            'owner_id': randomElFromList(userIds),
            'last_edit_dt': ``,
            'last_edit_by': ``,
            'patient_info': {
                'commPref': {
                    "type": "string",
                    "enum": [
                        "chat",
                        "Phone",
                        "Email",
                        "Postal Communication",
                        "Telepathy",
                        "Mass Media",
                        "The Silent Treatment",
                        "WWII Enigma"
                    ]
                },
                'is_animal': false,
                'maritalStatus': {
                    "type": `string`,
                    "enum": [
                        "Single",
                        "Married",
                        "Separated",
                        "Divorced",
                        "Widowed",
                        "Prefer not to answer"
                    ]
                },
                'ethnic': {
                    "type": "string",
                    "enum": [
                        "Abenaki",
                        "Ahtna",
                        "Cajun"
                    ]
                },
                'height': {
                    "type": "integer",
                    "minimum": 52,
                    "maximum": 77
                },
                'weight': {
                    "type": "integer",
                    "minimum": 95,
                    "maximum": 350
                },
                'licenseNo': {
                    "type": "integer",
                    "minimum": 20005000,
                    "maximum": 8888888888
                },
                'licenseState': stateAbbr,
                'expiryDate': makeFutureDate(),
                'language': {
                    "type": "string",
                    "enum": [
                        "english",
                        "french",
                        "spanish",
                        "japanese"
                    ]
                },
                'racial': {
                    "type": "string",
                    "enum": [
                        "African American",
                        "American Indian",
                        "Asian",
                        "Hispanic/Latino",
                        "Other",
                        "White",
                        "White,African American",
                        "White,American Indian"
                    ]
                },
                'c1Email': email,
                'c2Email': email,
                'c1HomePhone': phone,
                'c2HomePhone': phone,
                'c1MobilePhone': phone,
                'c2MobilePhone': phone,
                'c1AddressLine1': address1,
                'c2AddressLine1': address1,
                'c1AddressLine2': address2,
                'c2AddressLine2': address2,
                'c1City': city,
                'c2City': city,
                'c1State': stateAbbr,
                'c2State': stateAbbr,
                'c1WorkPhone': phone,
                'c2WorkPhone': phone,
                'c1Zip': zip,
                'c2Zip': zip,
                'employerAddress': address2,
                'enableVerterinary': false,
                'employerName': `${name.lastName()}, ${name.firstName()} ${name.firstName()}`,
                'resonForDeath': ``,
                ssn,
                'providerfacilities': ``,
                'employerPhone': phone,
                'patientFlag': ``,
                'mothers_maiden': name.lastName(),
                'deathReason': ``,
                providername,
                providerID,
                'smokingStatus': ``
            },
            'notes': ``,
            'alerts': ``,
            'facility_id': facilityId,
            'company_id': 1,
            'is_active': favorableOdds,
            'has_deleted': false,
            'deleted_dt': ``,
            'full_name': `${last_name}, ${first_name}${middle_name ? ' ' + middle_name : ''}`,
            'patient_uid_system': ``,
            'patient_details': ``,
            'rcopia_id': ``,
            'portal_info': ``,
            'portal_activated_dt': ``,
            created_dt,
            'ordering_facilities': ``,
            'employment_status_id': randomElFromList(employmentStatusIds),
            "_id": `${index}`
        };
    };
};
