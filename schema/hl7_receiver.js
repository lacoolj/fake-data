'use strict';

const {
    random,
} = require('faker');

const {
    numberUtils,
    letterUtils,
    randomElFromList,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
    aNumber,
} = numberUtils;

const {
    aLetter,
} = letterUtils;

module.exports = dependentData => {

    const {
        companies,
    } = dependentData;

    const {
        idList: companyIds,
        dataMap: companyData,
    } = companies;

    const company_id = randomElFromList(companyIds);

    const messageTypes = [
        `ORM`,
        `DFT`,
        `ORU`,
        `ADT`,
        `SIU`,
    ];

    const connectionTypes = [
        `T`,
        `F`,
        `FT`,
        `DB`,
    ];

    const more_info = {
        "sendACK": true,
        "frame_end": "undefined",
        "line_seperator": "undefined",
        "frame_start": "undefined",
    };

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const receiver_name = random.words();

        // Appears the 'interface_type' is actually a 'connection_type' like in hl7_interfaces table
        const interface_type = randomEl(connectionTypes);
        const message_type = randomEl(messageTypes);
        const trigger_event = `${aLetter(1)}${aNumber(1)}${aNumber(1)}`;
        const translation_script = ``; // Gotta be done in SQL fixer
        const is_active = favorableOdds;
        const has_deleted = false;
        const deleted_dt = ``;

        const sending_application = random.words();
        const sending_facility = random.words();

        return {
            receiver_name,
            interface_type,
            message_type,
            trigger_event,
            translation_script,
            is_active,
            has_deleted,
            deleted_dt,
            company_id,
            sending_application,
            sending_facility,
            more_info,
            "_id": `${index}`
        };
    };
};
