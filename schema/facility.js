'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    address
} = require('faker');

const {
    email,
    phone,
    address1,
    address2,
    city,
    state,
    zip,
    website
} = require('../starter-data/shared');

const {
    numberUtils,
    letterUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber,
    beatTheOdds
} = numberUtils;

const {
    aLetter
} = letterUtils;

const codeLengths = [
    2,
    2,
    3,
    4
];

module.exports = dependentData => {
    const {
        file_stores
    } = dependentData;

    const {
        idList: fileStoreIds
    } = file_stores;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        const mrnPrefix = `${aLetter(index % 5)}`.toUpperCase();
        const mrnSuffix = `${aLetter(index % 3)}`.toUpperCase();

        const mrn_info = {
            "prefix": mrnPrefix,
            "suffix": mrnSuffix,
            "preview": `${mrnPrefix}1${mrnSuffix}`,
            "can_edit": favorableOdds,
            "mrn_type": `U`,
            "start_from": ``,
            "field_length": aNumber(1),
            "current_index": ``,
            "is_fixed_length": !favorableOdds,
            "allow_duplicates": !favorableOdds
        };

        const facility_name = address.streetAddress()
            .slice(0, 64);

        const facility_code = facility_name
            .replace(/[a-z]{3,}(?=\s)/g, ``)
            .replace(/\s*/g, ``)
            .slice(0, 4)
            .toUpperCase();

        return {
            facility_code,
            facility_name,
            "facility_info": {
                "npino": aNumber(10),
                "fee_id": ``,
                "fee_name": ``,
                "pos_type": ``,
                "posOnAlways": !favorableOdds,
                "display_date": ``,
                "show_studies": !favorableOdds,
                "ssn_required": !favorableOdds,
                "submitter_id": ``,
                "updox_account": `main`,
                "facility_faxNo": phone,
                "federal_tax_id": aNumber(9),
                "updox_send_fax": favorableOdds,
                "benefit_on_date": ``,
                "facility_autoFax": !favorableOdds,
                "facility_inherit": favorableOdds,
                "facility_website": website,
                "auth_requester_id": ``,
                "global_auto_print": !favorableOdds,
                "pokitdok_response": ``,
                "exam_prep_required": !favorableOdds,
                "facility_autoEmail": !favorableOdds,
                "pay_to_provider_id": ``,
                "abbreviated_receipt": !favorableOdds,
                "billing_provider_id": ``,
                "global_auto_printer": ``,
                "service_facility_id": ``,
                "show_patient_alerts": !favorableOdds,
                "show_schedules_days": ``,
                "enable_alt_account_no": !favorableOdds,
                "primary_phys_required": !favorableOdds,
                "rendering_provider_id": ``,
                "restrictDoubleBooking": !favorableOdds,
                "service_facility_name": ``,
                "show_recent_schedules": !favorableOdds,
                "enable_manual_checkout": !favorableOdds,
                "modality_display_width": ``,
                "eligibility_receiver_id": ``,
                "enable_insurance_claims": !favorableOdds,
                "facility_mammoLicenseId": ``,
                "import_docs_into_dicoms": !favorableOdds,
                "mobileDispatchingAddress": !favorableOdds,
                "enable_insurance_eligibility": favorableOdds,
                'facility_email': email,
                'facility_fax': phone,
                'facility_contactNo': phone,
                'facility_address1': address1,
                'facility_address2': address2,
                'facility_city': city,
                'facility_state': state,
                'facility_zip': zip
            },
            "is_active": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            "company_id": 1,
            "file_store_id": randomElFromList(fileStoreIds),
            "time_zone": {
                "type": `string`,
                "enum": [
                    `US/Eastern`,
                    `US/Central`
                ]
            },
            mrn_info,
            "enable_veterinary": ``,
            "study_workflow_locations": ``/*[
                {
                    "id": "ORD",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "SCH",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "CON",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "CHI",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "TS",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "TE",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "CHO",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "INC",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "UNR",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "DIC",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "RE",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "DRFT",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "TRAN",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "APP",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                },
                {
                    "id": "WOP",
                    "left": ~~(Math.random() * 680),
                    "top": ~~(Math.random() * 550)
                }
            ]*/,
            "max_tat": index % 3 + 1,
            "mu_validation_fields": ``,
            "_id": `${index}`
        };
    };
};
