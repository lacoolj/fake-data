'use strict';

const dayjs = require('dayjs');

const {
    commerce,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
    TIMESTAMP_FORMAT,
} = require('../utils');

const {
    beatTheOdds,
    randomEl,
} = numberUtils;

const {
    makeTimestampTz,
} = require('../starter-data/shared');

module.exports = dependentData => {

    const {
        companies,
    } = dependentData;

    const {
        idList: companyIds,
    } = companies;

    return ({ index }) => {
        const favorableOdds = beatTheOdds();
        const company_id = randomElFromList(companyIds);

        const deactivated_dt = favorableOdds
            ? ``
            : makeTimestampTz();

        const deleted_dt = deactivated_dt && beatTheOdds()
            ? `${dayjs(deactivated_dt).add(1, 'day').format(TIMESTAMP_FORMAT)}${deactivated_dt.slice(-6)}`
            : ``;

        return {
            company_id,
            "name": `${commerce.department()} - ${commerce.productName()}`,
            deactivated_dt,
            deleted_dt,

            "_id": `${index}`
        };
    };
};
