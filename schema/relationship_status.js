'use strict';

module.exports = dependentData => {
    const {
        companies,
    } = dependentData;

    const {
        idList: companyIds
    } = companies;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        return {
            "company_id": companyIds.first(),
            "inactivated_dt": !favorableOdds
                ? ``
                : ``,
            "description": {
                "type": `string`,
                "enum": [
                    `Father`,
                    `Grandparent`,
                    `Mother`,
                    `Self`,
                    `Sibling`,
                    `Spouse`,
                ],
            },
            "_id": `${index}`
        };
    };
};
