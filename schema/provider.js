'use strict';

const {
    name,
    random,
} = require('faker');

const {
    numberUtils,
    letterUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber,
    beatTheOdds
} = numberUtils;

const {
    aLetter
} = letterUtils;

const providerTypeRandom = {
    "type": `string`,
    "enum": [
        `AT`, // Attorney
        `AT`, // Attorney
        `AT`, // Attorney
        `AT`, // Attorney
        `AT`, // Attorney
        `AT`, // Attorney
        `AT`, // Attorney

        `PD`, // Provider
        `PD`, // Provider
        `PD`, // Provider

        `PR`, // Provider-Radiology
        `PR`, // Provider-Radiology
        `PR`, // Provider-Radiology
        `PR`, // Provider-Radiology
        `PR`, // Provider-Radiology
        `PR`, // Provider-Radiology
        `PR`, // Provider-Radiology

        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider
        `RF`, // Referring Provider

        `TG`, // Technologist
        `TG`, // Technologist

        `NU`, // Nurse

        `LB`  // Laboratory
    ]
};

module.exports = dependentData => {
    const {
        facilities,
        modalities
    } = dependentData;

    const {
        idList: facilityIds,
        dataMap: facilityData,
    } = facilities;

    const {
        idList: modalityIds
    } = modalities;

    const {
        company_id
    } = facilityData.get(randomElFromList(facilityIds));

    const modalityCount = modalityIds.size;

    return ( { index, subType = providerTypeRandom } ) => {
        const favorableOdds = beatTheOdds(98, 8);

        const facilityListStr = beatTheOdds(84, 22)
            ? randomElFromList(facilityIds)
            : facilityIds.join();

        const startMod = index % modalityCount;
        const modalityListStr = favorableOdds
            ? modalityIds
                .slice(startMod, startMod + 2)
                .join()
            : randomElFromList(modalityIds);

        const first_name = name.firstName()
            .slice(0, 256);
        const middle_initial = name.firstName()
            .slice(0, Math.ceil(Math.random * 16));
        const last_name = name.lastName()
            .slice(0, 256);
        const suffix = name.suffix()
            .slice(0, 32);
        const full_name = `${last_name}, ${first_name} ${middle_initial} ${suffix}`
            .slice(0, 512);

        const npi = `${aNumber(10)}`;

        return {
            "provider_type": subType,
            // "provider_code": ``,
            "first_name": first_name,
            "middle_initial": middle_initial,
            "last_name": last_name,
            "suffix": suffix,
            "provider_info": {
                "MedicaidProviderNo": aNumber(7),
                "FederalTAXID": aNumber(9),
                "NPI": npi,
                "degree": ``,
                "SSNCode": ``,
                "EIN": ``,
                "MedicareProviderNo": ``,
                "SSN": ``,
                "fee_name": ``,
                "LicenseNo": `${aNumber(4)}-${aNumber(5)}`,
                "ProviderAgreementCode": `-1`,
                "ETIN": ``,
                "MedicareUPIN": ``,
                "fee_id": ``,
                "EINCode": ``,
                "signPrint": ``,
                "DROfficeName": random.words(),
                "TXC": `${aNumber(4)}${aLetter(1)}${aNumber(4)}${aLetter(1)}`
            },
            "facilities": `{${facilityListStr}}`,
            company_id,
            "is_active": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            full_name,
            "specialities": ``,
            "technologist_modalities": `{${modalityListStr}}`,
            "max_tat": index % 3 + 1,
            "sys_provider": false,
            "_id": `${index}`
        };
    };
};
