'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    random,
} = require('faker');

const {
    List,
    Map
} = require('immutable');

const {
    numberUtils,
    // letterUtils,
    // makeUidInstance,
    // makeUidSeries,
    randomElFromList,
    makeUidStudy,
    statusBeforeSchedule,
} = require('../utils');

const {
    makeTime,
    makeTimestampTz,
    timezones,
} = require('../starter-data/shared');

const {
    randomEl,
    nonZeroNumber,
    beatTheOdds,
    aNumber
} = numberUtils;

module.exports = dependentData => {
    const {
        orders,
        facilities,
        modalities,
        modality_rooms,
        // study_status,
        users,
        appointment_types,
        appointment_type_procedures
    } = dependentData;

    const {
        idList: orderIds,
        dataMap: orderData
    } = orders;

    const {
        dataMap: facilityMap
    } = facilities;

    const {
        dataMap: modalityData
    } = modalities;

    const {
        dataMap: modalityRoomData
    } = modality_rooms;

    // const {
    //     idList: studyStatusIds,
    //     dataMap: studyStatusData
    // } = study_status;

    // const statusesByFacility = studyStatusIds.reduce(( statusesByFacility, id ) => {
    //     const status = studyStatusData.get(id);
    //     const statusList = statusesByFacility.get(status.facility_id) || List();
    //     return statusesByFacility.set(status.facility_id, statusList.push(status));
    // }, Map());

    const {
        idList: userIds,
        // dataMap: userData
    } = users;

    const {
        idList: appointmentTypeIds,
        dataMap: appointmentTypeData
    } = appointment_types;

    const {
        idList: appointmentTypeProcedureIds,
        dataMap: appointmentTypeProcedureData
    } = appointment_type_procedures;

    return ( { index } ) => {
        // const favorableOdds = beatTheOdds();

        const order_id = randomElFromList(orderIds);

        if ( !order_id ) {
            throw new Error(`No orders to associate study with`);
        }

        const {
            facility_id,
            modality_id,
            modality_room_id,
            // order_status,
        } = orderData.get(order_id);

        // const hasBeenScheduled = !statusBeforeSchedule({
        //     status_code: order_status,
        //     order_related: true,
        // });

        // const filteredStatuses = hasBeenScheduled
        //     ? statusesByFacility
        //         .get(facility_id)
        //         .filter(status => !statusBeforeSchedule(status))
        //     : statusesByFacility
        //         .get(facility_id);
        //
        // const studyStatus = randomElFromList(filteredStatuses);
        // const {
        //     status_code: study_status
        // } = studyStatus;

        let study_status = ``;
        let study_dt = ``;
        // if ( hasBeenScheduled ) {
        //     study_dt = `${ordered_dt.slice(0, 10)}T${makeTime()}-${randomEl(timezones)}`;
        // }
        // else {
        //     if ( !statusBeforeSchedule(studyStatus) ) {
        //         study_dt = makeTimestampTz();
        //     }
        // }

        const facility = facilityMap.get(facility_id);
        const modalityRoom = modalityRoomData.get(modality_room_id);

        let accession_no = `${aNumber(8)}`.slice(0, 32);

        const reading_physician_id = ``;

        const referring_physician_id = ``;

        let refDescription = ``;

        // @TODO - optimize like other places
        const appointmentTypesFiltered = appointmentTypeIds.filter(id => {
            const apptType = appointmentTypeData.get(id);
            if ( apptType.is_active && apptType.facility_ids.includes(facility_id) && apptType.modality_ids.includes(modality_id) ) {
                if ( modality_room_id && apptType.modality_room_ids.length > 0 ) {
                    return apptType.modality_room_ids.includes(modality_room_id);
                }
                return true;
            }
            return false;
        });

        const appointment_type_id = randomElFromList(appointmentTypesFiltered);
        const appointmentType = appointmentTypeData.get(appointment_type_id);
        const procedure_id = appointmentTypeProcedureIds.find(id => {
            const procedure = appointmentTypeProcedureData.get(id);
            if ( procedure.appointment_type_id === appointment_type_id && procedure.allow_scheduling && procedure.is_active ) {
                return true;
            }
        });

        const department = random.word().slice(0, 64);
        const institution = random.words().slice(0, 128);

        let modalityRoomName = ``;
        if ( modalityRoom ) {
            modalityRoomName = modalityRoom.modality_room_name;
        }

        const modality = modalityData.get(modality_id);
        const modalityCode = modality
            ? modality.modality_code
            : ``;

        return {
            "dicom_study_id": ``,
            "study_uid": makeUidStudy(),
            "study_uid_received": ``,
            "study_guid": ``,
            accession_no,
            study_dt,
            "patient_id": ``,
            order_id,
            referring_physician_id,
            reading_physician_id,
            "institution_id": ``,
            study_status,
            "duration": appointmentType
                ? appointmentType.duration
                : 30,
            "procedure_id": procedure_id,
            "study_description": appointmentType
                ? appointmentType.name.slice(0, 512)
                : random.words().slice(0, 512),
            "body_part": ``,
            department,
            "reason_for_study": random.words(),
            "cpt_codes": `{'${appointmentTypesFiltered
                .map(id => appointmentTypeData.get(id).code)
                .join(`','`)}'}`,
            "orientation": {
                "type": `string`,
                "enum": [
                    ``,
                    `RT`,
                    `LT`
                ]
            },
            "is_mammo_study": modalityCode === `MG`
                ? true
                : ``,
            "stat_level": ``,
            "study_info": {
                "Office": facility.facility_name,
                "marketing_rep": ``,
                "current_status_waiting_time": ``,
                "orderingDesc": ``,
                "study_description": study_status,
                "units": 1.0,
                accession_no,
                "ModalityRooms": modalityRoomName,
                institution,
                "createdby": ``,
                "station": ``,
                "readDescription": ``,
                "refDescription": refDescription || ``,
                department,
                "isPreOrder": false,
                "Modality": modalityCode,
                "preOrderDays": ``,
            },
            "study_attributes": ``,
            "notes": ``,
            "study_received_dt": ``,
            "study_created_dt": ``,
            "last_edited_by": randomElFromList(userIds),
            "last_edited_dt": ``,
            "application_entity_id": ``,
            modality_id,
            facility_id,
            "has_deleted": false,
            "deleted_dt": ``,
            "patient_age": ``,
            "study_details": ``,
            "modalities": modalityData.get(modality_id).modality_code,
            "priority": {
                "type": `string`,
                "enum": [
                    ``,
                    `Low`,
                    `Medium`,
                    `High`
                ]
            },
            "no_of_series": ``,
            "no_of_instances": 0,
            "annotations": ``,
            "dicom_status": `NA`,
            "company_id": 1,
            "tat_level": randomEl([ 0, 1, 2, 3 ]),
            "viewer_series_link_info": ``,
            "viewer_stack_link_info": ``,
            "cpt_code_id": ``,
            institution,
            "row_version": ``,
            "status_last_changed_dt": ``,
            "linked_study_id": ``,
            "provider_group_id": ``,
            "study_started": ``,
            "study_unread_dt": ``,
            "approved_dt": ``,
            "has_locked": ``,
            "locked_dt": ``,
            "unlocked_dt": ``,
            "locked_by": ``,
            "signed_ref_doctor_dt": ``,
            "dictation_started": ``,
            "has_unread_dicoms": ``,
            "status_last_changed_by": ``,
            "dcm_patient_id": ``,
            "is_qc_study": ``,
            "report_count": ``,
            "attorney_provider_id": ``,
            "schedule_dt": study_dt,
            appointment_type_id,
            "schedule_hold": study_status === `RSCH`
                ? true
                : ``,
            "sde_study": ``,
            "_id": `${index}`
        };
    };
};
