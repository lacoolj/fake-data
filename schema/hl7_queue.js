'use strict';

const dayjs = require('dayjs');

const {
    random,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
    TIMESTAMP_FORMAT,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
    aNumber,
} = numberUtils;

module.exports = dependentData => {

    const {
        companies,
        studies,
        hl7_interfaces,
        hl7_messages,
    } = dependentData;

    const {
        idList: companyIds,
        dataMap: companyData,
    } = companies;

    const {
        idList: studyIds,
        dataMap: studyData,
    } = studies;

    const {
        idList: hl7InterfaceIds,
        dataMap: hl7InterfaceData,
    } = hl7_interfaces;

    const {
        idList: hl7MessageIds,
        dataMap: hl7MessageData,
    } = hl7_messages;

    const company_id = randomElFromList(companyIds);

    const currentStatuses = [
        `RE`,
        `ST`,
        `ER`,
    ];

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const queued_dt = makeTimestampTz();

        const current_status = randomEl(currentStatuses);
        const study_id = randomElFromList(studyIds);
        const order_id = ``; // Gotta be done in SQL fixer
        const patient_id = ``; // Gotta be done in SQL fixer
        const hl7_interface_id = randomElFromList(hl7InterfaceIds);
        const hl7_message_id = randomElFromList(hl7MessageIds);
        const input_data = ``; // Gotta be done in SQL fixer
        const has_deleted = false;
        const deleted_dt = ``;
        const status_info = ``; // Gotta be done in SQL fixer

        let sent_dt = ``;
        if ( favorableOdds ) {
            const date = dayjs(queued_dt)
                .add(aNumber(1), `hours`)
                .format(TIMESTAMP_FORMAT);

            sent_dt = `${date}${queued_dt.slice(-6)}`;
        }

        const retries = aNumber(1);

        const ack_received = ``; // Appears unused in prod DBs - leaving empty
        const hl7_text = ``; // Appears unused in prod DBs - leaving empty
        const ack_text = ``; // Appears unused in prod DBs - leaving empty
        const queue_flag = ``; // Appears unused in prod DBs - leaving empty

        return {
            queued_dt,
            company_id,
            current_status,
            study_id,
            order_id,
            patient_id,
            hl7_interface_id,
            hl7_message_id,
            input_data,
            has_deleted,
            deleted_dt,
            status_info,
            sent_dt,
            retries,
            ack_received,
            hl7_text,
            ack_text,
            queue_flag,
            "_id": `${index}`
        };
    };
};
