'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    Map,
} = require('immutable');

const {
    numberUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber
} = numberUtils;

module.exports = dependentData => {
    const {
        studies,
        cpt_codes,
        fee_schedule_cpts,
    } = dependentData;

    const {
        idList: studyIds,
        dataMap: studyData
    } = studies;

    const {
        dataMap: cptCodeData
    } = cpt_codes;

    const {
        idList: feeScheduleCptIds,
        dataMap: feeScheduleCptData
    } = fee_schedule_cpts;

    return ( { index } ) => {
        // const favorableOdds = beatTheOdds();

        const study_id = randomElFromList(studyIds);

        if ( !study_id ) {
            throw new Error(`No studies to associate study_cpt with`);
        }

        const {
            duration,
            duration_proc,
            modifier1_id,
            modifier2_id,
            modifier3_id,
            modifier4_id,
            order_id,
            appointment_type_id,
            modality_room_id,
            procedure_id: cpt_code_id,
            appt_duration,
            appt_display_code,
            appt_display_description
        } = studyData.get(study_id);

        /*const procedure = appointmentTypeProcedureData.get(apptProcedureId);

        // @TODO - add support for modality room durations
        const {
            procedure_id: cpt_code_id,
            duration: duration_proc,
            modifiers,
            modality_room_ids,
            modality_room_durations
        } = procedure;

        const modifierData = modifiers || {};*/

        const cptCode = cptCodeData.get(cpt_code_id) || {
            "display_code": appt_display_code,
            "display_description": appt_display_description,
            "duration": appt_duration || duration_proc || ``,
            "rvu": ``
        };

        const feesByCpt = feeScheduleCptIds.reduce(( feeMap, id ) => {
            const fee = feeScheduleCptData.get(id);
            return feeMap.set(fee.cpt_code_id, fee.global_fee);
        }, Map());

        const fee = feesByCpt.get(cpt_code_id);

        const {
            display_code,
            display_description,
            duration: duration_cpt,
            rvu: rvuValue
        } = cptCode;

        const rvu = typeof rvuValue === `string`
            ? rvuValue
            : ``;

        return {
            study_id,
            cpt_code_id,
            "cpt_code": display_code,
            "study_cpt_info": {
                "total_allowed_fee": 0,
                "cpt_description": display_description,
                "total_bill_fee": fee || 0,
                "modifiers1": modifier1_id || ``,
                "modifiers2": modifier2_id || ``,
                "modifiers3": modifier3_id || ``,
                "modifiers4": modifier4_id || ``,
                "duration": /*duration_proc || */duration_cpt || duration,
                "reOrder": ``,
                "allowed_fee": 0
            },
            "units": `${aNumber(1)}.0`,
            "modifier1_id": modifier1_id || ``,
            "modifier2_id": modifier2_id || ``,
            "modifier3_id": modifier3_id || ``,
            "modifier4_id": modifier4_id || ``,
            order_id,
            "bill_fee": fee || `$0.00`,
            "has_deleted": false,
            "deleted_dt": ``,
            "authorization_info": {
                "Primary": ``,
                "Secondary": ``,
                "Tertiary": ``
            },
            rvu,
            "_id": `${index}`
        };
    };
};
