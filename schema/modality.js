'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    random
} = require('faker');

const {
    numberUtils
} = require('../utils');

const {
    beatTheOdds
} = numberUtils;

module.exports = () => ( { index } ) => {
    const favorableOdds = beatTheOdds();

    const modality_name = random.words()
        .slice(0, 64);

    const modality_code = modality_name
        .replace(/[a-z]{3,}(?=\s)/g, ``)
        .replace(/\s*/g, ``)
        .slice(0, 16)
        .toUpperCase();

    return {
        modality_code,
        modality_name,
        "is_active": favorableOdds,
        "has_deleted": false,
        "deleted_dt": ``,
        "company_id": 1,
        "priority": index,
        "_id": `${index}`
    };
};
