'use strict';

const {
    random
} = require('faker');

const {
    numberUtils,
    randomElFromList,
    getColor
} = require('../utils');

const {
    aNumber,
    beatTheOdds
} = numberUtils;

module.exports = dependentData => {
    const {
        facilities
    } = dependentData;

    const {
        idList: facilityIds
    } = facilities;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        const status_desc = random.words()
            .slice(0, 32);

        const status_code = status_desc
            .replace(/[a-z]+(?=\s)/g, ``)
            .replace(/\s*/g, ``)
            .slice(0, 4)
            .toUpperCase();

        const facility_id = randomElFromList(facilityIds);

        return {
            status_code,
            status_desc,
            "color_code": `#${getColor()}`,
            facility_id,
            "order_related": !favorableOdds,
            "can_edit": favorableOdds,
            "can_delete": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            "rad_related": !favorableOdds,
            "display_order": aNumber(2),
            "waiting_time": favorableOdds ? `` : aNumber(2),
            "status_validation": ``,
            "show_right_click": favorableOdds,
            "show_validation_window": ``,
            "mobile_rad_related": !favorableOdds && !beatTheOdds(),
            "_id": `${index}`
        };
    };
};
