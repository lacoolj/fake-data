'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    random
} = require('faker');

const {
    email,
    phone,
    address1,
    address2,
    city,
    state,
    zip,
    website
} = require('../starter-data/shared');

const {
    numberUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber,
    beatTheOdds
} = numberUtils;

module.exports = dependentData => {
    const {
        facilities
    } = dependentData;

    const {
        idList: facilityIds
    } = facilities;

    const facilityCount = facilityIds.size;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        const insurance_name = random.words()
            .slice(0, 64);

        const insurance_code = insurance_name
            .replace(/[a-z]{3,}(?=\s)/g, ``)
            .replace(/\s*/g, ``)
            .slice(0, 20)
            .toUpperCase();

        const start = index % facilityCount;
        const facilityListStr = favorableOdds
            ? facilityIds
                .slice(start, start + 5)
                .join()
            : randomElFromList(facilityIds);

        return {
            insurance_code,
            insurance_name,
            "insurance_info": {
                "authCHDescription": ``,
                "claimCHCode": ``,
                "eligibilityClearingHouse": ``,
                "eligibilityRequestTemplateType": `-1`,
                "authCHCode": ``,
                "providerType": 15,
                "claimClearingHouse": ``,
                "medicareInsuranceType": ``,
                "sopPayer": ``,
                "isBatchMode": false,
                "partner_id": `anthem_blue_cross_ca`,
                "authRequestTemplateType": `-1`,
                "PhoneNo": phone,
                "isElectronicAuthorization": favorableOdds,
                "FaxNo": `(555)555-${aNumber(4)}`,
                "ZipPlus": aNumber(4),
                "ediCode": `F`,
                "eligibilityCHDescription": ``,
                "eligibilityCHCode": ``,
                "claim_req_type_prov": `-1`,
                "b2bReceiver": ``,
                "isREalTime": false,
                "claimFileIndicatorCode": `CI`,
                "eligibilityRequestTemplate": `-1`,
                "medigapNumber": ``,
                "ZipCode": zip,
                "billingMethod": {
                    "type": `string`,
                    "enum": [
                        `EB`, // Electronic Billing
                        `PC`, // Paper Claim
                        `PP`, // Patient Payment
                        `DB`  // Direct Billing
                    ]
                },
                "PayerID": aNumber(5),
                "Address2": address2,
                "claimRequestTemplateType": `-1`,
                "claimRequestTemplate": `-1`,
                "City": city,
                "Address1": address1,
                "authClearingHouse": ``,
                "isMedicarePayer": !favorableOdds,
                "fee_name": ``,
                "billingMethodBy": ``,
                "useLegacyID": false,
                "legacyType": `-1`,
                "authRequestTemplate": `-1`,
                "State": state,
                "isElectronicVerification": favorableOdds,
                "fee_id": ``,
                "claim_req_temp_prov": `-1`,
                "claimCHDescription": ``
            },
            "is_active": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            "company_id": 1,
            "facility_id": `{${facilityListStr}}`,
            "all_facilities": favorableOdds,
            "_id": `${index}`
        };
    };
};
