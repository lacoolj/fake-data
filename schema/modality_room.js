'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    random
} = require('faker');

const {
    years,
    months,
    days,
    hours,
    minutes
} = require('../starter-data/shared');

const {
    numberUtils,
    letterUtils,
    getColor,
    randomElFromList
} = require('../utils');

const {
    randomEl,
    aNumber,
    nonZeroNumber,
    beatTheOdds
} = numberUtils;

const {
    aLetter
} = letterUtils;

const roomTimes = [];
for ( let i = 0; i < 50; ++i ) {
    roomTimes.push(`${randomEl(hours)}:${randomEl(minutes)}:00`);
}

module.exports = dependentData => {
    const {
        facilities,
        modalities
    } = dependentData;

    const {
        idList: facilityIds
    } = facilities;

    const {
        idList: modalityIds
    } = modalities;

    const modalityCount = modalityIds.size;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();
        const color = getColor();

        const start = index % modalityCount;
        const modalityList = favorableOdds
            ? modalityIds.slice(start, start + 2)
            : [ randomElFromList(modalityIds) ];

        const dates = [
            `${2018 - aNumber()}-${randomEl(months)}-${randomEl(days)}`,
            `${2018 + aNumber()}-${randomEl(months)}-${randomEl(days)}`
        ].sort();

        const fromDate = dates[ 0 ];
        const toDate = dates[ 1 ];

        const times = [
            randomEl(roomTimes),
            randomEl(roomTimes),
            randomEl(roomTimes),
            randomEl(roomTimes)
        ].sort();

        const weekdayFromTime = times[ 0 ];
        const weekendFromTime = times[ 1 ];
        const weekdayToTime = times[ 2 ];
        const weekendToTime = times[ 3 ];

        const modality_room_name = random.words()
            .slice(0, 64);

        const modality_room_code = modality_room_name
            .replace(/[a-z]{3,}(?=\s)/g, ``)
            .replace(/\s*/g, ``)
            .slice(0, 8)
            .toUpperCase();

        return {
            modality_room_code,
            modality_room_name,
            "display_order": aNumber(2),
            "color_code": `#${color}`,
            "modalities": `{${modalityList.join()}}`,
            "from_date": fromDate,
            "to_date": toDate,
            "mon_from_time": weekdayFromTime,
            "mon_to_time": weekdayToTime,
            "tue_from_time": weekdayFromTime,
            "tue_to_time": weekdayToTime,
            "wed_from_time": weekdayFromTime,
            "wed_to_time": weekdayToTime,
            "thu_from_time": weekdayFromTime,
            "thu_to_time": weekdayToTime,
            "fri_from_time": weekdayFromTime,
            "fri_to_time": weekdayToTime,
            "sat_from_time": weekendFromTime,
            "sat_to_time": weekendToTime,
            "sun_from_time": weekendFromTime,
            "sun_to_time": weekendToTime,
            "allow_scheduling": favorableOdds,
            "is_active": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            "facility_id": randomElFromList(facilityIds),
            "application_entity_id": ``,
            "more_info": {
                "current_linked_resource": ``,
                "linked_tech_id": ``,
                "linked_tech_name": ``,
                "linked_vechicle_name": ``,
                "linked_vechicle_id": ``,
                "linked_tech_code": ``
            },
            "_id": `${index}`
        };
    };
};
