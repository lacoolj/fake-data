'use strict';

const {
    name
} = require('faker');

const {
    months,
    days,
    gender,
    phone,
    address1,
    address2,
    city,
    state,
    zip,
    medicareInsuranceTypeCode,
    makeDate,
    makeFutureDate,
    makeTimestampTz,
    makeFutureTimestampTz,
} = require('../starter-data/shared');

const {
    numberUtils,
    letterUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber,
    beatTheOdds,
    randomEl,
} = numberUtils;

const {
    aLetter
} = letterUtils;

module.exports = dependentData => {
    const {
        insurance_providers,
        patients,
        relationship_status,
        employment_status,
    } = dependentData;

    const {
        idList: insuranceProviderIds
    } = insurance_providers;

    const {
        idList: patientIds,
        dataMap: patientData
    } = patients;

    const {
        idList: relationshipStatusIds,
        dataMap: relationshipStatusData,
    } = relationship_status;

    const {
        idList: employmentStatusIds,
    } = employment_status;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();
        const patient_id = randomElFromList(patientIds);
        const patient = patientData.get(patient_id);

        const subscriber_relationship_id = randomElFromList(relationshipStatusIds);
        const relationshipStatus = relationshipStatusData.get(subscriber_relationship_id);


        let subscriberInfo = null;
        let subscriber_employment_status_id = ``;
        if ( relationshipStatus ) {
            subscriberInfo = {
                subscriber_firstname: ``,
                subscriber_lastname: ``,
                subscriber_middlename: ``,
                subscriber_name_suffix: ``,
                subscriber_dob: ``,
                subscriber_gender: ``,

                subscriber_address_line1: address1,
                subscriber_address_line2: address2,
                subscriber_city: city,
                subscriber_state: state,
                subscriber_zipcode: zip,
                work_phone_number: phone,
                home_phone_number: phone,
            };

            subscriber_employment_status_id = patient.employment_status_id || ``;

            if ( !/self/i.test(relationshipStatus.description) ) {
                subscriberInfo.subscriber_firstname = name.firstName();
                subscriberInfo.subscriber_lastname = name.lastName();
                subscriberInfo.subscriber_middlename = name.firstName();
                subscriberInfo.subscriber_name_suffix = name.suffix();
                subscriberInfo.subscriber_dob = `19${aNumber(2)}-${randomEl(months)}-${randomEl(days)}`;
                subscriberInfo.subscriber_gender = gender;
                subscriber_employment_status_id = randomElFromList(employmentStatusIds) || ``;
            }
        }

        return {
            patient_id,
            "insurance_provider_id": randomElFromList(insuranceProviderIds),
            subscriber_relationship_id,
            subscriber_employment_status_id,

            "valid_from_date": makeDate(),
            "valid_to_date": makeFutureDate(),
            "medicare_insurance_type_code": favorableOdds
                ? ``
                : medicareInsuranceTypeCode,

            "coverage_level": {
                "type": `string`,
                "enum": [
                    `primary`, // Primary
                    `secondary`, // Secondary
                    `tertiary`  // Tertiary
                ]
            },
            "policy_number": aNumber(8),
            "group_name": {
                "type": `string`,
                "faker": `company.companyName`
            },
            "group_number": `${aLetter(1)}${aNumber(5)}`,

            ...subscriberInfo,

            "_id": `${index}`
        };
    };
};
