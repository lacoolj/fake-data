'use strict';

const {
    random,
    name,
} = require('faker');

const {
    fileTypes,
    documentSources
} = require('../starter-data/document');

const {
    numberUtils,
    letterUtils,
    randomElFromList,
    // makeUidInstance,
    // makeUidSeries
} = require('../utils');

const {
    aLetter,
} = letterUtils;

const {
    randomEl,
    nonZeroNumber,
    beatTheOdds,
    aNumber,
} = numberUtils;

const {
    // years,
    // months,
    // days,
    // hours,
    // minutes,
    makeDate,
    makeTimestampTz,
} = require('../starter-data/shared');

module.exports = dependentData => {
    const {
        companies,
        facilities,
        payment_reasons,
        patients,
        provider_contacts,
        provider_groups,
        insurance_providers,
        users,
    } = dependentData;

    const {
        idList: companyIds,
    } = companies;

    const {
        idList: facilityIds,
    } = facilities;

    const {
        idList: paymentReasonIds,
    } = payment_reasons;

    const {
        idList: patientIds,
    } = patients;

    const {
        idList: providerContactIds,
    } = provider_contacts;

    const {
        idList: providerGroupIds,
    } = provider_groups;

    const {
        idList: insuranceProviderIds,
    } = insurance_providers;

    const {
        idList: userIds,
    } = users;

    const company_id = randomElFromList(companyIds);

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        const facility_id = randomElFromList(facilityIds);

        let patient_id = ``;
        let insurance_provider_id = ``;
        let provider_group_id = ``;
        let provider_contact_id = ``;
        if ( favorableOdds ) {
            insurance_provider_id = randomElFromList(insuranceProviderIds);
        }
        else {
            if ( beatTheOdds() ) {
                patient_id = randomElFromList(patientIds);
            }
            else {
                if ( !beatTheOdds() ) {
                    provider_contact_id = randomElFromList(providerContactIds)
                }
                else {
                    provider_group_id = randomElFromList(providerGroupIds);
                }
            }
        }

        if ( !patient_id && !insurance_provider_id && !provider_contact_id && !provider_group_id ) {
            throw new Error(`No association for payment available`);
        }

        const payment_reason_id = randomElFromList(paymentReasonIds);
        const amount = `$${favorableOdds ? nonZeroNumber(4) : nonZeroNumber(3)}.${aNumber(2)}`;
        const payment_dt = makeTimestampTz();
        const claim_dt = payment_dt.slice(0, 10);
        const created_by = randomElFromList(userIds);

        const payer_type = patient_id
            ? `patient`
            : insurance_provider_id
                ? `insurance`
                : provider_group_id
                    ? `ordering_facility`
                    : provider_contact_id
                        ? `ordering_provider`
                        : ``;

        const invoice_no = favorableOdds
            ? `${aLetter(4)}${aNumber(9)}`
            : ``;

        const claim_notes = random.words();


        return {
            company_id,
			facility_id,
			patient_id,
			billing_provider_id,
			place_of_service_id,
			billing_code_id,
			billing_class_id,
			created_by,
			billing_method,
			billing_notes,
			claim_dt,
			current_illness_date,
			same_illness_first_date,
			unable_to_work_from_date,
			unable_to_work_to_date,
			hospitalization_from_date,
			hospitalization_to_date,
			claim_notes,
			original_reference,
			authorization_no,
			frequency,
			is_auto_accident,
			is_other_accident,
			is_employed,
			service_by_outside_lab,
			payer_type,
			claim_status_id,
			rendering_provider_contact_id,
			primary_patient_insurance_id,
			secondary_patient_insurance_id,
			tertiary_patient_insurance_id,
			ordering_facility_id,
			referring_provider_contact_id,
            "_id": `${index}`
        }

    };

};
