'use strict';

const {
    random,
    internet,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
    aNumber,
} = numberUtils;

module.exports = dependentData => {

    const {
        companies,
    } = dependentData;

    const {
        idList: companyIds,
    } = companies;

    const company_id = randomElFromList(companyIds);

    const interfaceTypes = [
        `S`,
        `R`,
    ];

    const connectionTypes = [
        `T`,
        `F`,
        `FT`,
        `DB`,
    ];

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const interface_name = random.words();
        const interface_type = randomEl(interfaceTypes);
        const connection_type = randomEl(connectionTypes);
        const connection_info = {
            "tcp_port": aNumber(4),
            "waitForAck": favorableOdds,
            "max_retries": aNumber(1),
            "frame_end": `0x1C 0xD`,
            "tcp_host": internet.ip(),
            "line_seperator": `0xD`,
            "frame_start": `0x0B`,
            "encoding_type": `Select`,
            "time_out": `5000`,
        };
        const header_info = {
            "receiving_facility": random.words(),
            "receiver": random.words(),
            "sender": random.words(),
            "field_seperator": `|`,
            "encode_char": `^~\&`,
            "sending_facility": random.words(),
            "version": `2.5.1`,
        };

        const has_deleted = false;
        const deleted_dt = ``;
        const is_active = favorableOdds;

        return {
            interface_name,
            interface_type,
            connection_type,
            connection_info,
            header_info,
            company_id,
            has_deleted,
            deleted_dt,
            is_active,
            "_id": `${index}`
        };
    };
};
