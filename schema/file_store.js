'use strict';

const {
    random
} = require('faker');

const {
    numberUtils
} = require('../utils');

const {
    beatTheOdds
} = numberUtils;

module.exports = () => ({ index }) => {
    const favorableOdds = beatTheOdds(98, 8);

    const file_store_name = random.words()
        .slice(0, 64);

    return {
        file_store_name,
        "root_directory": `/z/km/exa-uploads`,
        "server_status": {
            "type": `string`,
            "enum": [
                `1`,
                `I`,
                ``
            ]
        },
        "notes": {
            "type": `string`,
            "faker": `lorem.words`
        },
        "has_deleted": false,
        "deleted_dt": ``,
        "company_id": 1,
        "has_registered": !favorableOdds,
        "is_default": false,
        "_id": `${index}`
    };
};
