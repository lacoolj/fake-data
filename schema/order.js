'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    random
} = require('faker');

const {
    List,
    Map
} = require('immutable');

const {
    years,
    months,
    days,
    hours,
    minutes,
    makeDate,
    makeTime,
    makeTimestampTz,
    makeFutureDate,
    makeFutureTimestampTz,
    timezones,
} = require('../starter-data/shared');

const {
    numberUtils,
    // letterUtils,
    // makeUidInstance,
    // makeUidSeries,
    randomElFromList,
    statusBeforeSchedule,
} = require('../utils');

const {
    randomEl,
    nonZeroNumber,
    beatTheOdds,
    aNumber
} = numberUtils;

module.exports = dependentData => {
    const {
        facilities,
        modalities,
        modality_rooms,
        patients,
        insurance_providers,
        patient_insurances,
        provider_contacts,
        sites,
        study_status,
        users
    } = dependentData;

    const {
        idList: facilityIds,
    } = facilities;

    const {
        idList: modalityIds,
    } = modalities;

    const {
        idList: modalityRoomIds,
        dataMap: modalityRoomData
    } = modality_rooms;

    const {
        idList: patientIds,
    } = patients;

    let {
        idList: patientInsuranceIds,
        dataMap: patientInsuranceData
    } = patient_insurances;

    const patientInsuranceMaps = patientInsuranceIds.reduce(( map, id ) => {
        const {
            patient_id,
            coverage_level,
        } = patientInsuranceData.get(id);

        map[ coverage_level ] = map[ coverage_level ].set(patient_id, id);

        return map;
    }, {
        'primary': Map(),
        'secondary': Map(),
        'tertiary': Map(),
    });

    const {
        idList: providerContactIds,
        dataMap: providerContactData
    } = provider_contacts;

    const providerContactsByType = providerContactIds.reduce(( providerContactsByType, id ) => {
        const provider = providerContactData.get(id);

        return providerContactsByType.update(
            provider.provider_type,
            ( providers = List() ) => providers.push(provider)
        );
    }, Map());

    const {
        dataMap: siteData
    } = sites;

    const stat_levels = List(siteData.get(1).stat_levels);

    const {
        idList: studyStatusIds,
        dataMap: studyStatusData
    } = study_status;

    const statusesByFacility = studyStatusIds.reduce(( statusesByFacility, id ) => {
        const status = studyStatusData.get(id);
        const statusList = statusesByFacility.get(status.facility_id) || List();
        return statusesByFacility.set(status.facility_id, statusList.push(status));
    }, Map());

    const {
        idList: userIds,
        // dataMap: userData
    } = users;

    return ( { index } ) => {
        const favorableOdds = beatTheOdds();

        const date = makeDate();
        const ordered_dt = `${date}T${makeTime()}-${randomEl(timezones)}`;

        let scheduled_dt = ``;
        // Most likely scheduled
        if ( beatTheOdds() ) {
            // Not as likely is in the future
            if ( !beatTheOdds() ) {
                scheduled_dt = makeFutureTimestampTz();
            }
            else {
                scheduled_dt = makeTimestampTz();
            }
        }

        const patient_id = randomElFromList(patientIds);
        const ordered_by = randomElFromList(userIds);

        let modality_room_id = ``;
        let modality_id = ``;
        let facility_id = ``;
        let filteredStatuses;
        if ( scheduled_dt ) {
            modality_room_id = randomElFromList(modalityRoomIds);
            const modalityRoom = modalityRoomData.get(modality_room_id);
            modality_id = randomEl(modalityRoom.modalities);
            facility_id = modalityRoom.facility_id;

            filteredStatuses = statusesByFacility
                .get(facility_id)
                .filter(status => !statusBeforeSchedule(status));
        }
        else {
            modality_id = randomElFromList(modalityIds);
            facility_id = randomElFromList(facilityIds);

            filteredStatuses = statusesByFacility
                .get(facility_id);
        }

        const {
            status_code: order_status,
            status_desc: order_status_desc
        } = randomElFromList(filteredStatuses);

        const referring_provider_ids = [];
        const referring_providers = ``;

        for ( let i = 0; i < nonZeroNumber(1); ++i ) {
            const providerContact = randomElFromList(providerContactsByType.get(`RF`));
            referring_provider_ids.push(providerContact.id);
        }

        const readingProviderContact = randomElFromList(providerContactsByType.get(`PR`));
        const statObj = randomElFromList(stat_levels);

        return {
            patient_id,
            facility_id,
            modality_id,
            modality_room_id,
            ordered_by,
            ordered_dt,
            scheduled_dt,
            "order_type": {
                "type": `string`,
                "enum": scheduled_dt
                    ? [
                        `S`,
                        `S`,
                        `S`,
                        `S`,
                        `S`,
                        `S`,
                        `S`,
                        `W`,
                        `W`,
                        `W`
                    ]
                    : [
                        `P`,
                    ]
            },
            order_status,
            order_status_desc,
            "order_info": {
                "referralNo": ``,
                "dispatch_contact_name": ``,
                "dispatch_contact_number": ``,
                "billing_method": `PC`,
                "isMedicationReconciliation": false,
                "bill_fee": 0.00,
                "emp": !favorableOdds,
                "currentDate": !favorableOdds
                    ? date
                    : ``,
                "source": {
                    "type": `string`,
                    "enum": [
                        `Attorney`,
                        `Emergency Room`,
                        `Clinic Referral`,
                        `Walk-in`
                    ]
                },
                "payer": `Primary Insurance`,
                "jos": random.words(),
                "billing_provider": ``,
                "payer_billing_method": `PC`,
                "oa": false,
                "pos_map_code": ``,
                "ordering_facility_id": 2,
                "patientRoom": aNumber(3),
                "balance": 0.00,
                "pos": ``,
                "createdby": ``,
                // "createdby": userData.get(ordered_by).full_name,
                "requestingDate": ``,
                "dispatch_address": ``,
                "is_rejected": false,
                "referring_providerloginID": 0,
                "isPreOrder": false,
                "patient_age": 40,
                "allowed_fee": 0.00,
                "aa": false,
                "billing_payer_type": `PIP`,
                "stat": ~~statObj.level,
                "pos_type_code": ``,
                "patientLocation": {
                    "type": `string`,
                    "enum": [
                        `ER`,
                        `ICU`,
                        `Home`
                    ]
                },
                "google_job_id": 0,
                "isquickappt_from": `facility_login`,
                "payer_type": `PIP`,
                "transport": `Select`,
                "ordering_facility": `KMHA Garner`,
                "appontmentReminder": false,
                "orderContact": ``,
                "patientEmail": `[object Object]`,
                "patientCondition": {
                    "type": `string`,
                    "enum": [
                        `Wheelchair`,
                        `Ambulatory`,
                        `Bed-ridden`,
                        `Walker`
                    ]
                },
                "adjustment": 0,
                // "billing_payer_id": insurancePayers[ 0 ],
                // "payer_id": insurancePayers[ 0 ],
                // "payer_name": insuranceNames[ 0 ],
                // "billing_payer_name": insuranceNames[ 0 ]
            },
            "cpt_codes": ``,
            "icd_codes": favorableOdds
                ? `{72${aNumber()}.${aNumber()}}`
                : ``,
            "primary_patient_insurance_id": patientInsuranceMaps.primary.get(patient_id) || ``,
            "secondary_patient_insurance_id": patientInsuranceMaps.secondary.get(patient_id) || ``,
            "tertiary_patient_insurance_id": patientInsuranceMaps.tertiary.get(patient_id) || ``,
            "referring_provider_ids": `{${referring_provider_ids.join()}}`,
            referring_providers,
            // "referring_providers": `{'${referring_providers.join(`','`)}'}`,
            "order_notes": ``,
            "order_guid": ``,
            "order_source": ``,
            "is_quick_appt": false,
            "has_deleted": false,
            "deleted_dt": ``,
            "company_id": 1,
            "duration": 30,
            "scheduled_date": date,
            "in_hold": ``,
            "has_rescheduled": ``,
            "has_approved": ``,
            "provider_id": ``,
            "ordering_providers": ``,
            "linked_rad_orders": ``,
            "history_info": ``,
            "cc_ros": ``,
            "reading_providers": `{${readingProviderContact.id}}`,
            "vehicle_id": ``,
            "technologist_id": ``,
            "mu_validation_data": ``,
            "mu_passed": ``,
            "mu_last_updated": ``,
            "_id": `${index}`
        };
    };
};
