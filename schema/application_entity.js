'use strict';
// @TODO - not used right now
const {
    random,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
} = numberUtils;

module.exports = dependentData => {

    const {
        companies,
        file_stores,
        facilities,
    } = dependentData;

    const {
        idList: companyIds,
        dataMap: companyData,
    } = companies;

    const {
        idList: fileStoreIds,
        dataMap: fileStoreData,
    } = file_stores;

    const {
        idList: facilityIds,
        dataMap: facilityData,
    } = facilities;

    return ({ index }) => {
        // const company_id = randomElFromList(companyIds);

        return {
            // "has_deleted": false,
            // "deleted_dt": ``,
            // company_id,
            "_id": `${index}`
        };
    };
};
