'use strict';

const {
    email,
    phone,
    address1,
    address2,
    city,
    stateAbbr,
    zip
} = require('../starter-data/shared');

const {
    numberUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber,
    beatTheOdds
} = numberUtils;


module.exports = dependentData => {
    const {
        providers
    } = dependentData;

    const {
        idList: providerIds
    } = providers;

    return ( { index } ) => {
        const favorableOdds = beatTheOdds(98, 8);

        const providerId = randomElFromList(providerIds);

        return {
            "provider_id": providerId,
            "contact_info": {
                "ZIP": zip,
                "MOBNO": phone,
                "STATE_NAME": stateAbbr,
                "PAGRNO": phone,
                "FAXNO": `(555)555-${aNumber(4)}`,
                "STATE": stateAbbr,
                "EMAIL": email,
                "NAME": ``,
                "ADDR2": address2,
                "mePrinter": ``,
                "ADDR1": address1,
                "CITY": city,
                "ZIPPLUS": aNumber(4),
                "Fax_me": {
                    "type": `string`,
                    "enum": [
                        `1`,
                        ``
                    ]
                },
                "report_password": {
                    "type": `string`,
                    "faker": `random.word`
                },
                "PHNO": phone,
                "OFPHNO": phone,
                "OFFAXNO": `(555)555-${aNumber(4)}`
            },
            "provider_group_id": ``,
            "is_primary": false,
            "is_active": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            // "dicom_name": ``,
            "_id": `${index}`
        };
    };
};
