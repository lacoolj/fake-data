'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    random
} = require('faker');

const {
    numberUtils,
    getColor
} = require('../utils');

const {
    beatTheOdds
} = numberUtils;

module.exports = () => ( { index } ) => {
    const favorableOdds = beatTheOdds();
    const color = getColor();

    return {
        "company_id": 1,
        "name": {
            "type": `string`,
            "faker": `random.words`
        },
        "is_active": favorableOdds,
        "deleted_dt": ``,
        "needs_review": !favorableOdds,
        "color": `#${color}`,
        "icon": ``,
        "_id": `${index}`
    };
};
