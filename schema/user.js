'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const bCrypt = require('bcrypt');

const {
    name
} = require('faker');

const {
    numberUtils,
    letterUtils,
    randomElFromList
} = require('../utils');

const {
    aNumber,
    beatTheOdds,
    randomEl
} = numberUtils;

const {
    aLetter
} = letterUtils;

module.exports = dependentData => {
    const {
        user_groups,
        providers,
        facilities
    } = dependentData;

    const {
        idList: userGroupIds
    } = user_groups;

    const {
        idList: providerIds,
        dataMap: providerData
    } = providers;

    const {
        idList: facilityIds
    } = facilities;

    const userGroupCount = userGroupIds.size;
    const providerCount = providerIds.size;
    const facilityCount = facilityIds.size;

    const salt = bCrypt.genSaltSync(8);
    const password = bCrypt.hashSync(`password123`, salt);

    return ( { index } ) => {
        const favorableOdds = beatTheOdds(98, 8);

        let num1 = aNumber();
        let num2 = index % facilityCount;
        if ( num1 >= num2 || num2 === 0 ) {
            num1 = 0;
            num2 = -1;
        }

        const defaultFacility = randomElFromList(facilityIds);
        const facilityList = favorableOdds
            ? [ defaultFacility ]
            : facilityIds;

        const provider_id = index % 88 === 0
            ? randomElFromList(providerIds)
            : ``;

        let first_name = ``;
        let middle_initial = ``;
        let last_name = ``;
        let suffix = ``;
        let user_type = `NU`;

        if ( provider_id > 0 ) {
            const provider = providerData.get(provider_id);
            switch ( provider.provider_type ) {
                case `RF`:
                    user_type = `PR`;
                    break;
                case `TG`:
                    user_type = `TG`;
                    break;
                case `AT`:
                    user_type = `AT`;
                    break;
            }
        }
        else {
            first_name = name.firstName()
                .slice(0, 64);
            middle_initial = name.firstName()
                .slice(0, Math.ceil(Math.random * 16));
            last_name = name.lastName()
                .slice(0, 64);
            suffix = name.suffix()
                .slice(0, 16);
        }

        return {
            "username": {
                "type": `string`,
                "faker": `internet.userName`
            },
            salt,
            password,
            first_name,
            last_name,
            middle_initial,
            suffix,
            "user_group_id": randomElFromList(userGroupIds),
            "facilities": `{${facilityList.join()}}`,
            "company_id": 1,
            "permissions": ``,
            "is_active": favorableOdds,
            "has_deleted": false,
            "deleted_dt": ``,
            "ad_user_id": ``,
            user_type,
            provider_id,
            "default_facility_id": defaultFacility,
            "user_settings": ``,
            "viewer_config": ``,
            "user_source": ``,
            "google_id": ``,
            "all_facilities": !favorableOdds,
            "marketing_rep_info": ``,
            "user_permissions": ``,
            "ordering_facilities": ``,
            "password_changed_dt": ``,
            "expiration_dt": ``,
            "one_time_access": ``,
            "chat_status": {
                "type": `string`,
                "enum": [
                    `online`,
                    `online`,

                    `offline`,
                    `offline`,
                    `offline`,
                    `offline`,
                    `offline`,
                    `offline`,

                    `busy`,
                    `dnd`,
                ]
            },
            "_id": `${index}`
        };
    };
};
