'use strict';

const {
    numberUtils: {
        beatTheOdds,
    },
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

module.exports = dependentData => {
    const {
        companies,
    } = dependentData;

    const {
        idList: companyIds
    } = companies;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        return {
            "company_id": companyIds.first(),
            "inactivated_dt": !favorableOdds
                ? makeTimestampTz()
                : ``,
            /**
             * THIS DOESNT ACTUALLY MAKE SENSE - SHOULD NEVER BE USED
             */
            "description": {
                "type": `string`,
                "enum": [
                    `Part time`,
                    `Full time`,
                    `Full time`,
                    `Full time`,
                    `Full time`,
                    `Full time`,
                    `Full time`,
                    `Self-employed`,
                    `Active Duty Military`,
                    `Active Duty Military`,
                    `Active Duty Military`,
                    `Active Duty Military`,
                    `Retired Military`,
                    `Retired Military`,
                    `Retired Military`,
                    `Veteran`,
                    `Veteran`,
                    `Veteran`,
                    `Student`,
                    `Student`,
                    `Unemployed`,
                    `Other`,
                ],
            },
            "_id": `${index}`
        };
    };
};
