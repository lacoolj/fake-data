'use strict';

const rtf = require('html-to-rtf');
const dayjs = require('dayjs');

const {
    List
} = require('immutable');

const {
    random,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
    TIMESTAMP_FORMAT,
} = require('../utils');

const {
    beatTheOdds,
    randomEl,
} = numberUtils;

const {
    makeTimestampTz,
} = require('../starter-data/shared');

module.exports = dependentData => {

    const {
        studies,
        users,
        provider_contacts,
    } = dependentData;

    const {
        idList: studyIds,
        dataMap: studyData,
    } = studies;

    const {
        idList: userIds,
        dataMap: userData,
    } = users;

    const {
        idList: providerContactIds,
        dataMap: providerContactData,
    } = provider_contacts;

    const providerMap = providerContactIds.reduce(( map, id ) => {
        const provider = providerContactData.get(id);
        const list = map.get(provider.provider_type) || List();
        map.set(provider.provider_type, list.push(id));

        return map;
    }, new Map());

    const listOfRadiologists = providerMap.get(`PR`);

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const study_id = randomElFromList(studyIds);
        const created_by = randomElFromList(userIds);
        const created_dt = makeTimestampTz();

        let transcription_type = `F`;
        if ( !favorableOdds ) {
            transcription_type = {
                "type": `string`,
                "enum": [
                    `A`,
                    `A`,
                    `A`,
                    `A`,
                    `P`,
                    `P`,
                ],
            };
        }

        const locked_dt = !favorableOdds
            ? `${dayjs(created_dt).add(1, 'hour').format(TIMESTAMP_FORMAT)}${created_dt.slice(-6)}`
            : ``;

        /*let transcription_text = random.words().toUpperCase();
        transcription_text += `\\n\\nTECHNIQUE\\n`;
        transcription_text += random.words();
        transcription_text += random.words();
        transcription_text += `\\n\\nFINDINGS\\n`;
        transcription_text += random.words();
        transcription_text += random.words();
        transcription_text += random.words();
        transcription_text += `\\n\\nIMPRESSION\\n`;
        transcription_text += random.words();

        let text = transcription_text.replace(`\\n`, '<br/>');
        let html = `<html><body>${text}</body></html>`;

        let transcription_data = rtf.convertHtmlToRtf(html);*/

        let approving_provider_id = ``;
        let approved_dt = ``;
        if ( favorableOdds && !locked_dt ) {
            approving_provider_id = randomElFromList(listOfRadiologists);
            const date = dayjs(created_dt)
                .add(1, 'day')
                .format(TIMESTAMP_FORMAT);

            approved_dt = `${date}${created_dt.slice(-6)}`;
        }

        return {
            study_id,
            "transcription_data": ``,
            created_by,
            created_dt,
            "updated_dt": created_dt,
            transcription_type,
            "text_type": `RT`, // Don't see any examples of it being anything else (HTML or plain text)
            "addendum_no": 0, // Fill this in with separate script
            "linked_studies": ``,
            approving_provider_id,
            "has_locked": !favorableOdds
                ? true
                : ``,
            locked_dt,
            "unlocked_dt": !locked_dt
                ? ``
                : `${dayjs(locked_dt).add(10, 'minutes').format(TIMESTAMP_FORMAT)}${locked_dt.slice(-6)}`,
            "transcription_text": ``,
            approved_dt,
            "transcribing_user": favorableOdds
                ? ``
                : randomElFromList(userIds),
            "pre_approved_provider_contact_id": ``,
            "pre_approved_dt": ``,
            "_id": `${index}`
        };
    };
};
