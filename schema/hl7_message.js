'use strict';

const {
    random,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
    letterUtils,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
    aNumber,
} = numberUtils;

const {
    aLetter,
} = letterUtils;

module.exports = dependentData => {

    const {
        companies,
        hl7_interfaces,
    } = dependentData;

    const {
        idList: companyIds,
        dataMap: companyData,
    } = companies;

    const {
        idList: hl7InterfaceIds,
        dataMap: hl7InterfaceData,
    } = hl7_interfaces;

    const company_id = randomElFromList(companyIds);

    const messageTypes = [
        `ORM`,
        `DFT`,
        `ORU`,
        `ADT`,
        `SIU`,
    ];

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const hl7_interface_id = randomElFromList(hl7InterfaceIds);
        const trigger_name = random.words();
        const message_type = randomEl(messageTypes);
        const trigger_event = `${aLetter(1)}${aNumber(1)}${aNumber(1)}`;
        const translation_script = ``; // Gotta be done in SQL fixer
        const more_info = {
            "trans_text_type": ``,
            "text_with_header": favorableOdds,
        };
        const tables = `{patient,guarantor,order,study,studies,facility,company,insurances,referringDoctor,readingDoctor,approvingDoctor}`;
        const is_order_based = !favorableOdds;

        return {
            hl7_interface_id,
            trigger_name,
            message_type,
            trigger_event,
            translation_script,
            more_info,
            tables,
            is_order_based,
            "_id": `${index}`
        };
    };
};
