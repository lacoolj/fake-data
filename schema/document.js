'use strict';

// const exportDir = `${process.env.FAKE_EXPORT_DIR}`;

const {
    Set,
} = require('immutable');

const {
    years,
    months,
    days,
    hours,
    minutes
} = require('../starter-data/shared');

const {
    fileTypes,
    documentSources
} = require('../starter-data/document');

const {
    numberUtils,
    letterUtils,
    randomElFromList,
    makeUidInstance,
    makeUidSeries
} = require('../utils');

const {
    randomEl,
    nonZeroNumber,
    beatTheOdds,
    aNumber
} = numberUtils;

module.exports = dependentData => {
    const {
        // document_types,
        companies,
        file_stores,
        orders,
        patients,
        providers,
        provider_groups,
        studies,
        users
    } = dependentData;

    const {
        idList: companyIds,
        dataMap: companyData,
    } = companies;

    const company = companyData.get(companyIds.first());

    const documentTypes = company.document_types;

    // const {
    //     idList: documentTypeIds,
    //     dataMap: documentTypeData
    // } = document_types;

    const {
        idList: fileStoreIds
    } = file_stores;

    const {
        idList: orderIds,
        dataMap: orderData,
    } = orders;

    const {
        idList: patientIds,
    } = patients;

    const {
        idList: providerIds,
    } = providers;

    const {
        idList: providerGroupIds,
    } = provider_groups;

    const {
        idList: studyIds,
    } = studies;

    const {
        idList: userIds
    } = users;

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const series_uid = makeUidSeries();
        const document_count = nonZeroNumber();
        const year = randomEl(years);
        const month = randomEl(months);
        const day = randomEl(days);
        const hour = randomEl(hours);
        const minute = randomEl(minutes);
        const created_dt = `${year}-${month}-${day}T${hour}:${minute}:00-05:00`;
        const created_dt_display = created_dt;

        const instance_uid = makeUidInstance();

        const document_file_name = instance_uid;
        const document_file_dir = `${year}/${month}/${day}`;
        const document_file_type = randomEl(fileTypes);

        let provider_id = ``;
        let provider_group_id = ``;
        let patient_id = ``;
        let order_id = ``;
        let study_id = ``;

        if ( beatTheOdds() ) {
            study_id = randomElFromList(studyIds);
        }
        else {
            if ( beatTheOdds() ) {
                order_id = randomElFromList(orderIds);
            }
            else {
                if ( beatTheOdds() ) {
                    patient_id = randomElFromList(patientIds);
                }
                else {
                    if ( beatTheOdds() ) {
                        provider_id = randomElFromList(providerIds);
                    }
                    else {
                        provider_group_id = randomElFromList(providerGroupIds);
                    }
                }
            }
        }


        const document_type = randomEl(documentTypes);
        // const document_type_id = randomElFromList(documentTypeIds);
        // const documentType = documentTypeData.get(document_type_id);

        return {
            patient_id,
            "doc_level": order_id > 0
                ? study_id > 0
                    ? 1
                    : 2
                : 0,
            document_count,
            "documents_uploaded": document_count,
            document_type,
            "document_source": randomEl(documentSources),
            "has_completed": true,
            "notes": ``,
            "created_by": randomElFromList(userIds),
            created_dt,
            created_dt_display,
            "has_deleted": false,
            "deleted_dt": ``,
            "file_store_id": randomElFromList(fileStoreIds),
            study_id,
            order_id,
            "lab_order_id": ``,
            "lab_order_test_id": ``,
            provider_group_id,
            provider_id,
            series_uid,
            instance_uid,
            // "document_info": ``,
            document_file_dir,
            document_file_name,
            document_file_type,
            // document_type_id,
            "provider_contact_id": ``,
            "_id": `${index}`
        };
    };
};
