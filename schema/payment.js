'use strict';

const {
    random,
    name,
} = require('faker');

const {
    fileTypes,
    documentSources
} = require('../starter-data/document');

const {
    numberUtils,
    letterUtils,
    randomElFromList,
    // makeUidInstance,
    // makeUidSeries
} = require('../utils');

const {
    aLetter,
} = letterUtils;

const {
    randomEl,
    nonZeroNumber,
    beatTheOdds,
    aNumber,
} = numberUtils;

const {
    // years,
    // months,
    // days,
    // hours,
    // minutes,
    makeDate,
    makeTimestampTz,
} = require('../starter-data/shared');

module.exports = dependentData => {
    const {
        companies,
        facilities,
        payment_reasons,
        patients,
        provider_contacts,
        provider_groups,
        insurance_providers,
        users,
    } = dependentData;

    const {
        idList: companyIds,
    } = companies;

    const {
        idList: facilityIds,
    } = facilities;

    const {
        idList: paymentReasonIds,
    } = payment_reasons;

    const {
        idList: patientIds,
    } = patients;

    const {
        idList: providerContactIds,
    } = provider_contacts;

    const {
        idList: providerGroupIds,
    } = provider_groups;

    const {
        idList: insuranceProviderIds,
    } = insurance_providers;

    const {
        idList: userIds,
    } = users;

    const company_id = randomElFromList(companyIds);

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        const facility_id = randomElFromList(facilityIds);

        let patient_id = ``;
        let insurance_provider_id = ``;
        let provider_group_id = ``;
        let provider_contact_id = ``;
        if ( favorableOdds ) {
            insurance_provider_id = randomElFromList(insuranceProviderIds);
        }
        else {
            if ( beatTheOdds() ) {
                patient_id = randomElFromList(patientIds);
            }
            else {
                if ( !beatTheOdds() ) {
                    provider_contact_id = randomElFromList(providerContactIds)
                }
                else {
                    provider_group_id = randomElFromList(providerGroupIds);
                }
            }
        }

        if ( !patient_id && !insurance_provider_id && !provider_contact_id && !provider_group_id ) {
            throw new Error(`No association for payment available`);
        }

        const payment_reason_id = randomElFromList(paymentReasonIds);
        const amount = `$${favorableOdds ? nonZeroNumber(4) : nonZeroNumber(3)}.${aNumber(2)}`;
        const payment_dt = makeTimestampTz();
        const accounting_date = payment_dt.slice(0, 10);
        const created_by = randomElFromList(userIds);

        const payer_type = patient_id
            ? `patient`
            : insurance_provider_id
                ? `insurance`
                : provider_group_id
                    ? `ordering_facility`
                    : provider_contact_id
                        ? `ordering_provider`
                        : ``;

        const invoice_no = favorableOdds
            ? `${aLetter(4)}${aNumber(9)}`
            : ``;

        const notes = random.words();

        let mode = ``;
        let card_name = ``;
        let card_number = ``;
        if ( favorableOdds ) {
            mode = {
                "type": `string`,
                "enum": [
                    `eft`,
                    `eft`,
                    `eft`,
                    `eft`,
                    `cash`,
                    `cash`,
                    `cash`,
                ]
            };
        }
        else {
            if ( beatTheOdds() ) {
                if ( beatTheOdds() ) {
                    mode = `card`;
                    card_name = `${name.firstName()} ${name.lastName()}`;
                    card_number = `${nonZeroNumber(4)}${nonZeroNumber(4)}${nonZeroNumber(4)}${nonZeroNumber(4)}`;
                }
                else {
                    mode = `adjustment`;
                    if ( beatTheOdds() ) {
                        card_name = `${name.firstName()} ${name.lastName()}`;
                        card_number = `${nonZeroNumber(4)}${nonZeroNumber(4)}${nonZeroNumber(4)}${nonZeroNumber(4)}`;
                    }
                }
            }
            else {
                mode = `check`;
                card_number = `${nonZeroNumber(5)}${nonZeroNumber(4)}`;
            }
        }

        return {
            company_id,
            facility_id,
            patient_id,
            insurance_provider_id,
            provider_group_id,
            provider_contact_id,
            payment_reason_id,
            amount,
            accounting_date,
            created_by,
            payment_dt,
            invoice_no,
            "alternate_payment_id": ``,
            payer_type,
            notes,
            mode,
            card_name,
            card_number,
            "_id": `${index}`
        }

    };

};
