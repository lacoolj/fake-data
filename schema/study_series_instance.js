'use strict';

const {
    numberUtils,
    randomElFromList,
    makeUidInstance,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    aNumber,
} = numberUtils;

module.exports = dependentData => {

    const {
        study_series,
        file_stores,
        application_entities,
    } = dependentData;

    const {
        idList: studySeriesIds,
        dataMap: studySeriesData,
    } = study_series;

    const {
        idList: fileStoreIds,
    } = file_stores;

    const {
        idList: applicationEntityIds,
        dataMap: applicationEntityData,
    } = application_entities;

    return ({ index }) => {
        const study_series_id = randomElFromList(studySeriesIds);
        const studySeries = studySeriesData.get(study_series_id);
        const study_id = studySeries.study_id;
        const sop_iuid = makeUidInstance();
        const sop_cuid = makeUidInstance();
        const instance_no = ``; // Gotta be done in SQL fixer
        const content_dt = ``; // Appears unused in prod DBs - leaving empty
        const instance_info = ``; // Gotta be done in SQL fixer
        const instance_attributes = ``;
        const has_archived = ``;
        const created_dt = makeTimestampTz();
        const updated_dt = ``; // Appears unused in prod DBs - leaving empty
        const stack_no = ``; // Appears unused in prod DBs - leaving empty
        const annotations = ``; // Appears unused in prod DBs - leaving empty
        const all_attributes = ``; // Appears unused in prod DBs - leaving empty
        const instance_guid = ``; // Appears unused in prod DBs - leaving empty
        const stack_id = aNumber(7); // No clue what this is...guess random number will work
        const temp_study_series_instance_id = ``; // Appears unused in prod DBs - leaving empty
        const study_uid = ``; // Gotta be done in SQL fixer
        const series_uid = ``; // Gotta be done in SQL fixer
        const current_status = ``; // Gotta be done in SQL fixer
        const ae_title_id = randomElFromList(applicationEntityIds);

        const aeTitle = applicationEntityData.get(ae_title_id);
        const file_store_id = aeTitle.file_store_id || randomElFromList(fileStoreIds);
        const file_md5 = `098f6bcd4621d373cade4e832627b4f6`; // md5('test') value
        const company_id = aeTitle.company_id;
        const api_version = 3;
        const dicom_patient_id = ``; // Gotta be done in SQL fixer
        const study_info = ``; // Appears unused in prod DBs - leaving empty
        const file_path = ``; // Gotta be done in SQL fixer
        const file_size = aNumber(6);

        return {
            study_series_id,
            study_id,
            sop_iuid,
            sop_cuid,
            instance_no,
            content_dt,
            instance_info,
            instance_attributes,
            has_archived,
            created_dt,
            updated_dt,
            stack_no,
            annotations,
            all_attributes,
            instance_guid,
            stack_id,
            temp_study_series_instance_id,
            study_uid,
            series_uid,
            current_status,
            file_store_id,
            file_md5,
            company_id,
            api_version,
            dicom_patient_id,
            study_info,
            file_path,
            file_size,
            ae_title_id,
            "_id": `${index}`
        };
    };
};
