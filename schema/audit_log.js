'use strict';

const {
    random,
    internet,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
} = require('../utils');

const {
    auditMessages,
    auditModules,
} = require('../starter-data/audit');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
} = numberUtils;

module.exports = dependentData => {

    const {
        companies,
        users,
        patients,
        studies,
        orders,
    } = dependentData;

    const {
        idList: companyIds,
    } = companies;

    const {
        idList: userIds,
        dataMap: userData,
    } = users;

    const {
        idList: patientIds,
    } = patients;

    const {
        idList: studyIds,
    } = studies;

    const {
        idList: orderIds,
    } = orders;

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const company_id = randomElFromList(companyIds);
        const user_id = randomElFromList(userIds);

        const patient_id = favorableOdds
            ? randomElFromList(patientIds)
            : ``;

        const order_id = patient_id && favorableOdds
            ? randomElFromList(orderIds)
            : ``;

        const study_id = order_id && favorableOdds
            ? randomElFromList(studyIds)
            : ``;

        const module_name = randomElFromList(auditModules);
        const screen_name = `${module_name} Screen`;
        const log_description = randomElFromList(auditMessages);
        const client_ip = internet.ip();
        const detailed_info = ``;
        const logged_dt = makeTimestampTz();
        const user_data = ``;
        const unique_id = study_id || order_id || patient_id || company_id;

        // looks like just the user's ID or 0
        const sa_id = favorableOdds
            ? user_id
            : `0`;

        const hash_value = ``;
        const hash_salt = ``;

        const audit_info = {
            "user_level": user_id > 2
                ? `user`
                : `system`,
            "username": userData.get(user_id)
        };

        return {
            user_id,
            patient_id,
            order_id,
            study_id,
            screen_name,
            module_name,
            log_description,
            client_ip,
            detailed_info,
            logged_dt,
            user_data,
            unique_id,
            sa_id,
            hash_value,
            hash_salt,
            audit_info,
            company_id,
            "_id": `${index}`
        };
    };
};
