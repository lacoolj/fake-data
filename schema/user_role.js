'use strict';

const {
    random
} = require('faker');

const {
    numberUtils
} = require('../utils');

const {
    aNumber,
    beatTheOdds
} = numberUtils;

const modulePermissions = require('../modulePermissions.json');

const permissions = modulePermissions.reduce(( perms, perm ) => {
    if ( perm.permission_code.length < 5 ) {
        perms.push(perm.permission_code);
    }
    return perms;
}, []);

module.exports = () => ( { index } ) => {
    const favorableOdds = beatTheOdds(98, 8);

    const role_name = random.words()
        .slice(0, 64);

    const role_description = `${role_name} ${random.words()}`
        .slice(0, 256);

    const randomPermissions = permissions
        .slice(aNumber(1), -aNumber(2))
        .join();

    return {
        role_name,
        role_description,
        "permissions": `{${randomPermissions}}`,
        "is_active": favorableOdds,
        "has_deleted": false,
        "deleted_dt": ``,
        "company_id": 1,
        "_id": `${index}`
    };
};
