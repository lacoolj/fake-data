'use strict';

const {
    name,
} = require('faker');

const {
    randomElFromList,
    numberUtils: {
        beatTheOdds,
        randomEl,
    },
} = require('../utils');

const {
    makeDate,
    makeTimestampTz,
} = require('../starter-data/shared');

module.exports = dependentData => {
    const {
        study_form_templates,
        orders,
        patients,
    } = dependentData;

    const {
        idList: studyFormTemplateIds,
        dataMap: studyFormTemplateData,
    } = study_form_templates;

    const {
        idList: orderIds,
        dataMap: orderData,
    } = orders;

    const {
        idList: patientIds,
    } = patients;

    return ( { index } ) => {

        const favorableOdds = beatTheOdds();

        let order_id = ``;
        let patient_id = ``;
        if ( favorableOdds ) {
            order_id = randomElFromList(orderIds);
        }
        else {
            patient_id = randomElFromList(patientIds);
        }

        const study_form_id = randomElFromList(studyFormTemplateIds);
        const {
            patient_portal,
        } = studyFormTemplateData.get(study_form_id);

        let signed_by_entity = ``;
        let signed_by = ``;
        let signed_date = ``;
        let signature_image = ``;
        let signature_integrity_hmac = ``;
        if ( !favorableOdds ) {
            signed_by_entity = {
                "type": `string`,
                "enum": [
                    `P`,
                    `P`,
                    `P`,
                    `P`,
                    `P`,
                    `G`,
                    `G`,
                    `G`,
                    `O`,
                ]
            };
            signed_by = `${name.firstName()} ${name.lastName()}`;

            signed_date = makeDate();
            signature_image = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAABkCAYAAACoy2Z3AAAG00lEQVR4Xu3dPYhcVRQH8HNfVoKWQawUsVAh6iYzg4UgGL/ttNFYRsQu2NoZY6+NhY2SYKMomNIIfoAo2MzMZtEldiJiocRCZF2y5l0Z2SKikuRms3vvzG/rOe+d+zsH/uTt5k0KPwQIECBAoEAgFdQoIUCAAAECIUAsAQECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFBAgQICBA7AABAgQIFAkIkCI2RQQIECAgQOwAAQIECBQJCJAiNkUECBAgIEDsAAECBAgUCQiQIjZFtQgMh8OHZr1MJpPPaulJHwQWRUCALMqk5+ycy8vLNy0tLX0SET+vr68fPnv27Lk5O6LjEKheQIBUPyIN/odANxqNzkfEofF4/CUhAgR2R0CA7I67uxYKDAaDR7quOz0ej5cKL6GMAIFtEhAg2wTpMtdWYDQaPRgRr+acb1hfX3/MI6tr6+3qBC5HQIBcjpLP7IrA8vLybV3XPdl13ZGIONf3/csrKytf7UozbkqAwL8EBIilqEZgFhh79uw5nFJ6IKV0V0Rs5py/6/v+xMrKygfVNKoRAgT+FhAgFmG3BNJgMBhFxBNd190XEfdsBcZazvnzCxcuvL+6uvrjbjXnvgQIXFpAgFzayCeuUmDrT27v7vv+oa7rllNKt+SclyNinFL6KSK+3NzcfE9gXCW0cgI7LCBAdhh8EW43HA6PppQezjnv23oU9WfO+ZvZ7zFyztOI+Gg6nZ5ZBAtnJDDPAgJknqe7g2c7cODAYGlp6bmIONL3/SQiTqeUvtrY2Ph2bW3t1x1sxa0IENghAQGyQ9DzeJv9+/fv27t375Gtv5Ka/cL7xMbGxsm1tbXf5/G8zkSAwD8FBIiNuGKB4XD4wuxfGimlOyPi5Obm5snV1dXZIyo/BAgskIAAWaBhX81RL35ElXNeyTm/NZ1O37maa6olQKBtAQHS9vyuafceUV1TXhcn0LyAAGl+hNt/gMFg8GxK6cWU0h0eUW2/rysSmBcBATIvk9yGc8y+WyOldDznfH3O+bXpdPruNlzWJQgQmFMBATKng72SY81eVJhzPh4R5/u+P+Z9U1ei57MEFldAgCzw7Eej0fM556Oz/+CXUjrmuzUWdxmcnECJgAApUWu3phsOh09HxDMR8VTO+Yu+7984c+bMh+0eSecECOyWgADZLfmdue/fLyzMOT/add3jKaX7c86zt9q+P5lMTu1MC+5CgMC8CgiQtiebRqPRSznn2yPih62j3JhzvqXrulsvfmFh3/cfT6fTN9s+ru4JEKhJQIDUNI3/6WU4HI5SSrNv49uIiF8i4o+ImL3N9lDO+dPZiwq7rvttVp5zvjml9GPO+ZQXFjYwXC0SaFhAgFQ+vIMHD97bdd3XEfF2RNy4FSDXzUJjOp2+Xnn72iNAYI4FBEjlwx0MBidTSt9PJpNXKm9VewQILJiAAKl84FvvoErj8Xj2inQ/BAgQqEZAgFQzCo0QIECgLQEB0ta8dEuAAIFqBARINaPQCAECBNoSECBtzUu3BAgQqEZAgFQzCo0QIECgLQEB0ta8dEuAAIFqBARINaPQCAECBNoSECBtzUu3BAgQqEZAgFQzCo0QIECgLQEB0ta8dEuAAIFqBARINaPQCAECBNoSECBtzUu3BAgQqEZAgFQzCo0QIECgLQEB0ta8dEuAAIFqBARINaPQCAECBNoSECBtzUu3BAgQqEZAgFQzCo0QIECgLQEB0ta8dEuAAIFqBARINaPQCAECBNoSECBtzUu3BAgQqEZAgFQzCo0QIECgLQEB0ta8dEuAAIFqBARINaPQCAECBNoS+AubwSt0KqkqqQAAAABJRU5ErkJggg==`;
        }

        const assigned_to_portal = patient_portal && beatTheOdds();
        let portal_submit_date = ``;
        if ( assigned_to_portal && favorableOdds ) {
            portal_submit_date = makeTimestampTz();
        }

        return {
            order_id,
            study_form_id,

            "has_signed": ``,
            "has_esigned": ``,
            "esigned_by": ``,

            signed_by_entity,
            signed_by,

            "reprocess_reason": ``,
            "has_printed": ``,
            "user_text": ``,
            "tag_info": ``,

            "document_html": ``,
            "document_options": ``,

            signed_date,
            signature_image,
            signature_integrity_hmac,

            patient_id,
            assigned_to_portal,
            portal_submit_date,

            "_id": `${index}`
        };
    };
};
