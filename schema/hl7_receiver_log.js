'use strict';

const {
    random,
    internet,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
    aNumber,
} = numberUtils;

module.exports = dependentData => {

    const {
        companies,
        hl7_receivers,
        studies,
    } = dependentData;

    const {
        idList: companyIds,
        dataMap: companyData,
    } = companies;

    const {
        idList: hl7ReceiverIds,
        dataMap: hl7ReceiverData,
    } = hl7_receivers;

    const {
        idList: studyIds,
        dataMap: studyData,
    } = studies;

    const company_id = randomElFromList(companyIds);

    const currentStatuses = [
        `SW`,
        `SU`,
        `QU`,
        `FA`,
    ];

    const received_data = `MSH|^~\\&|INTERWORKS^1.2.840.114093.1.1.100.12^ISO|MMD^^ISO|EXA||20180926170537-0600||ORU^R01^ORU_R01|20180926170537-06001720921|P|2.5.1|||AL|AL|||||SFT|AVREO, Inc^L^^^^AVREO&1.2.840.114093.1.1&ISO^XX^^^1.2.840.114093.1.1|8.0.1.17708|INTERWORKS|1.2.840.114093.1.1.100.12||PID|1||MMD630999000^^^Radiology and Imaging of South Texas&1.2.840.114093.1.1.100.12.123456789&ISO^MR^MMD&&ISO||DOE^JANE^B^^^^L^^^^^^^|^^^^^^M^^^^^^^|19000101|F|||^^^^^^L||^PRN^PH^^1^^^^|^WPN^PH^^1^^^^||||||||||||||||N|||201809250951-0600|Radiology and Imaging of South Texas^1.2.840.114093.1.1.100.12.123456789^ISO|||||PV1|1|I|^^^MMD&&ISO||||||||||||||||2208685|||||||||||||||||||||||||||||||||ORC|RE|2208685^Radiology and Imaging of South Texas^1.2.840.114093.1.1.100.12.123456789^ISO|2208685^Radiology and Imaging of South Texas^1.2.840.114093.1.1.100.12.123456789^ISO|GMMD2208685^Radiology and Imaging of South Texas^1.2.840.114093.1.1.100.12.123456789^ISO|CM||^^^^^R|||||MMDHCP8216RF^ANOTHER^PERSON^^^^^^Radiology and Imaging of South Texas&1.2.840.114093.1.1.100.12.123456789&ISO^L^^^XX^MMD&&ISO^^^^^^^|||||||||||||||||||OBR|1|2208685^Radiology and Imaging of South Texas^1.2.840.114093.1.1.100.12.123456789^ISO|2208685^Radiology and Imaging of South Texas^1.2.840.114093.1.1.100.12.123456789^ISO|72148^MRI Lumbar Spine without Contrast^LN^^^^2.40|||201809261705-0600||||||Pt c/o lbp radiating to rt hip.  No known injury. Pt states hx of breast ca. Prior study done here 5/9/16Surg Hx:Cholecystectomy--1964Hysterectomy--1969Rt Breast Mastectomy--1971Lt Knee replaced--2010Rt Knee replaced--2011|||MMDHCP8216RF^ANOTHER^PERSON^^^^^^Radiology and Imaging of South Texas&1.2.840.114093.1.1.100.12.123456789&ISO^L^^^XX^MMD&&ISO^^^^^^^||||(555)555-1234||201809261705-0600||MR|F||^^^201809171606^201809171611^R||||^Pt c/o lbp radiating to rt hip.  No known injury. Pt states hx of breast ca. Prior study done here 5/9/16Surg Hx:Cholecystectomy--1964Hysterectomy--1969Rt Breast Mastectomy--1971Lt Knee replaced--2010Rt Knee replaced--2011^^(L)^(L)Pt c/o lbp radiating to rt hip.  No known injury. Pt states hx of breast ca. Prior study done here 5/9/16Surg Hx:Cholecystectomy--1964Hysterectomy--1969Rt Breast Mastectomy--1971Lt Knee replaced--2010Rt Knee replaced--2011^L^29^1|RIST11&MAHESHWARI&MUKUL&&&&&&Radiology and Imaging of South Texas&1.2.840.114093.1.1.100.12.123456789&ISO^201809261541-0600^201809261705-0600||||||||||||||||||IPC|2208685||1.3.6.1.4.1.11157.505685307.20180912162543.2392893.81398508.0||||||OBX|1|TX|||Exam#:  MMD2208685      ~~~MRI Lumbar Spine without Contrast~~HISTORY:  Pt c/o lbp radiating to rt hip.  No known injury. Pt states hx of breast ca. Prior study done here 5/9/16Surg Hx:Cholecystectomy--1964Hysterectomy--1969Rt Breast Mastectomy--4971Lt Knee replaced--2010Rt Knee replaced--2011~~COMPARISON:  MRI of the lumbosacral spine report only from 5/9/2016.~~TECHNIQUE: Multiplanar MRI sequences were performed through the lumbar without intravenous administration of gadolinium.~~FINDINGS:~~ALIGNMENT:  Moderate rotoscoliosis of the lumbosacral spine with convexity towards left side. Minimal anterolisthesis of L4 on L5 seen.~~DISC SPACES:  Disc desiccation is visualized throughout the lumbosacral spine.~Mild diffuse disc bulges at T12-L1, L1-L2, L2-L3 with mild impression on the thecal sac with no significant spinal canal stenosis. Mild facet arthropathy is seen with no significant spinal canal stenosis seen.~~Very mild diffuse disc bulges are seen from L3-L4 to L5-S1 level with posterior lateral disc bulges particularly at the L3-L4 and L4-L5 levels with mild impression on the thecal sac. The neural foramina are patent. There is mild central spinal canal stenosis at the L3-L4 and L4-L5 levels. The exiting nerve roots are not compressed.~~VERTEBRAE:  Normal.~~MARROW: Mild degenerative endplate changes at multiple levels from T10-T11 to L1-L2 levels. INTRADURAL:  The conus medullaris is normal.  No intradural abnormality is demonstrated. ~~OTHER:  None.~~IMPRESSION:~~Very mild diffuse disc bulges at L3-L4 to L5-S1 levels. There is posterior lateral disc bulges at L3-L4 and L4-L5 levels. There is mild central spinal canal stenosis at the L3-L4 level and L4-L5 levels.~~Mild diffuse disc bulges at T12-L1 to L2-L3 levels with no significant spinal canal stenosis seen~~Moderate rotoscoliosis of the lumbosacral spine with convexity towards left side.~~9/26/2018 5:02 PM~Signing Physician: Some Doctor MD~Workstation: RAD-PC~~||||||F||||||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||||||||undefined`;
    const ack_msg = `MSH|^~\\&|EXA||INTERWORKS^1.2.840.114093.1.1.100.12^ISO|MMD^^ISO|20180926170543||ACK^O01^ACK_O01|20180926170537-06001720921|P|2.5.1||||AL|||MSA|AA|20180926170537-06001720921|`;

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const received_dt = makeTimestampTz();
        const hl7_receiver_id = randomElFromList(hl7ReceiverIds);

        const ack_sent = favorableOdds;
        const current_status = randomEl(currentStatuses);
        const status_info = ``;

        const src_host = internet.ip();
        const src_port = aNumber(4);
        const message_id = `${aNumber(14)}-${aNumber(11)}`;

        const patient_id = ``; // Gotta be done in SQL fixer
        const has_deleted = false;
        const deleted_dt = ``;
        const accession_no = studyData.get(randomElFromList(studyIds)).accession_no;
        const order_id = ``; // Gotta be done in SQL fixer

        return {
            received_dt,
            hl7_receiver_id,
            received_data,
            ack_sent,
            current_status,
            status_info,
            company_id,
            src_host,
            src_port,
            message_id,
            ack_msg,
            patient_id,
            has_deleted,
            deleted_dt,
            accession_no,
            order_id,
            "_id": `${index}`
        };
    };
};
