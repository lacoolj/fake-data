'use strict';

const dayjs = require('dayjs');

const {
    random,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
    makeUidSeries,
    TIMESTAMP_FORMAT,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
} = numberUtils;

module.exports = dependentData => {

    const {
        studies,
        modalities,
        users,
    } = dependentData;

    const {
        idList: studyIds,
        dataMap: studyData,
    } = studies;

    const {
        idList: modalityIds,
        dataMap: modalityData,
    } = modalities;

    const {
        idList: userIds,
    } = users;

    return ({ index }) => {
        const favorableOdds = beatTheOdds();

        const study_id = randomElFromList(studyIds);
        const study = studyData.get(study_id);
        const series_uid = makeUidSeries();
        const series_number = ``; // Will need to generate with fixer SQL
        const series_description = random.words();
        const modality = modalityData.get(study.modality_id).modality_code;
        const perf_physician_id = ``;
        const series_info = ``;
        const series_attributes = ``;
        const pps_start = ``;
        const pps_iuid = ``;

        let created_dt = makeTimestampTz();
        let series_dt = created_dt;
        if ( favorableOdds ) {
            const date = dayjs(created_dt)
                .add(-1, randomEl(['day', 'month', 'year']))
                .format(TIMESTAMP_FORMAT);

            series_dt = `${date}${created_dt.slice(-6)}`;
        }

        // These three don't seem to be used ever so leaving empty
        let updated_dt = ``;
        let last_edit_by = ``;
        let last_edit_dt = ``;

        /*if ( !beatTheOdds() ) {
            const date = dayjs(created_dt)
                .add(1, randomEl([
                    'day',
                    'month',
                ]))
                .format(TIMESTAMP_FORMAT);

            updated_dt = `${date}${created_dt.slice(-6)}`;
            last_edit_by = randomElFromList(userIds)
            last_edit_dt = updated_dt;
        }*/

        // For some reason these are all null in my DB so leaving null for now
        const src_aet = ``;
        const ext_retr_aet = ``;
        const retrieve_aets = ``;
        const availability = ``;
        const series_status = ``;
        const series_guid = ``;
        const temp_study_series_id = ``;

        return {
            study_id,
            series_uid,
            series_number,
            series_dt,
            series_description,
            modality,
            perf_physician_id,
            series_info,
            series_attributes,
            pps_start,
            pps_iuid,
            created_dt,
            updated_dt,
            last_edit_by,
            last_edit_dt,
            src_aet,
            ext_retr_aet,
            retrieve_aets,
            availability,
            series_status,
            series_guid,
            temp_study_series_id,
                "_id": `${index}`
        };
    };
};
