#!/bin/sh

echo "Using $EXA_DB exporting to $FAKE_EXPORT_DIR";

FILE_DIR=starter-scripts

ls "${FILE_DIR}" > /tmp/.temp-list

while IFS= read -r filename
        do
            sh "${FILE_DIR}/${filename}"
		echo "${FILE_DIR}/${filename}"
        done < /tmp/.temp-list

rm /tmp/.temp-list
