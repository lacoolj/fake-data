---
inject: true
to: helpers.js
before: MANUAL SYMBOL ALIASING
skip_if: ["<%= name %>", <%= h.changeCase.camel(name) %>Symbol]
---
    ["<%= name %>", <%= h.changeCase.camel(name) %>Symbol],<%
    if (name !== tableName) { %>
    ["<%= tableName %>", <%= h.changeCase.camel(name) %>Symbol],<% } %>
