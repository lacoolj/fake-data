---
inject: true
to: helpers.js
before: MANUAL SYMBOLS
skip_if: const <%= h.changeCase.camel(name) %>Symbol
---
const <%= h.changeCase.camel(name) %>Symbol = Symbol();
