---
inject: true
to: helpers.js
before: MANUAL SYMBOL MAPPING
skip_if: <%= h.changeCase.camel(name) %>Symbol,
---
    [<%= h.changeCase.camel(name) %>Symbol, {
        "typeName": `<%= name %>`,
        "tableName": `<%= tableName %>`,
        "dependencies": [<%
            var deps = [`companies`]
                .concat(dependentTables.split(/,\s*/g))
                .filter(tbl => tbl.trim());
            var depNames = deps
                .reduce(( tbls, tbl ) => {
                    var tblStr = `\`${tbl}\``;
                    if ( tbl && tbls.indexOf(tblStr) === -1 ) {
                        tbls.push(tblStr);
                    }
                    return tbls;
                }, []);
            var depStr = depNames.join(`,
            `);

            var innerDep = depStr
                ? `
            ${depStr},
        ` : ``;
            %><%= innerDep %>],
        "columns": [
            <%
                var foreignKeyCols = deps
                    .reduce(( cols, tbl ) => {
                        if ( !tbl ) {
                            return cols;
                        }
                        if ( tbl === `users` ) {
                            cols.push(
                                !type || type === `content`
                                    ? `created_by`
                                    : `user_id`
                            );
                        }
                        else {
                            var arr = tbl.split(/_/);
                            var last = h.inflection.singularize(arr.pop());
                            var str = arr.concat(last).join('_');
                            cols.push(`${str}_id`);
                        }
                        return cols;
                    }, []);
                var cols = [`id`]
                    .concat(foreignKeyCols)
                    .concat(columns.split(/,\s*/g))
                    .filter(col => col.trim());
                var colNames = cols
                    .reduce(( cols, col ) => {
                        var colStr = `\`${col}\``;
                        if ( col && cols.indexOf(colStr) === -1 ) {
                            cols.push(colStr);
                        }
                        return cols;
                    }, []);

                var columnString = colNames.join(`,
            `);

                %><%= columnString %>,
        ],
        "getSchemaInit": require(`./schema/<%= name %>`),
    }],
