---
to: schema/<%= name %>.js
---
'use strict';

const {
    random,
} = require('faker');

const {
    numberUtils,
    randomElFromList,
} = require('../utils');

const {
    makeTimestampTz,
} = require('../starter-data/shared');

const {
    beatTheOdds,
    randomEl,
} = numberUtils;

module.exports = dependentData => {<%
    var deps = [`companies`]
        .concat(dependentTables.split(/,\s*/g))
        .filter(tbl => tbl.trim());

        var depNames = deps
            .reduce(( tbls, tbl ) => {
                if ( tbl && tbls.indexOf(tbl) === -1 ) {
                    tbls.push(tbl);
                }
                return tbls;
            }, []);

        var depString = depNames.join(`,
        `);

        if ( depNames.length > 0 ) { %>

    const {
        <%= depString %>,
    } = dependentData;
    <%
        var extractedDeps = deps.map(dep => {
            var arr = dep.split(/_/);
            var last = h.inflection.singularize(arr.pop());
            var str = arr.join('_');
            var depSingular = h.changeCase.camel(`${str}_${last}`);
            return `
    const {
        idList: ${depSingular}Ids,
        dataMap: ${depSingular}Data,
    } = ${dep};`;
        });
            %><%= extractedDeps.join(`
`) %><% } %>

    return ({ index }) => {
        // const company_id = randomElFromList(companyIds);

        return {
            // "has_deleted": false,
            // "deleted_dt": ``,
            // company_id,
            "_id": `${index}`
        };
    };
};
