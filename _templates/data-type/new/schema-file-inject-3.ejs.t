---
inject: true
to: package.json
before: "<%= type || 'content' %>"
skip_if: "<%= name %>"
---
    "<%= name %>": "echo '\n\n===== <%= name %> =====\n\n'; export FAKE_TYPE=<%= name %> FAKE_COUNT=<%= count || 8000 %>;npm run full",
