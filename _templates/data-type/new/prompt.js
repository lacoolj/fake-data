module.exports = [{
    'type': `input`,
    'name': `tableName`,
    'message': `DB table (study_forms, study_form_templates, etc.)`
}, {
    'type': `input`,
    'name': `dependentTables`,
    'message': `[OPTIONAL] Tables this data type relies on, comma-separated. (Default: companies)`
}, {
    'type': `input`,
    'name': `columns`,
    'message': `[OPTIONAL] Specify the columns to get from this table by default, comma-separated. (Default: id and foreign key columns from dependent tables)`
}, {
    'type': `input`,
    'name': `type`,
    'message': `Is this 'content' or 'setup'? (Default: 'content')`
}, {
    'type': `input`,
    'name': `count`,
    'message': `How many items should be created? (Default: 8000)`
}];
