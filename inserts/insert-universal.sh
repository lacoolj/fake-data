#!/bin/sh

TYPE=$1
TABLE=$2
LISTID=$(date +%s)

ls ${FAKE_EXPORT_DIR}/exports-*${TYPE}.csv > ${FAKE_EXPORT_DIR}/list_${LISTID}

while IFS= read -r filename
        do
            COLS=$(head -1 -q "$filename")
            psql $DB_STRING -d "$EXA_DB" -c "\copy public.${TABLE}($COLS) FROM '$filename' DELIMITER ',' CSV HEADER;"
		echo "$filename"
		rm "$filename"
        done < ${FAKE_EXPORT_DIR}/list_${LISTID}

rm ${FAKE_EXPORT_DIR}/list_${LISTID}

#sh ../starters/${TYPE}.sh
exit 0;

psql -d "$EXA_DB" -c "COPY ( SELECT id FROM public.${TABLE} WHERE NOT has_deleted ORDER BY id ASC ) TO '${FAKE_EXPORT_DIR}/${TYPE}.csv';"
echo "[$(sed ':a;N;$!ba;s/\n/,/g' ${FAKE_EXPORT_DIR}/${TYPE}.csv)]" > $FAKE_EXPORT_DIR/$TYPE.json
rm ${FAKE_EXPORT_DIR}/${TYPE}.csv
